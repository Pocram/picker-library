#include <Picker/Image.hpp>
#include <Picker/String.hpp>
#include <boost/filesystem.hpp>

// Required by stb library.
#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <Picker/External/stb/stb_image.h>
#include <Picker/External/stb/stb_image_write.h>

picker::Image::Image() :
	_dimensions( 0, 0 )
{
}

picker::Image::Image( unsigned width, unsigned height, const picker::Color& color ) :
	_dimensions( width, height )
{
	// Each pixel has 4 channels.
	_pixels.resize( width * height * 4 );
	for( unsigned int i = 0; i < _pixels.size(); i++ )
	{
		// R, G, B, A.
		_pixels[4 * i + 0] = color.r;
		_pixels[4 * i + 1] = color.g;
		_pixels[4 * i + 2] = color.b;
		_pixels[4 * i + 3] = color.a;
	}
}

picker::Image::Image( unsigned width, unsigned height, const ::std::vector<unsigned char>& pixels ) :
	_dimensions( width, height ),
	_pixels( pixels )
{
}

picker::Image::Image( const ::std::string& pathToImage )
{
	if( loadFromFile( pathToImage ) == false )
	{
		// TODO: Throw exception.
	}
}

bool picker::Image::loadFromFile( const ::std::string& pathToImage )
{
	int width;
	int height;
	int bithPerPixel;
	unsigned char* rgba = stbi_load( pathToImage.c_str(), &width, &height, &bithPerPixel, 4 );
	if( rgba == nullptr )
	{
		return false;
	}
	::std::vector<unsigned char> pixels( rgba, rgba + ( width * height * 4 ) );
	_pixels = pixels;
	_dimensions.x = width;
	_dimensions.y = height;
	stbi_image_free( rgba );

	return true;
}

bool picker::Image::saveToFile( const ::std::string& pathToImage )
{
	const boost::filesystem::path path( pathToImage );
	const ::std::string fileExtension = boost::filesystem::extension( path );
	// Make sure the extension is normalised.
	const ::std::string lowerExtension = picker::toLowerString( fileExtension );
	
	if( lowerExtension == ".png" )
	{
		// ""stride_in_bytes" is the distance in bytes from the first byte of
		//  a row of pixels to the first byte of the next row of pixels." - stb_image_write.h
		const int stride = _dimensions.x * 4;
		const int result = stbi_write_png( pathToImage.c_str(), _dimensions.x, _dimensions.y, 4, &_pixels[0], stride );
		return result != 0;
	}
	
	if( lowerExtension == ".bmp" )
	{
		const int result = stbi_write_bmp( pathToImage.c_str(), _dimensions.x, _dimensions.y, 4, &_pixels[0] );
		return result != 0;
	}

	if ( lowerExtension == ".tga" )
	{
		const int result = stbi_write_tga( pathToImage.c_str(), _dimensions.x, _dimensions.y, 4, &_pixels[0] );
		return result != 0;
	}

	if ( lowerExtension == ".hdr" )
	{
		// Convert pixels into float values.
		::std::vector<float> floatPixels;
		floatPixels.reserve( getPixels().size() );
		for( unsigned int i = 0; i < floatPixels.size(); i++ )
		{
			const float value = getPixels()[i] / 255.0f;
			floatPixels.push_back( value );
		}

		const int result = stbi_write_hdr( pathToImage.c_str(), _dimensions.x, _dimensions.y, 4, &floatPixels[0] );
		return result != 0;
	}
	
	// Format is unsupported.
	return false;
}

const picker::Vector2U& picker::Image::getDimensions() const
{
	return _dimensions;
}

const ::std::vector<unsigned char>& picker::Image::getPixels() const
{
	return _pixels;
}

picker::Color picker::Image::getColorAtPixel( unsigned x, unsigned y ) const
{
	const unsigned int index = x + y * getDimensions().x;
	
	const unsigned char& r = getPixels()[4 * index + 0];
	const unsigned char& g = getPixels()[4 * index + 1];
	const unsigned char& b = getPixels()[4 * index + 2];
	const unsigned char& a = getPixels()[4 * index + 3];

	return picker::Color( r, g, b, a );
}

picker::Color picker::Image::getColorAtPixel( const picker::Vector2U& coordinates ) const
{
	return getColorAtPixel( coordinates.x, coordinates.y );
}

void picker::Image::setPixelAt( const picker::Color& color, unsigned x, unsigned y )
{
	const unsigned int index = x + y * getDimensions().x;
	_pixels[4 * index + 0] = color.r * 255;
	_pixels[4 * index + 1] = color.g * 255;
	_pixels[4 * index + 2] = color.b * 255;
	_pixels[4 * index + 3] = color.a * 255;
}

void picker::Image::setPixelAt( const picker::Color& color, const picker::Vector2U& coordinates )
{
	setPixelAt( color, coordinates.x, coordinates.y );
}
