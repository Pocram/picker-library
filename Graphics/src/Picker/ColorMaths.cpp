#include <Picker/ColorMaths.hpp>

#include <Picker/Maths/Maths.hpp>

picker::Color picker::lerp( const picker::Color& baseColor, const picker::Color& targetColor, float t )
{
	picker::Color c;
	c.r = picker::lerp<float>( baseColor.r, targetColor.r, t );
	c.g = picker::lerp<float>( baseColor.g, targetColor.g, t );
	c.b = picker::lerp<float>( baseColor.b, targetColor.b, t );
	c.a = picker::lerp<float>( baseColor.a, targetColor.a, t );
	return c;
}
