#ifndef PICKER_GRAPHICS_IMAGE_HPP
#define PICKER_GRAPHICS_IMAGE_HPP

#include <Picker/Config.hpp>
#include <Picker/Maths/Vector2.hpp>
#include <Picker/Color.hpp>
#include <string>
#include <vector>

namespace picker
{
	////////////////////////////////////////////////////////////
	/// @class picker::Image
	/// @brief Represents an image made of pixels.
	////////////////////////////////////////////////////////////
	class PICKER_EXPORT Image
	{
	public:
		////////////////////////////////////////////////////////////
		/// @brief Default constructor. Creates an empty image with no size.
		////////////////////////////////////////////////////////////
		Image();

		////////////////////////////////////////////////////////////
		/// @brief Constructs an image with a plain colour.
		///
		/// @param width	The width in pixels.
		/// @param height	The height in pixels.
		/// @param color	The background colour.
		////////////////////////////////////////////////////////////
		Image( unsigned int width, unsigned int height, const picker::Color& color );

		////////////////////////////////////////////////////////////
		/// @brief Constructs an image with a plain colour.
		///
		/// @param width	The width in pixels.
		/// @param height	The height in pixels.
		/// @param pixels	The image's pixels in RGBA order. Values range from 0 to 255.
		////////////////////////////////////////////////////////////
		Image( unsigned int width, unsigned int height, const ::std::vector<unsigned char>& pixels );

		////////////////////////////////////////////////////////////
		/// @brief Loads an image from a file.
		///
		/// Currently supported file types are:
		///	  - *.jpeg
		///	  - *.png
		///	  - *.bmp
		///	  - *.gif (non-animated)
		///   - *.psd
		///   - *.hdr
		///	  - *.tga
		///
		/// @returns True if the image loaded correctly.
		///
		/// TODO: Add exception data
		////////////////////////////////////////////////////////////
		Image( const ::std::string& pathToImage );


		////////////////////////////////////////////////////////////
		/// @brief Loads an image from a file.
		///
		/// Currently supported file types are:
		///	  - *.jpeg
		///	  - *.png
		///	  - *.bmp
		///	  - *.gif (non-animated)
		///   - *.psd
		///   - *.hdr
		///	  - *.tga
		///
		/// @param pathToImage The path to the image file.
		///
		/// @returns True if the image loaded correctly.
		////////////////////////////////////////////////////////////
		bool loadFromFile( const ::std::string& pathToImage );

		////////////////////////////////////////////////////////////
		/// @brief Saves an image to a file.
		///
		/// The file format will be deduced from the path's extension. 
		/// Existing files will be overwritten.
		/// Currently supported file types are:
		///	  - *.png
		///	  - *.bmp
		///   - *.hdr
		///	  - *.tga
		///
		/// @param pathToImage The path to the image file.
		///
		/// @returns True if the file saved correctly.
		////////////////////////////////////////////////////////////
		bool saveToFile( const ::std::string& pathToImage );


		////////////////////////////////////////////////////////////
		/// @brief Gets the image's dimension in pixels.
		/// @returns The dimensions in pixels.
		////////////////////////////////////////////////////////////
		const picker::Vector2U& getDimensions() const;

		////////////////////////////////////////////////////////////
		/// @brief Gets the image's pixels.
		///
		/// The pixel vector is arranged on a row basis.
		/// It starts with the topmost left pixel and continues to the right.
		/// Each row starts with the leftmost pixel as well.
		///
		/// @returns A vector of pixels.
		////////////////////////////////////////////////////////////
		const ::std::vector<unsigned char>& getPixels() const;

		////////////////////////////////////////////////////////////
		/// @brief Gets the pixel's color at the given position.
		///
		/// The coordinates' ranges are not checked. 
		/// An overflow will result in an exception.
		///
		/// @param x The pixel's X coordinate.
		/// @param y The pixel's Y coordinate.
		///
		/// @returns The pixel at the given position.
		////////////////////////////////////////////////////////////
		picker::Color getColorAtPixel( unsigned int x, unsigned int y ) const;

		////////////////////////////////////////////////////////////
		/// @brief Gets the pixel's color at the given position.
		///
		/// The coordinates' ranges are not checked. 
		/// An overflow will result in an exception.
		///
		/// @param coordinates The pixel's coordinates.
		///
		/// @returns The pixel at the given position.
		////////////////////////////////////////////////////////////
		picker::Color getColorAtPixel( const picker::Vector2U& coordinates ) const;

		////////////////////////////////////////////////////////////
		/// @brief Sets the pixel's color at the given position.
		///
		/// The coordinates' ranges are not checked. 
		/// An overflow will result in an exception.
		///
		/// @param color	The pixel's color.
		/// @param x		The pixel's X coordinate.
		/// @param y		The pixel's Y coordinate.
		////////////////////////////////////////////////////////////
		void setPixelAt( const picker::Color& color, unsigned int x, unsigned int y );

		////////////////////////////////////////////////////////////
		/// @brief Sets the pixel's color at the given position.
		///
		/// The coordinates' ranges are not checked. 
		/// An overflow will result in an exception.
		///
		/// @param color		The pixel's color.
		/// @param coordinates	The pixel's coordinates.
		////////////////////////////////////////////////////////////
		void setPixelAt( const picker::Color& color, const picker::Vector2U& coordinates );


	private:
		picker::Vector2U			_dimensions;	///< The image's dimensions in pixels.
		::std::vector<unsigned char>	_pixels;		///< The image's pixels.
	};
}

#endif // PICKER_GRAPHICS_IMAGE_HPP