#ifndef PICKER_GRAPHICS_COLOR_HPP
#define PICKER_GRAPHICS_COLOR_HPP

#include <Picker/Config.hpp>

namespace picker
{
	////////////////////////////////////////////////////////////
	/// @class picker::Color
	/// @brief Represents a single colour.
	////////////////////////////////////////////////////////////
	class PICKER_EXPORT Color
	{
	public:
		////////////////////////////////////////////////////////////
		/// @brief Default constructor. Creates a solid white colour.
		////////////////////////////////////////////////////////////
		Color();

		////////////////////////////////////////////////////////////
		/// @brief Constructs a colour.
		///
		/// @param red		The colour's red component. Ranges from 0 to 1.
		/// @param green	The colour's green component. Ranges from 0 to 1.
		/// @param blue		The colour's blue component. Ranges from 0 to 1.
		/// @param alpha	The colour's alpha (transparency) component. Ranges from 0 to 1.
		////////////////////////////////////////////////////////////
		Color( float red, float green, float blue, float alpha = 1.0f );

		float r;	///< The color's red component.	Ranges from 0 to 1.
		float g;	///< The color's green component. Ranges from 0 to 1.
		float b;	///< The color's blue component. Ranges from 0 to 1.
		float a;	///< The color's alpha component. Ranges from 0 to 1.
	};
}

#endif // PICKER_GRAPHICS_COLOR_HPP