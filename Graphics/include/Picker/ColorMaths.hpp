#ifndef PICKER_GRAPHICS_COLOR_MATHS_HPP
#define PICKER_GRAPHICS_COLOR_MATHS_HPP

#include <Picker/Color.hpp>

namespace picker
{
	picker::Color lerp( const picker::Color& baseColor, const picker::Color& targetColor, float t );
}

#endif // PICKER_GRAPHICS_COLOR_MATHS_HPP