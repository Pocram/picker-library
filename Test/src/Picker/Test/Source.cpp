#include <Picker/OpenGL/RenderWindow.hpp>
#include <Picker/OpenGL/Mesh.hpp>
#include <Picker/Color.hpp>
#include <Picker/OpenGL/VertexBuffer.hpp>
#include <Picker/OpenGL/Material.hpp>
#include <Picker/OpenGL/Camera.hpp>
#include <Picker/Clock.hpp>
#include <Picker/Image.hpp>
#include <Picker/OpenGL.hpp>

#include <Picker/Containers/StlDefines.hpp>

#include <iostream>

int main( int argc, char* argv[] )
{
	const picker::Vector3F v( 1.0f, 2.0f, 3.0f );
	::std::cout << v.getMagnitude() << "\n";
	::std::cout << v.getSquaredMagnitude() << "\n";
	::std::cout << v.getNormalized().x << ", " << v.getNormalized().y << ", " << v.getNormalized().z << "\n";
	getchar();
	return 0;
	picker::ogl::RenderWindow w( 500, 500, "MyWindow" );
	w.setVisible( true );

	picker::ogl::Material material( "Shader/test.vs", "Shader/test.fs" );
	picker::ogl::Material material2( "Shader/test.vs", "Shader/test.fs" );

	picker::ogl::Texture texture;
	texture.loadFromFile( "cobblestone.png" );
	material.setTexture( "myTexture", texture, 0 );

	picker::ogl::Texture texture2;
	texture2.loadFromFile( "otherImage.png" );
	material2.setTexture( "myTexture", texture2, 2 );

	picker::ogl::VertexBuffer vb(
	{
		// Front
		picker::ogl::Vertex( { -1.0f, -1.0f, 1.0f },	picker::Color(),	{ 1.0f, 0.0f } ),
		picker::ogl::Vertex( { -1.0f, 1.0f, 1.0f },		picker::Color(),	{ 1.0f, 1.0f } ),
		picker::ogl::Vertex( { 1.0f, -1.0f, 1.0f },		picker::Color(),	{ 0.0f, 0.0f } ),
		picker::ogl::Vertex( { 1.0f, -1.0f, 1.0f },		picker::Color(),	{ 0.0f, 0.0f } ),
		picker::ogl::Vertex( { 1.0f, 1.0f, 1.0f },		picker::Color(),	{ 0.0f, 1.0f } ),
		picker::ogl::Vertex( { -1.0f, 1.0f, 1.0f },		picker::Color(),	{ 1.0f, 1.0f } ),

		// Back
		picker::ogl::Vertex( { -1.0f, -1.0f, -1.0f },	picker::Color(),{ 1.0f, 0.0f } ),
		picker::ogl::Vertex( { -1.0f, 1.0f, -1.0f },		picker::Color(),{ 1.0f, 1.0f } ),
		picker::ogl::Vertex( { 1.0f, -1.0f, -1.0f },		picker::Color(),{ 0.0f, 0.0f } ),
		picker::ogl::Vertex( { 1.0f, -1.0f, -1.0f },		picker::Color(),{ 0.0f, 0.0f } ),
		picker::ogl::Vertex( { 1.0f, 1.0f, -1.0f },		picker::Color(),{ 0.0f, 1.0f } ),
		picker::ogl::Vertex( { -1.0f, 1.0f, -1.0f },		picker::Color(),{ 1.0f, 1.0f } )
	}
	);

	vb = picker::ogl::VertexBuffer(
	{
		// Front
		picker::ogl::Vertex( { -1.0f, -1.0f, 1.0f },	picker::Color(),{ 1.0f, 0.0f } ),
		picker::ogl::Vertex( { -1.0f, 1.0f, 1.0f },		picker::Color(),{ 1.0f, 1.0f } ),
		picker::ogl::Vertex( { 1.0f, -1.0f, 1.0f },		picker::Color(),{ 0.0f, 0.0f } ),
		picker::ogl::Vertex( { 1.0f, -1.0f, 1.0f },		picker::Color(),{ 0.0f, 0.0f } ),
		picker::ogl::Vertex( { 1.0f, 1.0f, 1.0f },		picker::Color(),{ 0.0f, 1.0f } ),
		picker::ogl::Vertex( { -1.0f, 1.0f, 1.0f },		picker::Color(),{ 1.0f, 1.0f } ),

		// Back
		picker::ogl::Vertex( { -1.0f, -1.0f, -1.0f },	picker::Color(),{ 1.0f, 0.0f } ),
		picker::ogl::Vertex( { -1.0f, 1.0f, -1.0f },		picker::Color(),{ 1.0f, 1.0f } ),
		picker::ogl::Vertex( { 1.0f, -1.0f, -1.0f },		picker::Color(),{ 0.0f, 0.0f } ),
		picker::ogl::Vertex( { 1.0f, -1.0f, -1.0f },		picker::Color(),{ 0.0f, 0.0f } ),
		picker::ogl::Vertex( { 1.0f, 1.0f, -1.0f },		picker::Color(),{ 0.0f, 1.0f } ),
		picker::ogl::Vertex( { -1.0f, 1.0f, -1.0f },		picker::Color(),{ 1.0f, 1.0f } )
	}
	);

	picker::ogl::VertexBuffer vb2(
	{
		// Front
		picker::ogl::Vertex( { -1.0f, -1.0f, 1.0f },	picker::Color(),{ 1.0f, 0.0f } ),
		picker::ogl::Vertex( { -1.0f, 1.0f, 1.0f },		picker::Color(),{ 1.0f, 1.0f } ),
		picker::ogl::Vertex( { 1.0f, -1.0f, 1.0f },		picker::Color(),{ 0.0f, 0.0f } ),
		picker::ogl::Vertex( { 1.0f, -1.0f, 1.0f },		picker::Color(),{ 0.0f, 0.0f } ),
		picker::ogl::Vertex( { 1.0f, 1.0f, 1.0f },		picker::Color(),{ 0.0f, 1.0f } ),
		picker::ogl::Vertex( { -1.0f, 1.0f, 1.0f },		picker::Color(),{ 1.0f, 1.0f } ),

		// Back
		picker::ogl::Vertex( { -1.0f, -1.0f, -1.0f },	picker::Color(),{ 1.0f, 0.0f } ),
		picker::ogl::Vertex( { -1.0f, 1.0f, -1.0f },		picker::Color(),{ 1.0f, 1.0f } ),
		picker::ogl::Vertex( { 1.0f, -1.0f, -1.0f },		picker::Color(),{ 0.0f, 0.0f } ),
		picker::ogl::Vertex( { 1.0f, -1.0f, -1.0f },		picker::Color(),{ 0.0f, 0.0f } ),
		picker::ogl::Vertex( { 1.0f, 1.0f, -1.0f },		picker::Color(),{ 0.0f, 1.0f } ),
		picker::ogl::Vertex( { -1.0f, 1.0f, -1.0f },		picker::Color(),{ 1.0f, 1.0f } )
	}
	);

	vb = vb2;
	
	picker::ogl::Camera		camera( picker::Vector3F( 0.5f, 0.5f, 4.0f ), 1.0f );
	//camera.lookInDirection( { 0.0f, 0.0f, -1.0f } );
	
	picker::ogl::Transform	transform;
	transform.setPosition( { 0.0f, 0.0f, 0.0f } );

	float t = 0.0f;
	picker::Clock clock;

	w.lockCursorToWindow( true );

	bool isRunning = true;
	while ( isRunning )
	{
		picker::WindowEvent e;
		while( w.pollEvent( e ) )
		{
			if( e.eventType == picker::WindowEvent::Resized )
			{
				//float newAspectRatio = e.resizeWindowEvent.newWidth / e.resizeWindowEvent.newHeight;
				//camera.setAspectRatio( newAspectRatio );
				::std::cout << "Size\n";
			}

			if ( e.eventType == picker::WindowEvent::MouseMoved )
			{
				::std::cout << e.mouseMoveEvent.newXPosition << ", " << e.mouseMoveEvent.newYPosition << "\n";
			}

			else if ( e.eventType == picker::WindowEvent::Closed )
			{
				isRunning = false;
				break;
			}
		}

		w.clearWindow( picker::Color( 0.125f, 0.125f, 0.125f ) );

		transform.setRotation( { 0.0f, 40.0f * t, 0.0f } );
		w.draw( vb, material, camera, transform );
		transform.setRotation( { 0.0f, 40.0f * t + 90.0f, 0.0f } );
		w.draw( vb, material2, camera, transform );
		transform.setRotation( { 90.0f, 0.0f, 90.0f - 40.0f * t } );
		w.draw( vb, material2, camera, transform );

		w.display();

		t += clock.restart().asSeconds();
	}

	return 0;
}
