#ifndef PICKER_MAIN_CONTAINERS_STL_DEFINES_HPP
#define PICKER_MAIN_CONTAINERS_STL_DEFINES_HPP

#include <vector>
#include <array>

namespace picker
{
	namespace std
	{
		////////////////////////////////////////////////////////////
		/// @brief An stl vector within an stl vector.
		///
		/// This is just a short and readable way of writing ::std::vector<::std::vector<T>>.
		////////////////////////////////////////////////////////////
		template<typename T>
		using vector_2d = ::std::vector<::std::vector<T>>;

		////////////////////////////////////////////////////////////
		/// @brief An stl vector within an stl vector within an stl vector.
		///
		/// This is just a short and readable way of writing :::std::vector<::std::vector<::std::vector<T>>>.
		////////////////////////////////////////////////////////////
		template<typename T>
		using vector_3d = ::std::vector<::std::vector<::std::vector<T>>>;

		////////////////////////////////////////////////////////////
		/// @brief An stl array within an stl array.
		///
		/// This is just a short and readable way of writing ::std::array<::std::array<T, Y>, X>.
		////////////////////////////////////////////////////////////
		template<typename T, size_t X, size_t Y>
		using array_2d = ::std::array<::std::array<T, Y>, X>;

		////////////////////////////////////////////////////////////
		/// @brief An stl array within an stl array within an stl array.
		///
		/// This is just a short and readable way of writing ::std::array<::std::array<::std::array<T, Z>, Y>, X>.
		////////////////////////////////////////////////////////////
		template<typename T, size_t X, size_t Y, size_t Z>
		using array_3d = ::std::array<::std::array<::std::array<T, Z>, Y>, X>;

		////////////////////////////////////////////////////////////
		/// @brief An stl array within an stl array of cubic size.
		///
		/// This is just a short and readable way of writing picker::std::array_2d<T, S, S>.
		////////////////////////////////////////////////////////////
		template<typename T, size_t S>
		using cubic_array_2d = picker::std::array_3d<T, S, S, S>;

		////////////////////////////////////////////////////////////
		/// @brief An stl array within an stl array within an stl array of cubic size.
		///
		/// This is just a short and readable way of writing picker::std::array_3d<T, S, S, S>.
		////////////////////////////////////////////////////////////
		template<typename T, size_t S>
		using cubic_array_3d = picker::std::array_3d<T, S, S, S>;
	}
}

#endif // PICKER_MAIN_CONTAINERS_STL_DEFINES_HPP