#ifndef PICKER_MAIN_CONFIG_HPP
#define PICKER_MAIN_CONFIG_HPP

////////////////////////////////////////////////////////////
// Identify the operating system.
////////////////////////////////////////////////////////////
#if defined(_WIN32)
	#define PICKER_SYSTEM_WINDOWS
	// Prevent <windows.h> from defining min and max macros.
	#ifndef NOMINMAX
		#define NOMINMAX
	#endif
	//#elif defined(__APPLE__) && defined(__MACH__)
	//	#define PICKER_SYSTEM_MACOS
	//#elif defined(__unix__)
	//	// There are multiple unix systems.
	//	#if defined(__ANDROID__)
	//		#define PICKER_SYSTEM_ANDROID
	//	#elif defined(__linux__)
	//		#define PICKER_SYSTEM_LINUX
	//	#elif defined(__FreeBSD__) || defined(__FreeBSD_kernel__)
	//		#define PICKER_SYSTEM_FREEBSD
	//	#else
	//		#error This UNIX operating system is not supported by PICKER.
	//	#endif
#else
	#error This operating system is not supported by PICKER.
#endif

////////////////////////////////////////////////////////////
// Define portable helpers for dllimport and dllexport macros.
////////////////////////////////////////////////////////////
#if defined( PICKER_SYSTEM_WINDOWS )
	#define PICKER_EXPORT __declspec(dllexport)
	#define PICKER_IMPORT __declspec(dllimport)
	// Disable warning for Visual C++ compilers.
	#ifdef _MSC_VER
		#pragma warning(disable: 4251)
	#endif
#else // Mac, Lunux and FreeBSD
	#if __GNUC__ >= 4
		#define PICKER_EXPORT __attribute__ ((__visibility__ ("default")))
		#define PICKER_IMPORT __attribute__ ((__visibility__ ("default")))
	#else
		#define PICKER_EXPORT
		#define PICKER_IMPORT
	#endif
#endif

////////////////////////////////////////////////////////////
// Define portable deprecation macros.
////////////////////////////////////////////////////////////
#if defined(PICKER_DISABLE_DEPRECATED_WARNINGS)
	#define PICKER_DEPRECATED
#elif defined(_MSC_VER)
	#define PICKER_DEPRECATED __declspec(deprecated)
#elif defined(__GNUC__)
	#define PICKER_DEPRECATED __attribute__ ((deprecated))
#else
	#pragma message("PICKER_DEPRECATED is not supported for your compiler. It will still be defined, but will not do anything.")
	#define PICKER_DEPRECATED
#endif

#endif // PICKER_MAIN_CONFIG_HPP