#ifndef PICKER_MAIN_TIMESPAN_HPP
#define PICKER_MAIN_TIMESPAN_HPP

#include <Picker/Config.hpp>

namespace picker
{
	////////////////////////////////////////////////////////////
	/// @class picker::Timespan
	/// @brief Represents a span of time.
	////////////////////////////////////////////////////////////
	class PICKER_EXPORT Timespan
	{
	public:
		////////////////////////////////////////////////////////////
		/// @brief Default constructor.
		///
		/// Creates a timespan with no duration.
		////////////////////////////////////////////////////////////
		Timespan();

		////////////////////////////////////////////////////////////
		/// @brief Creates a timespan from a given amount of seconds.
		////////////////////////////////////////////////////////////
		static picker::Timespan fromSeconds( float seconds );

		////////////////////////////////////////////////////////////
		/// @brief Creates a timespan from a given amount of milliseconds.
		////////////////////////////////////////////////////////////
		static picker::Timespan fromMilliseconds( long milliseconds );

		////////////////////////////////////////////////////////////
		/// @brief Creates a timespan from a given amount of microseconds.
		////////////////////////////////////////////////////////////
		static picker::Timespan fromMicroseconds( long long microseconds );

		////////////////////////////////////////////////////////////
		/// @brief Converts the timespan into seconds.
		/// @returns The timespan in seconds.
		////////////////////////////////////////////////////////////
		float asSeconds() const;

		////////////////////////////////////////////////////////////
		/// @brief Converts the timespan into milliseconds.
		/// @returns The timespan in milliseconds.
		////////////////////////////////////////////////////////////
		long asMilliseconds() const;

		////////////////////////////////////////////////////////////
		/// @brief Converts the timespan into microseconds.
		/// @returns The timespan in microseconds.
		////////////////////////////////////////////////////////////
		long long asMicroseconds() const;



	private:
		long long _microseconds;	///< The timespan in microseconds.
	};


	////////////////////////////////////////////////////////////
	/// @brief Checks whether two timespans are equal.
	///
	/// @param left		The first timespan to compare.
	/// @param right	The second timespan to compare.
	///
	/// @returns Whether the timespans are equal.
	////////////////////////////////////////////////////////////
	PICKER_EXPORT bool operator ==( const picker::Timespan& left, const picker::Timespan& right );

	////////////////////////////////////////////////////////////
	/// @brief Checks whether two timespans are unequal.
	///
	/// @param left		The first timespan to compare.
	/// @param right	The second timespan to compare.
	///
	/// @returns Whether the timespans are unequal.
	////////////////////////////////////////////////////////////
	PICKER_EXPORT bool operator !=( const picker::Timespan& left, const picker::Timespan& right );

	////////////////////////////////////////////////////////////
	/// @brief Checks whether one timespan is shorter than the other.
	///
	/// @param left		The first, "shorter", timespan to compare.
	/// @param right	The second, "longer", timespan to compare.
	///
	/// @returns Whether the first timespan is shorter than the second.
	////////////////////////////////////////////////////////////
	PICKER_EXPORT bool operator <( const picker::Timespan& left, const picker::Timespan& right );

	////////////////////////////////////////////////////////////
	/// @brief Checks whether one timespan is longer than the other.
	///
	/// @param left		The first, "longer", timespan to compare.
	/// @param right	The second, "shorter", timespan to compare.
	///
	/// @returns Whether the first timespan is longer than the second.
	////////////////////////////////////////////////////////////
	PICKER_EXPORT bool operator >( const picker::Timespan& left, const picker::Timespan& right );

	////////////////////////////////////////////////////////////
	/// @brief Checks whether one timespan is shorter than or equal to another.
	///
	/// @param left		The first, "shorter", timespan to compare.
	/// @param right	The second, "longer", timespan to compare.
	///
	/// @returns Whether the first timespan is shorter than or equal to the second.
	////////////////////////////////////////////////////////////
	PICKER_EXPORT bool operator <=( const picker::Timespan& left, const picker::Timespan& right );

	////////////////////////////////////////////////////////////
	/// @brief Checks whether one timespan is longer than or equal to another.
	///
	/// @param left		The first, "longer", timespan to compare.
	/// @param right	The second, "shorter", timespan to compare.
	///
	/// @returns Whether the first timespan is longer than or equal to the second.
	////////////////////////////////////////////////////////////
	PICKER_EXPORT bool operator >=( const picker::Timespan& left, const picker::Timespan& right );

	////////////////////////////////////////////////////////////
	/// @brief Negates a timespan.
	///
	/// @param left		The timespan to negate.
	///
	/// @returns The negative timespan.
	////////////////////////////////////////////////////////////
	PICKER_EXPORT picker::Timespan operator -( const picker::Timespan& left );

	////////////////////////////////////////////////////////////
	/// @brief Calculates the sum of two timespans.
	///
	/// @param left		The first timespan.
	/// @param right	The second timespan.
	///
	/// @returns The sum of the two timespans.
	////////////////////////////////////////////////////////////
	PICKER_EXPORT picker::Timespan operator +( const picker::Timespan& left, const picker::Timespan& right );

	////////////////////////////////////////////////////////////
	/// @brief Calculates the difference between two timespans.
	///
	/// @param left		The first timespan.
	/// @param right	The timespan to subtract from the first.
	///
	/// @returns The difference between the two timespans.
	////////////////////////////////////////////////////////////
	PICKER_EXPORT picker::Timespan operator -( const picker::Timespan& left, const picker::Timespan& right );

	////////////////////////////////////////////////////////////
	/// @brief Adds the second timespan to the first.
	///
	/// @param left		The first timespan.
	/// @param right	The second timespan.
	///
	/// @returns The sum of the two timespans.
	////////////////////////////////////////////////////////////
	PICKER_EXPORT picker::Timespan operator +=( picker::Timespan& left, const picker::Timespan& right );

	////////////////////////////////////////////////////////////
	/// @brief Subtracts the second timespan from the first.
	///
	/// @param left		The first timespan.
	/// @param right	The timespan to subtract from the first.
	///
	/// @returns The difference between the two timespans.
	////////////////////////////////////////////////////////////
	PICKER_EXPORT picker::Timespan operator -=( picker::Timespan& left, const picker::Timespan& right );

	////////////////////////////////////////////////////////////
	/// @brief Calculates the product of two timespans.
	///
	/// @param left		The first timespan.
	/// @param right	The second timespan.
	///
	/// @returns The product of the two timespans.
	////////////////////////////////////////////////////////////
	PICKER_EXPORT picker::Timespan operator *( const picker::Timespan& left, const picker::Timespan& right );

	////////////////////////////////////////////////////////////
	/// @brief Scales the timespan by the value.
	///
	/// @param left		The timespan.
	/// @param right	The value to multiply by.
	///
	/// @returns The product of the timespan and the value.
	////////////////////////////////////////////////////////////
	PICKER_EXPORT picker::Timespan operator *( const picker::Timespan& left, float right );

	////////////////////////////////////////////////////////////
	/// @brief Scales the timespan by the value.
	///
	/// @param left		The timespan.
	/// @param right	The value to multiply by.
	///
	/// @returns The product of the timespan and the value.
	////////////////////////////////////////////////////////////
	PICKER_EXPORT picker::Timespan operator *( const picker::Timespan& left, long right );

	////////////////////////////////////////////////////////////
	/// @brief Scales the timespan by the value.
	///
	/// @param left		The timespan.
	/// @param right	The value to multiply by.
	///
	/// @returns The product of the timespan and the value.
	////////////////////////////////////////////////////////////
	PICKER_EXPORT picker::Timespan operator *( const picker::Timespan& left, long long right );

	////////////////////////////////////////////////////////////
	/// @brief Scales the timespan by the value.
	///
	/// @param left		The value to multiply by.
	/// @param right	The timespan.
	///
	/// @returns The product of the timespan and the value.
	////////////////////////////////////////////////////////////
	PICKER_EXPORT picker::Timespan operator *( float left, const picker::Timespan& right );

	////////////////////////////////////////////////////////////
	/// @brief Scales the timespan by the value.
	///
	/// @param left		The value to multiply by.
	/// @param right	The timespan.
	///
	/// @returns The product of the timespan and the value.
	////////////////////////////////////////////////////////////
	PICKER_EXPORT picker::Timespan operator *( long left, const picker::Timespan& right );

	////////////////////////////////////////////////////////////
	/// @brief Scales the timespan by the value.
	///
	/// @param left		The value to multiply by.
	/// @param right	The timespan.
	///
	/// @returns The product of the timespan and the value.
	////////////////////////////////////////////////////////////
	PICKER_EXPORT picker::Timespan operator *( long long left, const picker::Timespan& right );

	////////////////////////////////////////////////////////////
	/// @brief Scales the timespan by the value.
	///
	/// @param left		The timespan.
	/// @param right	The value to multiply by.
	///
	/// @returns The product of the timespan and the value.
	////////////////////////////////////////////////////////////
	PICKER_EXPORT picker::Timespan operator *=( picker::Timespan& left, float right );

	////////////////////////////////////////////////////////////
	/// @brief Scales the timespan by the value.
	///
	/// @param left		The timespan.
	/// @param right	The value to multiply by.
	///
	/// @returns The product of the timespan and the value.
	////////////////////////////////////////////////////////////
	PICKER_EXPORT picker::Timespan operator *=( picker::Timespan& left, long right );

	////////////////////////////////////////////////////////////
	/// @brief Scales the timespan by the value.
	///
	/// @param left		The timespan.
	/// @param right	The value to multiply by.
	///
	/// @returns The product of the timespan and the value.
	////////////////////////////////////////////////////////////
	PICKER_EXPORT picker::Timespan operator *=( picker::Timespan& left, long long right );

	////////////////////////////////////////////////////////////
	/// @brief Calculates the quotient of two timespans.
	///
	/// @param left		The first timespan.
	/// @param right	The second timespan.
	///
	/// @returns The quotient of the two timespans.
	////////////////////////////////////////////////////////////
	PICKER_EXPORT picker::Timespan operator /( const picker::Timespan& left, const picker::Timespan& right );

	////////////////////////////////////////////////////////////
	/// @brief Calculates the quotient of the timespan divided by the value.
	///
	/// @param left		The timespan.
	/// @param right	The value.
	///
	/// @returns The quotient of the timespan divided by the value.
	////////////////////////////////////////////////////////////
	PICKER_EXPORT picker::Timespan operator /( const picker::Timespan& left, float right );

	////////////////////////////////////////////////////////////
	/// @brief Calculates the quotient of the timespan divided by the value.
	///
	/// @param left		The timespan.
	/// @param right	The value.
	///
	/// @returns The quotient of the timespan divided by the value.
	////////////////////////////////////////////////////////////
	PICKER_EXPORT picker::Timespan operator /( const picker::Timespan& left, long right );

	////////////////////////////////////////////////////////////
	/// @brief Calculates the quotient of the timespan divided by the value.
	///
	/// @param left		The timespan.
	/// @param right	The value.
	///
	/// @returns The quotient of the timespan divided by the value.
	////////////////////////////////////////////////////////////
	PICKER_EXPORT picker::Timespan operator /( const picker::Timespan& left, long long right );

	////////////////////////////////////////////////////////////
	/// @brief Divides the timespan by a value.
	///
	/// @param left		The timespan.
	/// @param right	The value.
	///
	/// @returns The quotient of the timespan divided by the value.
	////////////////////////////////////////////////////////////
	PICKER_EXPORT picker::Timespan operator /=( picker::Timespan& left, float right );

	////////////////////////////////////////////////////////////
	/// @brief Divides the timespan by a value.
	///
	/// @param left		The timespan.
	/// @param right	The value.
	///
	/// @returns The quotient of the timespan divided by the value.
	////////////////////////////////////////////////////////////
	PICKER_EXPORT picker::Timespan operator /=( picker::Timespan& left, long right );

	////////////////////////////////////////////////////////////
	/// @brief Divides the timespan by a value.
	///
	/// @param left		The timespan.
	/// @param right	The value.
	///
	/// @returns The quotient of the timespan divided by the value.
	////////////////////////////////////////////////////////////
	PICKER_EXPORT picker::Timespan operator /=( picker::Timespan& left, long long right );
}

#endif // PICKER_MAIN_TIMESPAN_HPP