#ifndef PICKER_MAIN_CLOCK_HPP
#define PICKER_MAIN_CLOCK_HPP

#include <Picker/Config.hpp>
#include <Picker/Timespan.hpp>

namespace picker
{
	////////////////////////////////////////////////////////////
	/// @class picker::Clock
	/// @brief Represents a timer that can be used to measure time.
	////////////////////////////////////////////////////////////
	class PICKER_EXPORT Clock
	{
	public:
		////////////////////////////////////////////////////////////
		/// @brief Default constructor.
		///
		/// This method automatically starts the timer.
		////////////////////////////////////////////////////////////
		Clock();

		
		////////////////////////////////////////////////////////////
		/// @brief Gets the elapsed time since the clock has been started or restarted.
		/// @returns The elapsed time since the clock has been started or restarted.
		////////////////////////////////////////////////////////////
		picker::Timespan getElapsedTime() const;
		
		////////////////////////////////////////////////////////////
		/// @brief Restarts the timer.
		/// @returns Returns the elapsed time until that point.
		////////////////////////////////////////////////////////////
		picker::Timespan restart();



	private:
		picker::Timespan _lastRestartedTime;	///< The time since epoch at the point of the last clock restart.
	};
}

#endif // PICKER_MAIN_CLOCK_HPP