#ifndef PICKER_MAIN_TIME_HPP
#define PICKER_MAIN_TIME_HPP

#include <Picker/Config.hpp>
#include <Picker/Timespan.hpp>
#include <Picker/Clock.hpp>

namespace picker
{
	class Timespan;

	////////////////////////////////////////////////////////////
	/// @brief Calculates the time since epoch.
	/// @returns The timespan since epoch.
	////////////////////////////////////////////////////////////
	PICKER_EXPORT picker::Timespan getTimeSinceEpoch();
}

#endif // PICKER_MAIN_TIME_HPP