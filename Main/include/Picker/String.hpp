#ifndef PICKER_MAIN_STRING_HPP
#define PICKER_MAIN_STRING_HPP

#include <Picker/Config.hpp>

#include <string>
#include <vector>

namespace picker
{
	////////////////////////////////////////////////////////////
	/// @brief Splits a string into multiple parts.
	///
	/// @param stringToSplit	The string that should be split.
	/// @param delimiter		The delimiter. The string will be split each time a delimiter is encountered. The delimiter will not be included in the output.
	/// @param filler			If multiple delimiters are encountered in succession, you may add fillers to represent empty space in the output.
	///
	/// @returns A vector of strings that were in between the delimiters.
	////////////////////////////////////////////////////////////
	::std::vector<::std::string> PICKER_EXPORT splitString( const ::std::string& stringToSplit, const ::std::string& delimiter, const ::std::string& filler = ::std::string() );

	////////////////////////////////////////////////////////////
	/// @brief Turns each character in a string to it's lowercase version.
	///
	/// @param string The string to work with.
	///
	/// @returns A lowercase version of the given string.
	////////////////////////////////////////////////////////////
	::std::string PICKER_EXPORT toLowerString( const ::std::string& string );
}

#endif // PICKER_MAIN_STRING_HPP