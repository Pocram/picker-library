#ifndef PICKER_MAIN_VECTOR_3_HPP
#define PICKER_MAIN_VECTOR_3_HPP

#include <Picker/Config.hpp>

#include <math.h>
#include <iomanip>
#include <string>
#include <sstream>

namespace picker
{
	////////////////////////////////////////////////////////////
	/// @class picker::Vector3
	/// @brief Represents a vector with three coordinates.
	///
	/// @tparam T The number type of the vector's axes. 
	/// It can be any type that supports arithmetic operators, 
	/// such as addition, subtraction, multiplication and division. 
	/// Generally, you would use `int` or `float`.
	///
	/// picker::Vector3 defines a mathematical vector with three coordinates: X, Y and Z.
	/// It can be used to describe a position, a direction, velocity, etc.
	///
	/// For convenience, the most common vector types have special typedefs:
	/// @li picker::Vector3F for `float` vectors
	/// @li picker::Vector3I for `int` vectors
	/// @li picker::Vector3U for `unsigned int` vectors
	////////////////////////////////////////////////////////////
	template <typename T>
	class PICKER_EXPORT Vector3
	{
	public:
		////////////////////////////////////////////////////////////
		/// \brief The default constructor.
		///
		/// The default values are set to (0, 0, 0).
		////////////////////////////////////////////////////////////
		Vector3();

		////////////////////////////////////////////////////////////
		/// \brief Creates a vector from given parameters.
		///
		/// \param x The X coordinate.
		/// \param y The Y coordinate.
		/// \param z The Z coordinate.
		////////////////////////////////////////////////////////////
		Vector3( T x, T y, T z );

		////////////////////////////////////////////////////////////
		/// \brief Creates a vector from a vector of another type.
		///
		/// \param vector The other vector.
		////////////////////////////////////////////////////////////
		template< typename U >
		explicit Vector3( const Vector3<U>& vector );

		////////////////////////////////////////////////////////////
		/// \brief Returns the vector's length.
		///
		/// If you want to compare the lengths of two vectors, refer to
		/// picker::Vector3::getSquaredMagnitude instead. In order to
		/// simply compare two magnitudes, the square root is not needed
		/// and takes more time to compute.
		////////////////////////////////////////////////////////////
		T getMagnitude() const;

		////////////////////////////////////////////////////////////
		/// \brief Returns the vector's squared length.
		///
		/// Compared to picker::Vector3::getMagnitude this operation
		/// takes less time to compute since it does not calculate
		/// the square root.  
		/// If you want to compare the lengths of two vectors, this
		/// method should be preferred.
		////////////////////////////////////////////////////////////
		T getSquaredMagnitude() const;

		////////////////////////////////////////////////////////////
		/// \brief Sets the vector's length to a specific value.
		////////////////////////////////////////////////////////////
		void setMagnitude( T targetMagnitude );

		////////////////////////////////////////////////////////////
		/// \brief Sets the vector's length to one.
		////////////////////////////////////////////////////////////
		void normalize();

		////////////////////////////////////////////////////////////
		/// \brief Returns a normalized vector of the same direction.
		////////////////////////////////////////////////////////////
		Vector3<T> getNormalized() const;

		////////////////////////////////////////////////////////////
		/// \brief Turns the vector into a string that can be used for printing.
		////////////////////////////////////////////////////////////
		::std::string toString() const;


		T x;	///< The vector's X coordinate.
		T y;	///< The vector's Y coordinate.
		T z;	///< The vector's Z coordinate.
	};

	template<typename T>
	Vector3<T> operator +( const Vector3<T>& left, const Vector3<T>& right );

	template<typename T>
	Vector3<T> operator -( const Vector3<T>& left, const Vector3<T>& right );

	template<typename T>
	Vector3<T> operator -( const Vector3<T>& right );

	template<typename T>
	Vector3<T> operator +=( Vector3<T>& left, const Vector3<T>& right );

	template<typename T>
	Vector3<T> operator -=( Vector3<T>& left, const Vector3<T>& right );

	template<typename T>
	Vector3<T> operator *( const Vector3<T>& left, T right );

	template<typename T>
	Vector3<T> operator /( const Vector3<T>& left, T right );

	template<typename T>
	Vector3<T> operator *=( Vector3<T>& left, T right );

	template<typename T>
	Vector3<T> operator /=( Vector3<T>& left, T right );

	template<typename T>
	bool operator ==( const Vector3<T>& left, const Vector3<T>& right );

	template<typename T>
	bool operator !=( const Vector3<T>& left, const Vector3<T>& right );

	#include <Picker/Maths/Vector3.inl>

	typedef Vector3<int>			Vector3I;
	typedef Vector3<unsigned int>	Vector3U;
	typedef Vector3<float>			Vector3F;
}

namespace std
{
	template<>
	struct hash<picker::Vector3I>
	{
		::std::size_t operator()( const picker::Vector3I& vector ) const
		{
			const ::std::size_t hashX( ::std::hash<int>()( vector.x ) );
			const ::std::size_t hashY( ::std::hash<int>()( vector.y ) );
			const ::std::size_t hashZ( ::std::hash<int>()( vector.z ) );
			return hashX ^ ( hashY << 1 ) ^ ( hashZ << 2 );
		}
	};

	template<>
	struct hash<picker::Vector3U>
	{
		::std::size_t operator()( const picker::Vector3U& vector ) const
		{
			const ::std::size_t hashX( ::std::hash<unsigned int>()( vector.x ) );
			const ::std::size_t hashY( ::std::hash<unsigned int>()( vector.y ) );
			const ::std::size_t hashZ( ::std::hash<unsigned int>()( vector.z ) );
			return hashX ^ ( hashY << 1 ) ^ ( hashZ << 2 );
		}
	};

	template<>
	struct hash<picker::Vector3F>
	{
		::std::size_t operator()( const picker::Vector3F& vector ) const
		{
			const ::std::size_t hashX( ::std::hash<float>()( vector.x ) );
			const ::std::size_t hashY( ::std::hash<float>()( vector.y ) );
			const ::std::size_t hashZ( ::std::hash<float>()( vector.z ) );
			return hashX ^ ( hashY << 1 ) ^ ( hashZ << 2 );
		}
	};
}

#endif // PICKER_MAIN_VECTOR_3_HPP