#ifndef PICKER_MAIN_MATH_MATRIX_HPP
#define PICKER_MAIN_MATH_MATRIX_HPP

#include <Picker/Config.hpp>

#include <Picker/Maths/Vector3.hpp>
#include <Picker/Maths/Vector3Maths.hpp>
#include <Picker/Exceptions/MatrixExceptions.hpp>

#include <vector>
#include <sstream>
#include <iostream>

namespace picker
{
	////////////////////////////////////////////////////////////
	/// \brief TODO: Document
	////////////////////////////////////////////////////////////
	template <typename T>
	class PICKER_EXPORT Matrix
	{
	public:
		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		Matrix();

		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		Matrix( unsigned int width, unsigned int height );


		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		static Matrix getIdentityMatrix( unsigned int scale );


		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		unsigned int getWidth() const;

		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		unsigned int getHeight() const;

		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		T getDeterminant() const;


		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		Matrix getTransposed() const;



		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		::std::string toMultilineString() const;


		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		::std::vector<T>& operator[]( unsigned int x )
		{
			return _columns[x];
		}

		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		const ::std::vector<T>& operator[]( unsigned int x ) const
		{
			return _columns[x];
		}

	private:
		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		::std::vector<picker::Matrix<T>> getLaplaceCuts() const;

		T getTwoByTwoDeterminant() const;
		T getThreeByThreeDeterminant() const;


		::std::vector<::std::vector<T>> _columns;	///< Contains all the matrix' columns.
	};

	template<typename T>
	Matrix<T> operator +( const Matrix<T>& left, const Matrix<T>& right );

	template<typename T>
	Matrix<T> operator +=( Matrix<T>& left, const Matrix<T>& right );

	template<typename T>
	Matrix<T> operator -( const Matrix<T>& left, const Matrix<T>& right );

	template<typename T>
	Matrix<T> operator -( const Matrix<T>& v );

	template<typename T>
	Matrix<T> operator *( const Matrix<T>& left, T right );

	template<typename T>
	Matrix<T> operator *( T left, const Matrix<T>& right );

	template<typename T>
	Matrix<T> operator *( const Matrix<T>& matrix, const picker::Vector3<T>& vector );

	template<typename T>
	Matrix<T> operator -=( Matrix<T>& left, const Matrix<T>& right );

	template<typename T>
	Matrix<T> operator *( const Matrix<T>& left, const Matrix<T>& right );

	template<typename T>
	Matrix<T> operator ==( const Matrix<T>& left, const Matrix<T>& right );

	template<typename T>
	Matrix<T> operator !=( const Matrix<T>& left, const Matrix<T>& right );

#include <Picker/Maths/Matrix.inl>

	typedef Matrix<int>		MatrixI;
	typedef Matrix<int>		MatrixU;
	typedef Matrix<float>	MatrixF;
}

#endif // PICKER_MAIN_MATH_MATRIX_HPP