#ifndef PICKER_MAIN_MATH_VECTOR_3_MATHS_HPP
#define PICKER_MAIN_MATH_VECTOR_3_MATHS_HPP

#include <Picker/Maths/Vector3.hpp>

namespace picker
{
	template <typename T>
	T scalar( const picker::Vector3<T>& a, const picker::Vector3<T>& b );

	template <typename T>
	picker::Vector3<T> cross( const picker::Vector3<T>& a, const picker::Vector3<T>& b );

	template<typename T>
	T scalar( const picker::Vector3<T>& a, const picker::Vector3<T>& b )
	{
		return a.x * b.x + a.y * b.y + a.z * b.z;
	}

	template<typename T>
	picker::Vector3<T> cross( const picker::Vector3<T>& a, const picker::Vector3<T>& b )
	{
		picker::Vector3<T> v;
		v.x = a.y * b.z - a.z * b.y;
		v.y = a.z * b.x - a.x * b.z;
		v.z = a.x * b.y - a.y * b.x;
		return v;
	}
}

#endif // PICKER_MAIN_MATH_VECTOR_3_MATHS_HPP