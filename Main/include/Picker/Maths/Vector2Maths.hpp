#ifndef PICKER_MAIN_MATH_VECTOR_2_MATHS_HPP
#define PICKER_MAIN_MATH_VECTOR_2_MATHS_HPP

#include <Picker/Maths/Vector2.hpp>

namespace picker
{
	template <typename T>
	T scalar( const picker::Vector2<T>& a, const picker::Vector2<T>& b );

	template <typename T>
	T cross( const picker::Vector2<T>& a, const picker::Vector2<T>& b );

	template<typename T>
	T scalar( const picker::Vector2<T>& a, const picker::Vector2<T>& b )
	{
		return a.x * b.x + a.y * b.y;
	}

	template<typename T>
	T cross( const picker::Vector2<T>& a, const picker::Vector2<T>& b )
	{
		return a.x * b.y - a.y * b.x;
	}
}

#endif // PICKER_MAIN_MATH_VECTOR_3_MATHS_HPP