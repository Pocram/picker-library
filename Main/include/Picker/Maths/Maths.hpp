#ifndef PICKER_MAIN_MATHS_MATHS_HPP
#define PICKER_MAIN_MATHS_MATHS_HPP

#include <Picker/Config.hpp>

namespace picker
{
	template <typename T>
	PICKER_EXPORT T lerp( T baseValue, T targetValue, float t )
	{
		return ( 1.0f - t ) * baseValue + t * targetValue;
	}

	PICKER_EXPORT int mod( int a, int b );
}

#endif // PICKER_MAIN_MATHS_MATHS_HPP