#ifndef PICKER_MAIN_MATH_MATRIX_INL
#define PICKER_MAIN_MATH_MATRIX_INL

template <typename T>
Matrix<T>::Matrix()
{
	::std::vector<T> row( 0, 0 );
	_columns = ::std::vector<::std::vector<T>>( 0, row );
}

template <typename T>
Matrix<T>::Matrix( unsigned width, unsigned height )
{
	::std::vector<T> row( height, 0 );
	_columns = ::std::vector<::std::vector<T>>( width, row );
}

template <typename T>
Matrix<T> Matrix<T>::getIdentityMatrix( unsigned scale )
{
	if ( scale <= 1 )
	{
		throw picker::InvalidMatrixSize();
	}

	picker::Matrix<T> m( scale, scale );
	for ( unsigned int i = 0; i < scale; i++ )
	{
		m[i][i] = 1;
	}
	return m;
}

template <typename T>
unsigned Matrix<T>::getWidth() const
{
	// Each column has the same size.
	return _columns.size();
}

template <typename T>
unsigned Matrix<T>::getHeight() const
{
	if ( getWidth() == 0 )
	{
		return 0;
	}

	return _columns[0].size();
}

template <typename T>
T Matrix<T>::getDeterminant() const
{
	const picker::Matrix<T>& thisMatrix = *this;
	if ( thisMatrix.getWidth() != thisMatrix.getHeight() )
	{
		throw picker::InvalidMatrixSize();
	}

	unsigned int matrixDimension = thisMatrix.getWidth();
	if ( matrixDimension == 3 )
	{
		return thisMatrix.getThreeByThreeDeterminant();
	}
	if ( matrixDimension == 2 )
	{
		return thisMatrix.getTwoByTwoDeterminant();
	}

	::std::vector<picker::Matrix<T>> allMatrixCuts = thisMatrix.getLaplaceCuts();
	::std::vector<T> determinants;
	for ( unsigned int i = 0; i < allMatrixCuts.size(); )
	{
		picker::Matrix<T>& m = allMatrixCuts[i];
		if ( m.getWidth() < 2 || m.getHeight() < 2 )
		{
			allMatrixCuts.erase( allMatrixCuts.begin() + i );
			continue;
		}

		if ( m.getWidth() == 3 )
		{
			T determinant = thisMatrix.getThreeByThreeDeterminant();
			determinants.push_back( determinant );
			allMatrixCuts.erase( allMatrixCuts.begin() + i );
			continue;
		}
		if ( m.getWidth() == 2 )
		{
			T determinant = thisMatrix.getTwoByTwoDeterminant();
			determinants.push_back( determinant );
			allMatrixCuts.erase( allMatrixCuts.begin() + i );
			continue;
		}

		::std::vector<picker::Matrix<T>> newCuts = m.getLaplaceCuts();
		allMatrixCuts.insert( allMatrixCuts.end(), newCuts.begin(), newCuts.end() );
		allMatrixCuts.erase( allMatrixCuts.begin() + i );
		i++;
	}

	::std::cout << "\n-----\n";
	for ( unsigned int i = 0; i < determinants.size(); i++ )
	{
		::std::cout << determinants[i] << ", ";
	}
	::std::cout << "\n";
	return 0;
}

template <typename T>
Matrix<T> Matrix<T>::getTransposed() const
{
	const picker::Matrix<T>& thisMatrix = *this;
	picker::Matrix<T> m( thisMatrix.getHeight(), thisMatrix.getWidth() );

	for ( unsigned int leftRowIndex = 0; leftRowIndex < thisMatrix.getHeight(); leftRowIndex++ )
	{
		for ( unsigned int i = 0; i < thisMatrix.getWidth(); i++ )
		{
			m[leftRowIndex][i] = thisMatrix[i][leftRowIndex];
		}
	}

	return m;
}

template <typename T>
::std::string Matrix<T>::toMultilineString() const
{
	::std::stringstream ss;
	for ( unsigned int y = 0; y < getHeight(); y++ )
	{
		for ( unsigned int x = 0; x < getWidth(); x++ )
		{
			const picker::Matrix<T>& m = *this;
			T value = m[x][y];
			ss << value;
			if ( x < getWidth() - 1 )
			{
				ss << ", ";
			}
		}
		ss << "\n";
	}
	return ss.str();
}

template <typename T>
::std::vector<picker::Matrix<T>> Matrix<T>::getLaplaceCuts() const
{
	const picker::Matrix<T>& thisMatrix = *this;
	// Matrix has to be square.
	if ( thisMatrix.getWidth() != thisMatrix.getHeight() )
	{
		throw picker::InvalidMatrixSize();
	}

	unsigned int matrixDimension = thisMatrix.getWidth();
	if ( matrixDimension <= 1 )
	{
		throw picker::InvalidMatrixSize();
	}

	::std::vector<picker::Matrix<T>> output( matrixDimension - 1 );
	// 2 is the smalles you can go.
	if ( matrixDimension == 2 )
	{
		output.push_back( thisMatrix );
		return output;
	}

	for ( unsigned int column = 0; column < matrixDimension; column++ )
	{
		picker::Matrix<T> newMatrix( matrixDimension - 1, matrixDimension - 1 );

		unsigned int sourceXShift = 0;
		for ( unsigned int x = 0; x < matrixDimension - 1; x++ )
		{
			if ( x == column )
			{
				sourceXShift++;
			}

			for ( unsigned int y = 0; y < matrixDimension - 1; y++ )
			{
				newMatrix[x][y] = thisMatrix[x + sourceXShift][y + 1];
			}
		}

		output.push_back( newMatrix );
	}
	return output;
}

template <typename T>
T Matrix<T>::getTwoByTwoDeterminant() const
{
	const picker::Matrix<T>& thisMatrix = *this;
	T determinant = thisMatrix[0][0] * thisMatrix[1][1] - thisMatrix[1][0] * thisMatrix[0][1];
	return determinant;
}

template <typename T>
T Matrix<T>::getThreeByThreeDeterminant() const
{
	const picker::Matrix<T>& thisMatrix = *this;
	T determinant = thisMatrix[0][0] * ( thisMatrix[1][1] * thisMatrix[2][2] - thisMatrix[2][1] * thisMatrix[1][2] )
		- thisMatrix[1][0] * ( thisMatrix[0][1] * thisMatrix[2][2] - thisMatrix[2][1] * thisMatrix[0][2] )
		+ thisMatrix[2][0] * ( thisMatrix[0][1] * thisMatrix[1][2] - thisMatrix[1][1] * thisMatrix[0][2] );
	return determinant;
}

template<typename T>
Matrix<T> operator+( const Matrix<T>& left, const Matrix<T>& right )
{
	if ( left.getWidth() != right.getWidth() || left.getHeight() != right.getHeight() )
	{
		throw picker::InvalidMatrixSize();
	}

	picker::Matrix<T> m( left.getWidth(), right.getHeight() );
	for ( unsigned int x = 0; x < left.getWidth(); x++ )
	{
		for ( unsigned int y = 0; y < left.getHeight(); y++ )
		{
			m[x][y] = left[x][y] + right[x][y];
		}
	}
	return m;
}

template<typename T>
Matrix<T> operator+=( Matrix<T>& left, const Matrix<T>& right )
{
	left = left + right;
	return left;
}

template<typename T>
Matrix<T> operator-( const Matrix<T>& left, const Matrix<T>& right )
{
	if ( left.getWidth() != right.getWidth() || left.getHeight() != right.getHeight() )
	{
		throw picker::InvalidMatrixSize();
	}

	picker::Matrix<T> m( left.getWidth(), right.getHeight() );
	for ( unsigned int x = 0; x < left.getWidth(); x++ )
	{
		for ( unsigned int y = 0; y < left.getHeight(); y++ )
		{
			m[x][y] = left[x][y] - right[x][y];
		}
	}
	return m;
}

template<typename T>
Matrix<T> operator-=( Matrix<T>& left, const Matrix<T>& right )
{
	left = left - right;
	return left;
}

template<typename T>
Matrix<T> operator-( const Matrix<T>& v )
{
	picker::Matrix<T> m( v.getWidth(), v.getHeight() );
	for ( unsigned int x = 0; x < v.getWidth(); x++ )
	{
		for ( unsigned int y = 0; y < v.getHeight(); y++ )
		{
			m[x][y] = -v[x][y];
		}
	}
	return m;
}

template<typename T>
Matrix<T> operator*( const Matrix<T>& left, T right )
{
	picker::Matrix<T> m( left.getWidth(), right.getHeight() );
	for ( unsigned int x = 0; x < left.getWidth(); x++ )
	{
		for ( unsigned int y = 0; y < left.getHeight(); y++ )
		{
			m[x][y] = left[x][y] * right;
		}
	}
	return m;
}

template<typename T>
Matrix<T> operator*( T left, const Matrix<T>& right )
{
	return right * left;
}

template <typename T>
Matrix<T> operator*( const Matrix<T>& matrix, const picker::Vector3<T>& vector )
{
	if ( matrix.getWidth() != 3 )
	{
		throw picker::InvalidMatrixSize();
	}

	picker::Matrix<T> m( 1, matrix.getHeight() );
	for ( unsigned int rightColumnIndex = 0; rightColumnIndex < matrix.getWidth(); rightColumnIndex++ )
	{
		picker::Vector3<T> matrixVector( matrix[0][rightColumnIndex], matrix[1][rightColumnIndex], matrix[2][rightColumnIndex] );
		T scalar = picker::scalar( vector, matrixVector );
		m[0][rightColumnIndex] = scalar;
	}

	return m;
}

template<typename T>
Matrix<T> operator*( const Matrix<T>& left, const Matrix<T>& right )
{
	if ( left.getWidth() != right.getHeight() )
	{
		throw picker::InvalidMatrixSize();
	}

	picker::Matrix<T> m( right.getWidth(), left.getHeight() );

	// For every row in the left matrix...
	for ( unsigned int leftRowIndex = 0; leftRowIndex < left.getHeight(); leftRowIndex++ )
	{
		// Do something for with every column in the right matrix.
		for ( unsigned int rightColumnIndex = 0; rightColumnIndex < right.getWidth(); rightColumnIndex++ )
		{
			// Save row and column as an array. We know that they are the same length.
			::std::vector<T> leftVector( left.getWidth(), 0 );
			::std::vector<T> rightVector( left.getWidth(), 0 );

			for ( unsigned int i = 0; i < left.getWidth(); i++ )
			{
				leftVector[i] = left[i][leftRowIndex];
				rightVector[i] = right[rightColumnIndex][i];
			}

			// Calculate scalar product.
			T scalar = 0;
			for ( unsigned int i = 0; i < left.getWidth(); i++ )
			{
				scalar += leftVector[i] * rightVector[i];
			}

			m[rightColumnIndex][leftRowIndex] = scalar;
		}
	}

	return m;
}

template <typename T>
Matrix<T> operator==( const Matrix<T>& left, const Matrix<T>& right )
{
	return left._columns == right._columns;
}

template <typename T>
Matrix<T> operator!=( const Matrix<T>& left, const Matrix<T>& right )
{
	return left._columns != right._columns;
}

#endif // PICKER_MAIN_MATH_MATRIX_INL
