#ifndef PICKER_MAIN_MATHS_VECTOR_2_HPP
#define PICKER_MAIN_MATHS_VECTOR_2_HPP

#include <Picker/Config.hpp>

#include <math.h>
#include <iomanip>

namespace picker
{
	////////////////////////////////////////////////////////////
	/// @class picker::Vector2
	/// @brief Represents a vector with three coordinates.
	///
	/// @tparam T The number type of the vector's axes. 
	/// It can be any type that supports arithmetic operators, 
	/// such as addition, subtraction, multiplication and division. 
	/// Generally, you would use `int` or `float`.
	///
	/// picker::Vector2 defines a mathematical vector with two coordinates: X and Y.
	/// It can be used to describe a position, a direction, velocity, etc.
	///
	/// For convenience, the most common vector types have special typedefs:
	/// @li picker::Vector2F for `float` vectors
	/// @li picker::Vector2I for `int` vectors
	/// @li picker::Vector2U for `unsigned int` vectors
	////////////////////////////////////////////////////////////
	template <typename T>
	class PICKER_EXPORT Vector2
	{
	public:
		////////////////////////////////////////////////////////////
		/// \brief The default constructor.
		///
		/// The default values are set to (0, 0).
		////////////////////////////////////////////////////////////
		Vector2();

		////////////////////////////////////////////////////////////
		/// \brief Creates a vector from given parameters.
		///
		/// \param x The X coordinate.
		/// \param y The Y coordinate.
		////////////////////////////////////////////////////////////
		Vector2( T x, T y );

		////////////////////////////////////////////////////////////
		/// \brief Creates a vector from a vector of another type.
		///
		/// \param vector The other vector.
		////////////////////////////////////////////////////////////
		template< typename U >
		explicit Vector2( const Vector2<U>& vector );

		////////////////////////////////////////////////////////////
		/// \brief Returns the vector's length.
		///
		/// If you want to compare the lengths of two vectors, refer to
		/// picker::Vector2::getSquaredMagnitude instead. In order to
		/// simply compare two magnitudes, the square root is not needed
		/// and takes more time to compute.
		////////////////////////////////////////////////////////////
		T getMagnitude() const;

		////////////////////////////////////////////////////////////
		/// \brief Returns the vector's squared length.
		///
		/// Compared to picker::Vector2::getMagnitude this operation
		/// takes less time to compute since it does not calculate
		/// the square root.  
		/// If you want to compare the lengths of two vectors, this
		/// method should be preferred.
		////////////////////////////////////////////////////////////
		T getSquaredMagnitude() const;

		////////////////////////////////////////////////////////////
		/// \brief Sets the vector's length to a specific value.
		////////////////////////////////////////////////////////////
		void setMagnitude( T targetMagnitude );

		////////////////////////////////////////////////////////////
		/// \brief Sets the vector's length to one.
		////////////////////////////////////////////////////////////
		void normalize();

		////////////////////////////////////////////////////////////
		/// \brief Returns a normalized vector of the same direction.
		////////////////////////////////////////////////////////////
		Vector2<T> getNormalized() const;

		T x;	///< The vector's X coordinate.
		T y;	///< The vector's Y coordinate.
	};

	template<typename T>
	Vector2<T> operator +( const Vector2<T>& left, const Vector2<T>& right );

	template<typename T>
	Vector2<T> operator -( const Vector2<T>& left, const Vector2<T>& right );

	template<typename T>
	Vector2<T> operator -( const Vector2<T>& right );

	template<typename T>
	Vector2<T> operator +=( Vector2<T>& left, const Vector2<T>& right );

	template<typename T>
	Vector2<T> operator -=( Vector2<T>& left, const Vector2<T>& right );

	template<typename T>
	Vector2<T> operator *( const Vector2<T>& left, T right );

	template<typename T>
	Vector2<T> operator /( const Vector2<T>& left, T right );

	template<typename T>
	Vector2<T> operator *=( Vector2<T>& left, T right );

	template<typename T>
	Vector2<T> operator /=( Vector2<T>& left, T right );

	template<typename T>
	bool operator ==( const Vector2<T>& left, const Vector2<T>& right );

	template<typename T>
	bool operator !=( const Vector2<T>& left, const Vector2<T>& right );

	#include <Picker/Maths/Vector2.inl>

	typedef Vector2<int>			Vector2I;
	typedef Vector2<unsigned int>	Vector2U;
	typedef Vector2<float>			Vector2F;
}

namespace std
{
	template<>
	struct hash<picker::Vector2I>
	{
		::std::size_t operator()( const picker::Vector2I& vector ) const
		{
			const ::std::size_t hashX( ::std::hash<int>()( vector.x ) );
			const ::std::size_t hashY( ::std::hash<int>()( vector.y ) );
			return hashX ^ ( hashY << 1 );
		}
	};

	template<>
	struct hash<picker::Vector2U>
	{
		::std::size_t operator()( const picker::Vector2U& vector ) const
		{
			const ::std::size_t hashX( ::std::hash<unsigned int>()( vector.x ) );
			const ::std::size_t hashY( ::std::hash<unsigned int>()( vector.y ) );
			return hashX ^ ( hashY << 1 );
		}
	};

	template<>
	struct hash<picker::Vector2F>
	{
		::std::size_t operator()( const picker::Vector2F& vector ) const
		{
			const ::std::size_t hashX( ::std::hash<float>()( vector.x ) );
			const ::std::size_t hashY( ::std::hash<float>()( vector.y ) );
			return hashX ^ ( hashY << 1 );
		}
	};
}


#endif // PICKER_MAIN_MATHS_VECTOR_2_HPP