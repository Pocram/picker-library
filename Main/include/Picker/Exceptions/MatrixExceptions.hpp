#ifndef PICKER_MAIN_EXCEPTIONS_MATRIX_EXCEPTIONS_HPP
#define PICKER_MAIN_EXCEPTIONS_MATRIX_EXCEPTIONS_HPP

#include <exception>

namespace picker
{
	class InvalidMatrixSize : public ::std::exception
	{
	public:
		char const* what() const throw( ) override
		{
			return "The given matrix does not fit the size requirements for the given action.";
		}
	};
}

#endif // PICKER_MAIN_EXCEPTIONS_MATRIX_EXCEPTIONS_HPP