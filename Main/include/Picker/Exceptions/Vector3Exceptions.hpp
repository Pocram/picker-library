#ifndef PICKER_MAIN_EXCEPTIONS_VECTOR_3_EXCEPTIONS_HPP
#define PICKER_MAIN_EXCEPTIONS_VECTOR_3_EXCEPTIONS_HPP

#include <exception>

namespace picker
{
	class InvalidVector3Exception : public ::std::exception
	{
	public:
		char const* what() const throw( ) override
		{
			return "The given vector is incompatible with the current action.";
		}
	};
}

#endif // PICKER_MAIN_EXCEPTIONS_VECTOR_3_EXCEPTIONS_HPP