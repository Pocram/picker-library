#include <Picker/String.hpp>

#include <algorithm>

::std::vector<::std::string> picker::splitString( const ::std::string& stringToSplit, const ::std::string& delimiter, const ::std::string& filler )
{
	::std::vector<::std::string> tokens;
	if( delimiter.empty() )
	{
		tokens.push_back( stringToSplit );
		return tokens;
	}

	::std::string::size_type delimSize = delimiter.size();
	::std::string::size_type lastPos = 0;
	::std::string::size_type pos = stringToSplit.find( delimiter, lastPos );
	while( true )
	{
		::std::string::size_type length = pos - lastPos;
		// If the length is 0 the remaining string starts with a delimiter.
		// Only add a filler string if the filler is set.
		if( length == 0 && filler != ::std::string() )
		{
			tokens.push_back( filler );
			lastPos = pos + delimSize;
			pos = stringToSplit.find( delimiter, lastPos );
			continue;
		}

		// When the last delimiter has been encountered, pos is set to npos.
		// We must account for that to get the correct results.
		if( pos == stringToSplit.npos )
		{
			length = stringToSplit.size() - lastPos;
		}

		// Get the string segment so far and add it to the output.
		::std::string substring = stringToSplit.substr( lastPos, length );
		if( substring.empty() == false )
		{
			tokens.push_back( substring );
		}

		if( pos == stringToSplit.npos )
		{
			break;
		}

		lastPos = pos + delimSize;
		pos = stringToSplit.find( delimiter, lastPos );
	}

	return tokens;
}

::std::string picker::toLowerString( const ::std::string& string )
{
	::std::string str = string;
	::std::transform( str.begin(), str.end(), str.begin(), ::tolower );
	return str;
}
