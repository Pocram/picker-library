#include <Picker/Clock.hpp>
#include <Picker/Time.hpp>
#include <iostream>

picker::Clock::Clock() :
	_lastRestartedTime( picker::getTimeSinceEpoch() )
{
}

picker::Timespan picker::Clock::getElapsedTime() const
{
	return picker::getTimeSinceEpoch() - _lastRestartedTime;
}

picker::Timespan picker::Clock::restart()
{
	picker::Timespan elapsedTime = getElapsedTime();
	_lastRestartedTime = picker::getTimeSinceEpoch();
	return elapsedTime;
}
