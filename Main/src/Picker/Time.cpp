#include <Picker/Time.hpp>
#include <chrono>

picker::Timespan picker::getTimeSinceEpoch()
{
	long long microsecondsSinceEpoch = ::std::chrono::duration_cast<::std::chrono::microseconds>( ::std::chrono::system_clock::now().time_since_epoch() ).count();
	return picker::Timespan::fromMicroseconds( microsecondsSinceEpoch );
}
