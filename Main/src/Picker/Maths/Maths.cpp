#include <Picker/Maths/Maths.hpp>

int picker::mod( int a, int b )
{
	int remainder = a % b;
	if( remainder < 0 )
	{
		remainder += b;
	}
	return remainder;
}
