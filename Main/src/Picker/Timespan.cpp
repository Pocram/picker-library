#include <Picker/Timespan.hpp>

picker::Timespan::Timespan() : 
	_microseconds( 0 )
{
}

picker::Timespan picker::Timespan::fromSeconds( float seconds )
{
	picker::Timespan timespan;
	timespan._microseconds = seconds *	1000000;
	return timespan;
}

picker::Timespan picker::Timespan::fromMilliseconds( long milliseconds )
{
	picker::Timespan timespan;
	timespan._microseconds = milliseconds * 1000;
	return timespan;
}

picker::Timespan picker::Timespan::fromMicroseconds( long long microseconds )
{
	picker::Timespan timespan;
	timespan._microseconds = microseconds;
	return timespan;
}

float picker::Timespan::asSeconds() const
{
	return _microseconds / 1000000.0f;
}

long picker::Timespan::asMilliseconds() const
{
	return _microseconds / 1000;
}

long long picker::Timespan::asMicroseconds() const
{
	return _microseconds;
}

bool picker::operator==( const picker::Timespan& left, const picker::Timespan& right )
{
	return left.asMicroseconds() == right.asMicroseconds();
}

bool picker::operator!=( const picker::Timespan& left, const picker::Timespan& right )
{
	return !( left == right );
}

bool picker::operator<( const picker::Timespan& left, const picker::Timespan& right )
{
	return left.asMicroseconds() < right.asMicroseconds();
}

bool picker::operator>( const picker::Timespan& left, const picker::Timespan& right )
{
	return left.asMicroseconds() > right.asMicroseconds();
}

bool picker::operator<=( const picker::Timespan& left, const picker::Timespan& right )
{
	return left.asMicroseconds() <= right.asMicroseconds();
}

bool picker::operator>=( const picker::Timespan& left, const picker::Timespan& right )
{
	return left.asMicroseconds() >= right.asMicroseconds();
}

picker::Timespan picker::operator-( const picker::Timespan& left )
{
	return picker::Timespan::fromMicroseconds( -left.asMicroseconds() );
}

picker::Timespan picker::operator+( const picker::Timespan& left, const picker::Timespan& right )
{
	return picker::Timespan::fromMicroseconds( left.asMicroseconds() + right.asMicroseconds() );
}

picker::Timespan picker::operator-( const picker::Timespan& left, const picker::Timespan& right )
{
	return picker::Timespan::fromMicroseconds( left.asMicroseconds() - right.asMicroseconds() );
}

picker::Timespan picker::operator+=( picker::Timespan& left, const picker::Timespan& right )
{
	return left = left + right;
}

picker::Timespan picker::operator-=( picker::Timespan& left, const picker::Timespan& right )
{
	return left = left - right;
}

picker::Timespan picker::operator*( const picker::Timespan& left, const picker::Timespan& right )
{
	return picker::Timespan::fromMicroseconds( left.asMicroseconds() * right.asMicroseconds() );
}

picker::Timespan picker::operator*( const picker::Timespan& left, float right )
{
	return picker::Timespan::fromSeconds( left.asSeconds() * right );
}

picker::Timespan picker::operator*( const picker::Timespan& left, long right )
{
	return picker::Timespan::fromMilliseconds( left.asMilliseconds() * right );
}

picker::Timespan picker::operator*( const picker::Timespan& left, long long right )
{
	return picker::Timespan::fromMicroseconds( left.asMicroseconds() * right );
}

picker::Timespan picker::operator*( float left, const picker::Timespan& right )
{
	return right * left;
}

picker::Timespan picker::operator*( long left, const picker::Timespan& right )
{
	return right * left;
}

picker::Timespan picker::operator*( long long left, const picker::Timespan& right )
{
	return right * left;
}

picker::Timespan picker::operator*=( picker::Timespan& left, float right )
{
	return left = left * right;
}

picker::Timespan picker::operator*=( picker::Timespan& left, long right )
{
	return left = left * right;
}

picker::Timespan picker::operator*=( picker::Timespan& left, long long right )
{
	return left = left * right;
}

picker::Timespan picker::operator/( const picker::Timespan& left, const picker::Timespan& right )
{
	return picker::Timespan::fromMicroseconds( left.asMicroseconds() * right.asMicroseconds() );
}

picker::Timespan picker::operator/( const picker::Timespan& left, float right )
{
	return picker::Timespan::fromSeconds( left.asSeconds() * right );
}

picker::Timespan picker::operator/( const picker::Timespan& left, long right )
{
	return picker::Timespan::fromMilliseconds( left.asMilliseconds() * right );
}

picker::Timespan picker::operator/( const picker::Timespan& left, long long right )
{
	return picker::Timespan::fromMicroseconds( left.asMicroseconds() * right );
}

picker::Timespan picker::operator/=( picker::Timespan& left, float right )
{
	return left = left / right;
}

picker::Timespan picker::operator/=( picker::Timespan& left, long right )
{
	return left = left / right;
}

picker::Timespan picker::operator/=( picker::Timespan& left, long long right )
{
	return left = left / right;
}
