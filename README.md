# Picker Library
Picker Library is a collection of private C++ libraries that I use for university projects.
## Installation
Since this library has no dependencies as of yet, no installation is needed. Just build the Visual Studio project.
## Contributing
Since this is a private library for private use, you can just copy it and work on it yourself.
## License
TODO: Write license