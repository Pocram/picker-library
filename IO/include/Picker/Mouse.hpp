#ifndef PICKER_IO_MOUSE_HPP
#define PICKER_IO_MOUSE_HPP

#include <Picker/Config.hpp>
#include <Picker/Maths/Vector2.hpp>

namespace picker
{
	class Window;

	////////////////////////////////////////////////////////////
	/// @class picker::Mouse
	/// @brief Handles functions and information related to the user's mouse.
	////////////////////////////////////////////////////////////
	class PICKER_EXPORT Mouse
	{
	public:
		////////////////////////////////////////////////////////////
		/// @brief Represents the buttons of a mouse.
		////////////////////////////////////////////////////////////
		enum Button
		{
			/// The left mouse button.
			Left,
			/// The right mouse button.
			Right,
			/// The middle mouse button.
			Middle
		};

		////////////////////////////////////////////////////////////
		/// @brief Retrieves the cursor position on the screen.
		///
		/// The X and Y coordinates are in pixels.
		////////////////////////////////////////////////////////////
		static picker::Vector2U getCursorPosition();

		////////////////////////////////////////////////////////////
		/// @brief Retrieves the cursor position in a given window.
		///
		/// The X and Y coordinates are in pixels.
		///
		/// @param window The window to relate to.
		////////////////////////////////////////////////////////////
		static picker::Vector2U getCursorPositionInWindow( const picker::Window& window );
	};
}

#endif // PICKER_IO_MOUSE_HPP