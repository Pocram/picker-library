#ifndef PICKER_IO_KEYBOARD_HPP
#define PICKER_IO_KEYBOARD_HPP

#include <Picker/Config.hpp>
#include <Picker/KeyCode.hpp>

namespace picker
{
	////////////////////////////////////////////////////////////
	/// @class picker::Keyboard
	/// @brief Handles functions and information related to the user's keyboard.
	////////////////////////////////////////////////////////////
	class PICKER_EXPORT Keyboard
	{
	public:
		////////////////////////////////////////////////////////////
		/// @brief Checks whether or not a given key is currently pressed
		////////////////////////////////////////////////////////////
		static bool isKeyPressed( picker::KeyCode keyCode );
	};
}

#endif // PICKER_IO_KEYBOARD_HPP