#ifndef PICKER_IO_WINDOW_HPP
#define PICKER_IO_WINDOW_HPP
             
#include <Picker/Config.hpp>

#include <Picker/WindowHandle.hpp>
#include <Picker/WindowEvent.hpp>
#include <Picker/Exceptions/WindowExceptions.hpp>

#include <Picker/Maths/Vector2.hpp>

#if defined( PICKER_SYSTEM_WINDOWS )
	#include <windows.h>
#endif

#include <string>

namespace picker
{
	////////////////////////////////////////////////////////////
	/// @class picker::Window
	/// @brief Handles an empty window.
	////////////////////////////////////////////////////////////
	class PICKER_EXPORT Window
	{
	public:
		////////////////////////////////////////////////////////////
		/// @brief Default constructor.
		////////////////////////////////////////////////////////////
		Window();

		////////////////////////////////////////////////////////////
		/// @brief Constructs a window.
		///
		/// @param width	The window's width in pixels
		/// @param height	The window's height in pixels
		/// @param title	The window's title
		////////////////////////////////////////////////////////////
		Window( unsigned int width, unsigned int height, const ::std::string& title );

		
		////////////////////////////////////////////////////////////
		/// @brief Whether to show or hide to window.
		////////////////////////////////////////////////////////////
		void setVisible( bool isVisible );

		////////////////////////////////////////////////////////////
		/// @brief Gets the window's handle.
		////////////////////////////////////////////////////////////
		picker::WindowHandle getWindowHandle() const;

		////////////////////////////////////////////////////////////
		/// @brief Gets the event on top of the event queue and returns it, if there is one.
		////////////////////////////////////////////////////////////
		bool pollEvent( picker::WindowEvent& outEvent );

		////////////////////////////////////////////////////////////
		/// @brief Wait for an event and return it.
		////////////////////////////////////////////////////////////
		bool waitEvent( picker::WindowEvent& outEvent );

		////////////////////////////////////////////////////////////
		/// @brief Sets the window's title.
		///
		/// @param title	The window's new title.
		////////////////////////////////////////////////////////////
		void setTitle( const ::std::string& title );

		////////////////////////////////////////////////////////////
		/// @brief Gets the window's title.
		////////////////////////////////////////////////////////////
		::std::string getTitle() const;

		////////////////////////////////////////////////////////////
		/// @brief Locks or unlocks the cursor to/from the window.
		///
		/// @param isLockedToWindow	Whether the cursor should be locked to the window or not.
		////////////////////////////////////////////////////////////
		void lockCursorToWindow( bool isLockedToWindow );

		////////////////////////////////////////////////////////////
		/// @brief Shows or hide the cursor.
		///
		/// @param isCursorHidden	Whether the cursor should be hidden or not.
		////////////////////////////////////////////////////////////
		void hideCursor( bool isCursorHidden );


	protected:
		virtual void onSetVisible( bool isVisible );


	private:
		#if defined( PICKER_SYSTEM_WINDOWS )
		static bool registerBasicWindowClass();
		static LRESULT CALLBACK wndProc( HWND windowHandle, UINT message, WPARAM wParam, LPARAM lParam );
		static bool getWindowEventFromMessage( const MSG& message, picker::WindowEvent& outEvent, picker::Window& window );
		static picker::Vector2U getWindowCursorPositionFromLastEvent( const picker::WindowHandle& windowHandle );
		static void handleMouseButtonEvent( picker::WindowEvent::WindowEventType eventType, picker::Mouse::Button button, picker::WindowEvent& event, const picker::WindowHandle& windowHandle );

		static const wchar_t*	_basicWindowClassName;
		static bool				_hasWindowClassBeenRegistered;
		#endif

		picker::WindowHandle _windowHandle;
		bool _isCursorLockedToWindow;
		bool _isCursorHidden;
	};
}

#endif // PICKER_IO_WINDOW_HPP