#ifndef PICKER_IO_WINDOW_EVENT_HPP
#define PICKER_IO_WINDOW_EVENT_HPP

#include <Picker/Config.hpp>

#include <Picker/Mouse.hpp>

namespace picker
{
	class PICKER_EXPORT WindowEvent
	{
	public:

		// Define class internal event structs.
		struct ResizeWindowEvent
		{
			unsigned int newWidth;
			unsigned int newHeight;
		};

		struct MoveWindowEvent
		{
			unsigned int newXPosition;
			unsigned int newYPosition;
		};

		struct KeyWindowEvent
		{
			// TODO: Key code
		};

		struct MouseButtonWindowEvent
		{
			picker::Mouse::Button	mouseButton;
			unsigned int			xPosition;
			unsigned int			yPosition;
		};

		struct MouseMoveEvent
		{
			unsigned int			newXPosition;
			unsigned int			newYPosition;
		};

		enum WindowEventType
		{
			Closed,
			Resized,
			Moved,
			MouseMoved,
			FocusGained,
			FocusLost,
			MouseButtonPressed,
			MouseButtonReleased,
			KeyPressed,
			KeyReleased
		};

		picker::WindowEvent::WindowEventType eventType;

		// Since only one of these will only ever be populated at a time, we can save space by using a union.
		union
		{
			picker::WindowEvent::ResizeWindowEvent		resizeWindowEvent;
			picker::WindowEvent::MoveWindowEvent		moveWindowEvent;
			picker::WindowEvent::KeyWindowEvent			keyWindowEvent;
			picker::WindowEvent::MouseButtonWindowEvent	mouseButtonWindowEvent;
			picker::WindowEvent::MouseMoveEvent			mouseMoveEvent;
		};
	};
}

#endif // PICKER_IO_WINDOW_EVENT_HPP