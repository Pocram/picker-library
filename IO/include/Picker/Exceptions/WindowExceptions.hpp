#ifndef PICKER_IO_EXCEPTIONS_WINDOW_EXCEPTIONS_HPP
#define PICKER_IO_EXCEPTIONS_WINDOW_EXCEPTIONS_HPP

#include <exception>

namespace picker
{
	class WindowNotInitialized : public ::std::exception
	{
	public:
		char const* what() const throw( ) override
		{
			return "The operation can only be used on a properly initialised window";
		}
	};
}

#endif // PICKER_IO_EXCEPTIONS_WINDOW_EXCEPTIONS_HPP