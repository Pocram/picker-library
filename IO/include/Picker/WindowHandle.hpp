#ifndef PICKER_IO_WINDOW_HANDLE_HPP
#define PICKER_IO_WINDOW_HANDLE_HPP

#include <Picker/Config.hpp>

#if defined( PICKER_SYSTEM_WINDOWS )
	struct HWND__;
#endif

namespace picker
{
#if defined( PICKER_SYSTEM_WINDOWS )
	typedef HWND__* WindowHandle;
#endif
}

#endif // PICKER_IO_WINDOW_HANDLE_HPP