#include <Picker/Window.hpp>
#include <iostream>

#if defined( PICKER_SYSTEM_WINDOWS )
	#include <windowsx.h>

	// picker internal window message for closed windows.
	#define PM_CLOSED 100666
	#define PM_SIZE 100667
	#define PM_GAIN_FOCUS 100668
	#define PM_LOSE_FOCUS 100669
	#define PM_MOVE 100670
	#define PM_MOUSEMOVE 100671

	// Static definitions
	const wchar_t* picker::Window::_basicWindowClassName	= L"pickerBasicWindowClass";
	bool picker::Window::_hasWindowClassBeenRegistered		= false;
#endif

picker::Window::Window() :
	_windowHandle( nullptr ),
	_isCursorLockedToWindow( false ),
	_isCursorHidden( false )
{
}

picker::Window::Window( unsigned width, unsigned height, const ::std::string& title ) : 
	_windowHandle( nullptr ),
	_isCursorLockedToWindow( false ),
	_isCursorHidden( false )
{
	#if defined( PICKER_SYSTEM_WINDOWS )
	if( _hasWindowClassBeenRegistered == false )
	{
		registerBasicWindowClass();
	}

	// Convert title to long string.
	::std::wstring t( title.begin(), title.end() );

	_windowHandle = CreateWindowW( _basicWindowClassName, t.c_str(), WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, width, height, nullptr, nullptr, GetModuleHandle( nullptr ), nullptr );
	
	if ( _windowHandle == nullptr )
	{
		throw picker::WindowNotInitialized();
	}
	#endif
}

void picker::Window::setVisible( bool isVisible )
{
	#if defined( PICKER_SYSTEM_WINDOWS )
	if ( _windowHandle == nullptr )
	{
		throw picker::WindowNotInitialized();
	}

	if( isVisible == true )
	{
		ShowWindow( _windowHandle, SW_SHOWNORMAL );
		UpdateWindow( _windowHandle );
	}
	else
	{
		ShowWindow( _windowHandle, SW_HIDE );
	}
	#endif
}

picker::WindowHandle picker::Window::getWindowHandle() const
{
	return _windowHandle;
}

bool picker::Window::pollEvent( picker::WindowEvent& outEvent )
{
	#if defined( PICKER_SYSTEM_WINDOWS )
	MSG msg;

	// "If a message is available, the return value is nonzero. 
	// If no messages are available, the return value is zero." - MSDN
	bool hasMessage = PeekMessage( &msg, getWindowHandle(), 0, 0, PM_REMOVE ) != 0;
	if( hasMessage == false )
	{
		TranslateMessage( &msg );
		DispatchMessage( &msg );
		return false;
	}

	hasMessage = getWindowEventFromMessage( msg, outEvent, *this );
	
	TranslateMessage( &msg );
	DispatchMessage( &msg );

	return hasMessage;
	#endif
}

bool picker::Window::waitEvent( picker::WindowEvent& outEvent )
{
	#if defined( PICKER_SYSTEM_WINDOWS )
	MSG msg;
	
	// "If there is an error, the return value is -1." - MSDN
	bool wasPollSuccessful = GetMessage( &msg, getWindowHandle(), 0, 0 ) != -1;

	// Only handle a message if a message is actually available.
	if ( wasPollSuccessful )
	{
		if ( getWindowEventFromMessage( msg, outEvent, *this ) == false )
		{
			TranslateMessage( &msg );
			DispatchMessage( &msg );
			return false;
		}
	}

	TranslateMessage( &msg );
	DispatchMessage( &msg );

	return wasPollSuccessful;
	#endif
}

void picker::Window::setTitle( const ::std::string& title )
{
	#if defined( PICKER_SYSTEM_WINDOWS )
	if( _windowHandle == nullptr )
	{
		throw picker::WindowNotInitialized();
	}

	::std::wstring t( title.begin(), title.end() );
	SetWindowText( _windowHandle, t.c_str() );
	#endif
}

::std::string picker::Window::getTitle() const
{
	#if defined( PICKER_SYSTEM_WINDOWS )
	if ( _windowHandle == nullptr )
	{
		throw picker::WindowNotInitialized();
	}

	::std::wstring title;
	title.resize( GetWindowTextLength( _windowHandle ) + 1, '\0' );
	GetWindowText( _windowHandle, &title[0], GetWindowTextLength( _windowHandle ) + 1 );
	return ::std::string( title.begin(), title.end() );
	#endif
}

void picker::Window::lockCursorToWindow( bool isLockedToWindow )
{
	_isCursorLockedToWindow = isLockedToWindow;
}

void picker::Window::hideCursor( bool isCursorHidden )
{
	_isCursorHidden = isCursorHidden;
}

void picker::Window::onSetVisible( bool isVisible )
{
	// Used by children.
}

#if defined( PICKER_SYSTEM_WINDOWS )
bool picker::Window::registerBasicWindowClass()
{
	WNDCLASSEX windowClass;

	windowClass.cbSize			= sizeof( windowClass );
	windowClass.style			= CS_HREDRAW | CS_VREDRAW;
	windowClass.lpfnWndProc		= wndProc;
	windowClass.cbClsExtra		= 0;
	windowClass.cbWndExtra		= 0;
	windowClass.hInstance		= GetModuleHandle( nullptr );
	windowClass.hIcon			= LoadIcon( nullptr, IDI_APPLICATION );
	windowClass.hCursor			= LoadCursor( nullptr, IDC_ARROW );
	windowClass.hbrBackground	= reinterpret_cast<HBRUSH>( COLOR_WINDOW + 1 );
	windowClass.lpszMenuName	= nullptr;
	windowClass.lpszClassName	= _basicWindowClassName;
	windowClass.hIconSm			= LoadIcon( nullptr, IDI_APPLICATION );

	if ( RegisterClassEx( &windowClass ) == false )
	{
		MessageBox( nullptr, L"Window registration failed!", L"Error", MB_ICONEXCLAMATION | MB_OK );
		return false;
	}

	_hasWindowClassBeenRegistered = true;
	return true;
}

LRESULT picker::Window::wndProc( HWND windowHandle, UINT message, WPARAM wParam, LPARAM lParam )
{
	switch ( message )
	{
		case WM_CREATE:
		{
			// TODO: Inform children.
			return 0;
		}
		case WM_PAINT:
		{
			PAINTSTRUCT ps;
			HDC deviceContextHandle = BeginPaint( windowHandle, &ps );

			FillRect( deviceContextHandle, &ps.rcPaint, reinterpret_cast<HBRUSH>( COLOR_WINDOW + 1 ) );
			EndPaint( windowHandle, &ps );
			return 0;
		}
		case WM_CLOSE:
		{
			// This is quite the hack.
			// Send 'WM_CLOSE' to message queue which in turn calls wndProc with the 'PM_CLOSED' message.
			// This then closes the window.
			PostMessage( windowHandle, WM_CLOSE, wParam, lParam );
			return 0;
		}
		case WM_MOUSEMOVE:
		{
			PostMessage( windowHandle, PM_MOUSEMOVE, wParam, lParam );
			return DefWindowProc( windowHandle, message, wParam, lParam );
		}
		case WM_SIZE:
		{
			PostMessage( windowHandle, PM_SIZE, wParam, lParam );
			return DefWindowProc( windowHandle, message, wParam, lParam );
		}
		case WM_SETFOCUS:
		{
			PostMessage( windowHandle, PM_GAIN_FOCUS, wParam, lParam );
			return DefWindowProc( windowHandle, message, wParam, lParam );
		}
		case WM_KILLFOCUS:
		{
			PostMessage( windowHandle, PM_LOSE_FOCUS, wParam, lParam );
			return DefWindowProc( windowHandle, message, wParam, lParam );
		}
		case WM_MOVE:
		{
			PostMessage( windowHandle, PM_MOVE, wParam, lParam );
			return DefWindowProc( windowHandle, message, wParam, lParam );
		}
		case PM_CLOSED:
		{
			return DefWindowProc( windowHandle, WM_CLOSE, wParam, lParam );
		}
		default:
		{
			return DefWindowProc( windowHandle, message, wParam, lParam );
		}
	}
}

bool picker::Window::getWindowEventFromMessage( const MSG& message, picker::WindowEvent& outEvent, picker::Window& window )
{
	bool returnValue = false;

	picker::WindowEvent e = {};
	switch ( message.message )
	{
		case WM_CLOSE:
		{
			e.eventType = picker::WindowEvent::WindowEventType::Closed;
			outEvent = e;
			wndProc( message.hwnd, PM_CLOSED, message.wParam, message.lParam );
			returnValue = true;
			break;
		}
		case WM_MOUSEMOVE:
		{
			e.eventType = picker::WindowEvent::WindowEventType::MouseMoved;
			e.mouseMoveEvent.newXPosition = GET_X_LPARAM( message.lParam );
			e.mouseMoveEvent.newYPosition = GET_Y_LPARAM( message.lParam );
			outEvent = e;
			returnValue = true;
			break;
		}
		case WM_LBUTTONDOWN:
		{
			handleMouseButtonEvent( picker::WindowEvent::WindowEventType::MouseButtonPressed, picker::Mouse::Button::Left, e, message.hwnd );
			outEvent = e;
			returnValue = true;
			break;
		}
		case WM_RBUTTONDOWN:
		{
			handleMouseButtonEvent( picker::WindowEvent::WindowEventType::MouseButtonPressed, picker::Mouse::Button::Right, e, message.hwnd );
			outEvent = e;
			returnValue = true;
			break;
		}
		case WM_MBUTTONDOWN:
		{
			handleMouseButtonEvent( picker::WindowEvent::WindowEventType::MouseButtonPressed, picker::Mouse::Button::Middle, e, message.hwnd );
			returnValue = true;
			break;
		}
		case WM_LBUTTONUP:
		{
			handleMouseButtonEvent( picker::WindowEvent::WindowEventType::MouseButtonReleased, picker::Mouse::Button::Left, e, message.hwnd );
			outEvent = e;
			returnValue = true;
			break;
		}
		case WM_RBUTTONUP:
		{
			handleMouseButtonEvent( picker::WindowEvent::WindowEventType::MouseButtonReleased, picker::Mouse::Button::Right, e, message.hwnd );
			outEvent = e;
			returnValue = true;
			break;
		}
		case WM_MBUTTONUP:
		{
			handleMouseButtonEvent( picker::WindowEvent::WindowEventType::MouseButtonReleased, picker::Mouse::Button::Middle, e, message.hwnd );
			returnValue = true;
			break;
		}
		case WM_KEYDOWN:
		{
			e.eventType = picker::WindowEvent::WindowEventType::KeyPressed;
			outEvent = e;
			returnValue = true;
			break;
		}
		case WM_KEYUP:
		{
			returnValue = true;
			break;
		}
		case PM_SIZE:
		{
			e.eventType = picker::WindowEvent::WindowEventType::Resized;
			e.resizeWindowEvent.newWidth = 100.0f;
			e.resizeWindowEvent.newHeight = 100.0f;
			outEvent = e;
			returnValue = true;
			break;
		}
		case PM_MOVE:
		{
			e.eventType = picker::WindowEvent::WindowEventType::Moved;
			e.moveWindowEvent.newXPosition = GET_X_LPARAM( message.lParam );
			e.moveWindowEvent.newYPosition = GET_Y_LPARAM( message.lParam );
			outEvent = e;
			returnValue = true;
			break;
		}
		case PM_GAIN_FOCUS:
		{
			e.eventType = picker::WindowEvent::WindowEventType::FocusGained;
			outEvent = e;
			returnValue = true;
			break;
		}
		case PM_LOSE_FOCUS:
		{
			e.eventType = picker::WindowEvent::WindowEventType::FocusLost;
			outEvent = e;
			returnValue = true;
			break;
		}
		default:
		{
			// Do nothing! The event is invalid or unsupported.
			returnValue = false;
			break;
		}
	}

	if ( window._isCursorLockedToWindow )
	{
		RECT windowRect;
		GetWindowRect( window.getWindowHandle(), &windowRect );

		int width = windowRect.right - windowRect.left;
		int height = windowRect.bottom - windowRect.top;

		RECT lockRect;
		lockRect.top = windowRect.top + height / 2;
		lockRect.bottom = lockRect.top;
		lockRect.left = windowRect.left + width / 2;
		lockRect.right = lockRect.left;
		ClipCursor( &lockRect );
	}

	if ( window._isCursorHidden )
	{
		ShowCursor( false );
	}
	else
	{
		ShowCursor( true );
	}

	return returnValue;
}

picker::Vector2U picker::Window::getWindowCursorPositionFromLastEvent( const picker::WindowHandle& windowHandle )
{
	DWORD combinedPosition = GetMessagePos();
	POINT p;
	p.x = GET_X_LPARAM( combinedPosition );
	p.y = GET_Y_LPARAM( combinedPosition );
	
	if ( ScreenToClient( windowHandle, &p ) == false )
	{
		return picker::Vector2U( 0, 0 );
	}

	return picker::Vector2U( p.x, p.y );
}

void picker::Window::handleMouseButtonEvent( picker::WindowEvent::WindowEventType eventType, picker::Mouse::Button button, picker::WindowEvent& event, const picker::WindowHandle& windowHandle )
{
	event.eventType = eventType;
	event.mouseButtonWindowEvent.mouseButton = button;
	
	const picker::Vector2U cursorPositionInWindow = getWindowCursorPositionFromLastEvent( windowHandle );
	event.mouseButtonWindowEvent.xPosition = cursorPositionInWindow.x;
	event.mouseButtonWindowEvent.yPosition = cursorPositionInWindow.y;
}
#endif
