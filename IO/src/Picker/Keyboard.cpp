#include <Picker/Keyboard.hpp>

#if defined( PICKER_SYSTEM_WINDOWS )
	#include <windows.h>
#endif

bool picker::Keyboard::isKeyPressed( picker::KeyCode keyCode )
{
	#if defined( PICKER_SYSTEM_WINDOWS )
		return GetAsyncKeyState( keyCode );
	#else
		return false;
	#endif
}