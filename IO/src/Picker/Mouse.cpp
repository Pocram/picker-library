#include <Picker/Mouse.hpp>

#if defined( PICKER_SYSTEM_WINDOWS )
	#include <windows.h>
#endif

#include <Picker/Window.hpp>

picker::Vector2U picker::Mouse::getCursorPosition()
{
	picker::Vector2U position( 0, 0 );
	
	#if defined( PICKER_SYSTEM_WINDOWS )
	POINT p;
	if( GetCursorPos( &p ) )
	{
		position.x = p.x;
		position.y = p.y;
	}
	#endif
	
	return position;
}

picker::Vector2U picker::Mouse::getCursorPositionInWindow( const picker::Window& window )
{
	picker::Vector2U position = picker::Mouse::getCursorPosition();
	
	#if defined( PICKER_SYSTEM_WINDOWS )
	POINT p;
	p.x = position.x;
	p.y = position.y;
	if ( ScreenToClient( window.getWindowHandle(), &p ) )
	{
		position.x = p.x;
		position.y = p.y;
	}
	#endif
	
	return position;
}
