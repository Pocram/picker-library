#ifndef PICKER_OPEN_GL_I_RENDER_TARGET_HPP
#define PICKER_OPEN_GL_I_RENDER_TARGET_HPP

#include <Picker/Config.hpp>
#include <Picker/OpenGL/Transform.hpp>

namespace picker
{
	namespace ogl
	{
		class Camera;
		class Transform;
		class Material;
		class IDrawable;

		////////////////////////////////////////////////////////////
		/// @class picker::ogl::IRenderTarget
		/// @brief An interface that allows the child to be drawn on.
		////////////////////////////////////////////////////////////
		class PICKER_EXPORT IRenderTarget
		{
		public:
			////////////////////////////////////////////////////////////
			/// @brief Virtual destructor.
			////////////////////////////////////////////////////////////
			virtual ~IRenderTarget();


			////////////////////////////////////////////////////////////
			/// @brief Draws the drawable object on the render target.
			///
			/// @param drawableObject	The object to draw.
			////////////////////////////////////////////////////////////
			void draw( const IDrawable& drawableObject );

			////////////////////////////////////////////////////////////
			/// @brief Draws the drawable object on the render target.
			///
			/// @param drawableObject	The object to draw.
			/// @param material			The material to use.
			/// @param camera			The camera that sees the object.
			/// @param transform		The object's transform component.
			////////////////////////////////////////////////////////////
			void draw( const IDrawable& drawableObject, const picker::ogl::Material& material, const picker::ogl::Camera& camera, const picker::ogl::Transform& transform = picker::ogl::Transform() );


		private:
			unsigned int _modelViewProjectionMatrixId;	///< The ID of the MPV is used by OpenGL to send the data to the shaders.
		};
	}
}

#endif // PICKER_OPEN_GL_RENDER_TARGET_HPP