#ifndef PICKER_OPEN_GL_SHADER_UTILITY_HPP
#define PICKER_OPEN_GL_SHADER_UTILITY_HPP

#include <Picker/Config.hpp>
#include <string>

namespace picker
{
	namespace ogl
	{
		////////////////////////////////////////////////////////////
		/// @enum picker::ogl::ShaderType
		/// @brief The type of a shader; either vertex or fragment.
		////////////////////////////////////////////////////////////
		PICKER_EXPORT enum ShaderType 
		{
			Vertex,
			Fragment
		};

		////////////////////////////////////////////////////////////
		/// @brief Compiles a shader.
		///
		/// Compiles a shader and creates an ID that can be used by OpenGL functions.
		///
		/// @param pathToShaderFile	The path to the shader file.
		/// @param shaderType		The shader's type. Either vertex or fragment.
		/// @param outShaderId		The shader ID that is stored in an external variable. 
		///							The ID used by OpenGL functions.
		////////////////////////////////////////////////////////////
		bool compileShader( const ::std::string& pathToShaderFile, ShaderType shaderType, unsigned int& outShaderId );

		////////////////////////////////////////////////////////////
		/// @brief Compiles a shader.
		///
		/// Compiles a shader and creates an ID that can be used by OpenGL functions.
		///
		/// @param pathToShaderFile	The path to the shader file.
		/// @param shaderType		The shader's type. Either vertex or fragment.
		/// @param outShaderId		The shader ID that is stored in an external variable. 
		///							The ID used by OpenGL functions.
		/// @param outErrorMessage	In case the compilation fails, you can use this to store
		///							the error message in an external string.
		////////////////////////////////////////////////////////////
		bool compileShader( const ::std::string& pathToShaderFile, ShaderType shaderType, unsigned int& outShaderId, ::std::string& outErrorMessage );

		////////////////////////////////////////////////////////////
		/// @brief Links two shaders together.
		///
		/// Links two shaders together and creates an OpenGL ID for the
		/// shader program.
		///
		/// @param vertexShaderId		The vertex shader's OpenGL ID.
		/// @param fragmentShaderId		The vertex shader's OpenGL ID.
		/// @param outShaderProgramId	The shader program ID that is stored in an external variable. 
		///								The ID used by OpenGL functions.
		////////////////////////////////////////////////////////////
		bool linkShaders( int vertexShaderId, int fragmentShaderId, unsigned int& outShaderProgramId );

		////////////////////////////////////////////////////////////
		/// @brief Links two shaders together.
		///
		/// Links two shaders together and creates an OpenGL ID for the
		/// shader program.
		///
		/// @param vertexShaderId		The vertex shader's OpenGL ID.
		/// @param fragmentShaderId		The vertex shader's OpenGL ID.
		/// @param outShaderProgramId	The shader program ID that is stored in an external variable. 
		///								The ID used by OpenGL functions.
		/// @param outErrorMessage		In case the link fails, you can use this to store
		///								the error message in an external string.
		////////////////////////////////////////////////////////////
		bool linkShaders( int vertexShaderId, int fragmentShaderId, unsigned int& outShaderProgramId, ::std::string& outErrorMessage );
	}
}

#endif // PICKER_OPEN_GL_SHADER_UTILITY_HPP