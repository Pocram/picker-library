#ifndef PICKER_OPEN_GL_MESH_HPP
#define PICKER_OPEN_GL_MESH_HPP

#include <Picker/Config.hpp>
#include <Picker/OpenGL/Vertex.hpp>
#include <Picker/OpenGL/Transformable.hpp>
#include <Picker/OpenGL/IDrawable.hpp>
#include <Picker/OpenGL/VertexBuffer.hpp>

#include <vector>

namespace picker
{
	namespace ogl
	{
		////////////////////////////////////////////////////////////
		/// @class picker::ogl::Mesh
		/// @brief Handles a mesh that is made of vertices.
		////////////////////////////////////////////////////////////
		class PICKER_EXPORT Mesh :
			public picker::ogl::Transformable,
			public picker::ogl::IDrawable
		{
			////////////////////////////////////////////////////////////
			/// @struct picker::ogl::Mesh::VertexIndeces
			/// @brief Contains indeces that point to vertex relevant data.
			////////////////////////////////////////////////////////////
			struct VertexIndeces
			{
				unsigned int positionIndex;
				unsigned int uvCoordinateIndex;
				unsigned int normalIndex;
			};

			////////////////////////////////////////////////////////////
			/// @struct picker::ogl::Mesh::VertexIndexTriple
			/// @brief Contains three picker::ogl::Mesh::VertexIndeces that form a triangle.
			////////////////////////////////////////////////////////////
			struct VertexIndexTriple
			{
				picker::ogl::Mesh::VertexIndeces a;
				picker::ogl::Mesh::VertexIndeces b;
				picker::ogl::Mesh::VertexIndeces c;
			};

		public:
			////////////////////////////////////////////////////////////
			/// @brief Default constructor.
			////////////////////////////////////////////////////////////
			Mesh();

			////////////////////////////////////////////////////////////
			/// @brief Loads a mesh from a file.
			///
			/// Currently supported file types are: 
			///  - .obj
			/// The mesh object is not altered if loading fails.
			///
			/// @param path The full path to the mesh file.
			///
			/// @returns Whether or not the mesh could be loaded.
			////////////////////////////////////////////////////////////
			bool loadFromFile( const ::std::string& path );


		protected:
			////////////////////////////////////////////////////////////
			/// @brief Draws the object to the render target.
			////////////////////////////////////////////////////////////
			void draw( IRenderTarget& target ) const override;


		private:
			::std::vector<picker::Vector3F>						_vertexPositions;	///< A list of all individual vertex positions.
			::std::vector<picker::Vector2F>						_uvCoordinates;		///< A list of all individual UV coordinates.
			::std::vector<picker::Vector3F>						_normals;			///< A list of all individual normal vectors.
			::std::vector<picker::ogl::Mesh::VertexIndexTriple>	_triangles;			///< A container of all the mesh's triangles.
			//picker::ogl::VertexBuffer							_vertexBuffer;
		};
	}
}

#endif // PICKER_OPEN_GL_MESH_HPP