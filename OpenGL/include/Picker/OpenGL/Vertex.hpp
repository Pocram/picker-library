#ifndef PICKER_OPEN_GL_VERTEX_HPP
#define PICKER_OPEN_GL_VERTEX_HPP

#include <Picker/Config.hpp>

#include <Picker/Maths/Vector2.hpp>
#include <Picker/Maths/Vector3.hpp>
#include <Picker/Color.hpp>

namespace picker
{
	namespace ogl
	{
		struct SortedVertex;

		////////////////////////////////////////////////////////////
		/// @class picker::ogl::Vertex
		/// @brief Represents a vertex for use in 3D applications.
		////////////////////////////////////////////////////////////
		class PICKER_EXPORT Vertex
		{
		public:
			////////////////////////////////////////////////////////////
			/// @brief Default constructor.
			///
			/// Creates a white point at (0, 0, 0) with no normal and UVs.
			////////////////////////////////////////////////////////////
			Vertex();

			////////////////////////////////////////////////////////////
			/// @brief Constructs a vertex.
			///
			/// @param position	The vertex' position in space.
			////////////////////////////////////////////////////////////
			Vertex( const picker::Vector3F& position );
			
			////////////////////////////////////////////////////////////
			/// @brief Constructs a vertex.
			///
			/// @param position	The vertex' position in space.
			/// @param normal	The vertex' normal vector.
			////////////////////////////////////////////////////////////
			Vertex( const picker::Vector3F& position, const picker::Vector3F normal );

			////////////////////////////////////////////////////////////
			/// @brief Constructs a vertex.
			///
			/// @param position	The vertex' position in space.
			/// @param color	The vertex' color.
			////////////////////////////////////////////////////////////
			Vertex( const picker::Vector3F& position, const picker::Color& color );

			////////////////////////////////////////////////////////////
			/// @brief Constructs a vertex.
			///
			/// @param position	The vertex' position in space.
			/// @param color	The vertex' colour.
			/// @param normal	The vertex' normal vector.
			////////////////////////////////////////////////////////////
			Vertex( const picker::Vector3F& position, const picker::Color& color, const picker::Vector3F normal );

			////////////////////////////////////////////////////////////
			/// @brief Constructs a vertex.
			///
			/// @param position		The vertex' position in space.
			/// @param color		The vertex' colour.
			/// @param uvCoordinate	The vertex' texture's UV coordinate.
			////////////////////////////////////////////////////////////
			Vertex( const picker::Vector3F& position, const picker::Color& color, const picker::Vector2F uvCoordinate );

			////////////////////////////////////////////////////////////
			/// @brief Constructs a vertex.
			///
			/// @param position		The vertex' position in space.
			/// @param color		The vertex' colour.
			/// @param normal		The vertex' normal vector.
			/// @param uvCoordinate	The vertex' texture's UV coordinate.
			////////////////////////////////////////////////////////////
			Vertex( const picker::Vector3F& position, const picker::Color& color, const picker::Vector3F normal, const picker::Vector2F uvCoordinate );

			////////////////////////////////////////////////////////////
			/// @brief Constructs a vertex.
			///
			/// @param position		The vertex' position in space.
			/// @param normal		The vertex' normal vector.
			/// @param uvCoordinate	The vertex' texture's UV coordinate.
			////////////////////////////////////////////////////////////
			Vertex( const picker::Vector3F& position, const picker::Vector3F normal, const picker::Vector2F uvCoordinate );

			
			////////////////////////////////////////////////////////////
			/// @brief Gets the vertex' current position.
			////////////////////////////////////////////////////////////
			picker::Vector3F getPostion() const;

			////////////////////////////////////////////////////////////
			/// @brief Sets the vertex' position.
			/// @param position The vertex' new position.
			////////////////////////////////////////////////////////////
			void setPosition( const picker::Vector3F& position );

			////////////////////////////////////////////////////////////
			/// @brief Gets the vertex' current color.
			////////////////////////////////////////////////////////////
			picker::Color getColor() const;

			////////////////////////////////////////////////////////////
			/// @brief Sets the vertex' color.
			/// @param color The vertex' new color.
			////////////////////////////////////////////////////////////
			void setColor( const picker::Color& color );

			////////////////////////////////////////////////////////////
			/// @brief Gets the vertex' current normal.
			////////////////////////////////////////////////////////////
			picker::Vector3F getNormal() const;

			////////////////////////////////////////////////////////////
			/// @brief Sets the vertex' vector.
			/// @param normal The vertex' new normal vector.
			////////////////////////////////////////////////////////////
			void setNormal( const picker::Vector3F& normal );

			////////////////////////////////////////////////////////////
			/// @brief Gets the vertex' current UV coordinate.
			////////////////////////////////////////////////////////////
			picker::Vector2F getUvCoordinate() const;

			////////////////////////////////////////////////////////////
			/// @brief Sets the vertex' coordinate.
			/// @param uvCoordinate The vertex' new UV coordinate.
			////////////////////////////////////////////////////////////
			void setUvCoordinate( const picker::Vector2F& uvCoordinate );

			////////////////////////////////////////////////////////////
			/// @brief Converts the vertex to a sorted vertex.
			////////////////////////////////////////////////////////////
			picker::ogl::SortedVertex toSorted() const;


		private:
			picker::Vector3F	_position;
			picker::Color		_color;
			picker::Vector3F	_normal;
			picker::Vector2F	_uvCoordinate;
		};

		struct PICKER_EXPORT SortedVertex
		{
		public:
			////////////////////////////////////////////////////////////
			/// @brief Default constructor.
			///
			/// Creates a white point at (0, 0, 0) with no normal and UVs.
			////////////////////////////////////////////////////////////
			SortedVertex();

			////////////////////////////////////////////////////////////
			/// @brief Constructs a vertex.
			///
			/// @param position	The vertex' position in space.
			////////////////////////////////////////////////////////////
			SortedVertex( const picker::Vector3F& position );

			////////////////////////////////////////////////////////////
			/// @brief Constructs a vertex.
			///
			/// @param position	The vertex' position in space.
			/// @param color	The vertex' color.
			////////////////////////////////////////////////////////////
			SortedVertex( const picker::Vector3F& position, const picker::Color& color );

			////////////////////////////////////////////////////////////
			/// @brief Constructs a vertex.
			///
			/// @param position	The vertex' position in space.
			/// @param color	The vertex' colour.
			/// @param normal	The vertex' normal vector.
			////////////////////////////////////////////////////////////
			SortedVertex( const picker::Vector3F& position, const picker::Color& color, const picker::Vector3F normal );

			////////////////////////////////////////////////////////////
			/// @brief Constructs a vertex.
			///
			/// @param position		The vertex' position in space.
			/// @param color		The vertex' colour.
			/// @param uvCoordinate	The vertex' texture's UV coordinate.
			////////////////////////////////////////////////////////////
			SortedVertex( const picker::Vector3F& position, const picker::Color& color, const picker::Vector2F uvCoordinate );

			////////////////////////////////////////////////////////////
			/// @brief Constructs a vertex.
			///
			/// @param position		The vertex' position in space.
			/// @param color		The vertex' colour.
			/// @param normal		The vertex' normal vector.
			/// @param uvCoordinate	The vertex' texture's UV coordinate.
			////////////////////////////////////////////////////////////
			SortedVertex( const picker::Vector3F& position, const picker::Color& color, const picker::Vector3F normal, const picker::Vector2F uvCoordinate );


			float xPosition;
			float yPosition;
			float zPosition;

			float uTextureCoordinate;
			float vTextureCoordinate;

			float normalX;
			float normalY;
			float normalZ;

			float colorR;
			float colorG;
			float colorB;
		};
	}
}

#endif // PICKER_OPEN_GL_VERTEX_HPP