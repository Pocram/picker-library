#ifndef PICKER_OPEN_GL_SHADERS_HPP
#define PICKER_OPEN_GL_SHADERS_HPP

#include <Picker/OpenGL/VertexShader.hpp>
#include <Picker/OpenGL/FragmentShader.hpp>

#endif // PICKER_OPEN_GL_SHADERS_HPP