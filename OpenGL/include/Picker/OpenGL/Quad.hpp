#ifndef PICKER_OPEN_GL_QUAD_HPP
#define PICKER_OPEN_GL_QUAD_HPP

#include <Picker/Config.hpp>
#include <Picker/OpenGL/Transformable.hpp>
#include <Picker/OpenGL/IDrawable.hpp>
#include <Picker/Maths/Vector3.hpp>
#include <Picker/OpenGL/Vertex.hpp>

#include <array>

namespace picker
{
	namespace ogl
	{
		////////////////////////////////////////////////////////////
		/// @class picker::ogl::Quad
		/// @brief Represents a quad that is made of four vertices.
		////////////////////////////////////////////////////////////
		class PICKER_EXPORT Quad :
			public picker::ogl::Transformable,
			public picker::ogl::IDrawable
		{
		public:
			////////////////////////////////////////////////////////////
			/// @brief Default constructor.
			////////////////////////////////////////////////////////////
			Quad();

			////////////////////////////////////////////////////////////
			/// @brief Constructs a quad.
			///
			/// @param pointA The position of the first vertex.
			/// @param pointB The position of the second vertex.
			/// @param pointC The position of the third vertex.
			/// @param pointD The position of the fourth vertex.
			////////////////////////////////////////////////////////////
			Quad( const picker::Vector3F& pointA, const picker::Vector3F& pointB, const picker::Vector3F& pointC, const picker::Vector3F& pointD );

			////////////////////////////////////////////////////////////
			/// @brief Constructs a quad.
			///
			/// @param vertices A list of all four vertex positions.
			////////////////////////////////////////////////////////////
			Quad( const ::std::array<picker::Vector3F, 4>& vertices );

			////////////////////////////////////////////////////////////
			/// @brief Constructs a quad.
			///
			/// @param vertexA The first vertex.
			/// @param vertexB The second vertex.
			/// @param vertexC The third vertex.
			/// @param vertexD The fourth vertex.
			////////////////////////////////////////////////////////////
			Quad( const picker::ogl::Vertex& vertexA, const picker::ogl::Vertex& vertexB, const picker::ogl::Vertex& vertexC, const picker::ogl::Vertex& vertexD );

			////////////////////////////////////////////////////////////
			/// @brief Constructs a quad.
			///
			/// @param vertices A list of all four vertices.
			////////////////////////////////////////////////////////////
			Quad( const ::std::array<picker::ogl::Vertex, 4>& vertices );


			////////////////////////////////////////////////////////////
			/// @brief Gets a vertex of the quad.
			///
			/// @param index The vertex' index. Ranges from 0 to 3.
			////////////////////////////////////////////////////////////
			const picker::ogl::Vertex& getVertex( unsigned int index ) const;

			////////////////////////////////////////////////////////////
			/// @brief Gets a vertex of the quad.
			///
			/// @param index The vertex' index. Ranges from 0 to 3.
			////////////////////////////////////////////////////////////
			picker::ogl::Vertex& getVertex( unsigned int index );

			////////////////////////////////////////////////////////////
			/// @brief Sets a vertex of the quad.
			///
			/// @param index	The vertex' index. Ranges from 0 to 3.
			/// @param vertex	The vertex data.
			////////////////////////////////////////////////////////////
			void setVertex( unsigned int index, const picker::ogl::Vertex& vertex );


		protected:
			////////////////////////////////////////////////////////////
			/// @brief Draws the object to the render target.
			////////////////////////////////////////////////////////////
			void draw( IRenderTarget& target ) const override;


		private:
			::std::array<picker::ogl::Vertex, 4> _vertices; ///< All four vertices.
		};
	}
}

#endif // PICKER_OPEN_GL_QUAD_HPP