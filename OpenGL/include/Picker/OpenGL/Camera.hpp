#ifndef PICKER_OPEN_GL_CAMERA_HPP
#define PICKER_OPEN_GL_CAMERA_HPP

#include <Picker/Config.hpp>
#include <Picker/Maths/Vector3.hpp>
#include <Picker/OpenGL/Transformable.hpp>

#include <glm/glm.hpp>

namespace picker
{
	namespace ogl
	{
		////////////////////////////////////////////////////////////
		/// @class picker::ogl::Camera
		/// @brief Represents a camera that is used to render an object.
		////////////////////////////////////////////////////////////
		class PICKER_EXPORT Camera
		{
		public:
			////////////////////////////////////////////////////////////
			/// @brief Default constructor.
			////////////////////////////////////////////////////////////
			Camera();

			////////////////////////////////////////////////////////////
			/// @brief Constructs a camera for three-dimensional rendering.
			///
			/// @param position				The camera's position in 3D space.
			/// @param aspectRatio			The camera's aspect ratio. Width / Height.
			/// @param fieldOfView			The camera's field of view in degrees.
			/// @param nearClippingDistance	The camera's near clipping distance. Anything that is closer to the camera than this will not be rendered.
			/// @param farClippingDistance	The camera's far clipping distance. Anything that is further away from the camera than this will not be rendered.
			////////////////////////////////////////////////////////////
			Camera( const picker::Vector3F& position, float aspectRatio, float fieldOfView = 90.0f, float nearClippingDistance = 0.1f, float farClippingDistance = 100.0f );


			////////////////////////////////////////////////////////////
			/// @brief Sets the camera's position.
			/// @param position	The new position in world space.
			////////////////////////////////////////////////////////////
			void setPosition( const picker::Vector3F& position );

			////////////////////////////////////////////////////////////
			/// @brief Gets the camera's position.
			////////////////////////////////////////////////////////////
			picker::Vector3F getPosition() const;

			////////////////////////////////////////////////////////////
			/// @brief Gets the camera's projection matrix.
			////////////////////////////////////////////////////////////
			const glm::mat4& getProjectionMatrix() const;

			////////////////////////////////////////////////////////////
			/// @brief Gets the camera's view matrix.
			////////////////////////////////////////////////////////////
			const glm::mat4& getViewMatrix() const;

			////////////////////////////////////////////////////////////
			/// @brief Rotates the camera so that it looks at a given position.
			///
			/// @param target	The target position to look at.
			/// @param up		The direction of the camera's "up" side.
			////////////////////////////////////////////////////////////
			void lookAt( const picker::Vector3F& target, const picker::Vector3F up = picker::Vector3F( 0.0f, 1.0f, 0.0f ) );

			////////////////////////////////////////////////////////////
			/// @brief Rotates the camera so that it looks in a given direction.
			///
			/// @param direction	The target direction to look in.
			/// @param up			The direction of the camera's "up" side.
			////////////////////////////////////////////////////////////
			void lookInDirection( const picker::Vector3F& direction, const picker::Vector3F up = picker::Vector3F( 0.0f, 1.0f, 0.0f ) );

			////////////////////////////////////////////////////////////
			/// @brief Sets the camera's aspect ratio.
			///
			/// The aspect ratio is usually calculated by dividing the window's width by its height.
			///
			/// @param aspectRatio The new aspect ratio.
			////////////////////////////////////////////////////////////
			void setAspectRatio( float aspectRatio );

			////////////////////////////////////////////////////////////
			/// @brief Sets the camera's field of view.
			/// @param fieldOfView The new field of view.
			////////////////////////////////////////////////////////////
			void setFieldOfView( float fieldOfView );


		private:
			void updateProjectionMatrix();

			glm::mat4	_projectionMatrix;
			glm::mat4	_viewMatrix;
			float		_aspectRatio;
			float		_fieldOfView;

			picker::Vector3F _position;
			picker::Vector3F _viewDirection;
		};
	}
}

#endif // PICKER_OPEN_GL_CAMERA_HPP