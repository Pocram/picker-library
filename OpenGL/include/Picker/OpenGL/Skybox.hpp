#ifndef PICKER_OPEN_GL_SKYBOX_HPP
#define PICKER_OPEN_GL_SKYBOX_HPP

#include <Picker/Config.hpp>
#include <Picker/OpenGL/IDrawable.hpp>
#include <Picker/OpenGL/VertexBuffer.hpp>

namespace picker
{
	namespace ogl
	{
		class PICKER_EXPORT Skybox : public picker::ogl::IDrawable
		{
		public:
			Skybox();

		protected:
			void draw( IRenderTarget& target ) const override;

		private:
			void generateVertexBuffer();

			picker::ogl::VertexBuffer _vertexBuffer;
		};
	}
}

#endif // PICKER_OPEN_GL_SKYBOX_HPP