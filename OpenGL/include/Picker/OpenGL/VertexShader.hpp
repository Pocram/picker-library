#ifndef PICKER_OPEN_GL_VERTEX_SHADER_HPP
#define PICKER_OPEN_GL_VERTEX_SHADER_HPP

#include <Picker/Config.hpp>
#include <Picker/OpenGL/Shader.hpp>
#include <Picker/OpenGL/Exceptions/ShaderExceptions.hpp>

#include <string>

namespace picker
{
	namespace ogl
	{
		////////////////////////////////////////////////////////////
		/// @class picker::ogl::VertexShader
		/// @brief Represents a vertex shader.
		////////////////////////////////////////////////////////////
		class PICKER_EXPORT VertexShader : public picker::ogl::Shader
		{
		public:
			////////////////////////////////////////////////////////////
			/// @brief Default constructor.
			////////////////////////////////////////////////////////////
			VertexShader();
			
			////////////////////////////////////////////////////////////
			/// @brief Loads and compiles a shader.
			///
			/// @param pathToShaderFile The path to the file that contains the shader code.
			/// 
			/// @throws picker::ogl::ShaderFailedToCompile	Thrown if the shader failed to compile.
			///												Use picker::ogl::VertexShader::getCompilationErrorMessage()
			///												to retrieve the error message.
			////////////////////////////////////////////////////////////
			VertexShader( const ::std::string& pathToShaderFile );


			////////////////////////////////////////////////////////////
			/// @brief Loads and compiles a shader.
			///
			/// Use this method if you cannot use the 
			/// picker::ogl::VertexShader::VertexShader( const ::std::string& pathToShaderFile ) constructor.
			///
			/// @param pathToShaderFile The path to the file that contains the shader code.
			////////////////////////////////////////////////////////////
			bool loadFromFile( const ::std::string& pathToShaderFile );
		};
	}
}

#endif // PICKER_OPEN_GL_VERTEX_SHADER_HPP