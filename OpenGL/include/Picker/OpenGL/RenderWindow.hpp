#ifndef PICKER_OPEN_GL_RENDER_WINDOW_HPP
#define PICKER_OPEN_GL_RENDER_WINDOW_HPP

#include <Picker/Config.hpp>

#include <Picker/Window.hpp>
#include <Picker/OpenGL/IRenderTarget.hpp>

namespace picker
{
	class Color;

	namespace ogl
	{
		////////////////////////////////////////////////////////////
		/// @class picker::Window
		/// @brief Represents a window that you can display to with OpenGL.
		////////////////////////////////////////////////////////////
		class PICKER_EXPORT RenderWindow : public picker::Window, public picker::ogl::IRenderTarget
		{
		public:
			////////////////////////////////////////////////////////////
			/// @brief Default constructor.
			////////////////////////////////////////////////////////////
			RenderWindow();

			////////////////////////////////////////////////////////////
			/// @brief Constructs a window.
			///
			/// @param width	The window's width in pixels
			/// @param height	The window's height in pixels
			/// @param title	The window's title
			////////////////////////////////////////////////////////////
			RenderWindow( unsigned int width, unsigned int height, const ::std::string& title, unsigned char colorBits = 8, unsigned char depthBits = 16 );

			////////////////////////////////////////////////////////////
			/// @brief Destructor. Used for inheritance.
			////////////////////////////////////////////////////////////
			virtual ~RenderWindow();

			////////////////////////////////////////////////////////////
			/// @brief Clears the window and fills it with a given colour.
			////////////////////////////////////////////////////////////
			void clearWindow( const picker::Color& clearColor );

			////////////////////////////////////////////////////////////
			/// @brief Displays everything that has been drawn to the window since
			///        the last picker::Window::display() call.
			////////////////////////////////////////////////////////////
			void display();


		private:
			#if defined( PICKER_SYSTEM_WINDOWS )
			bool setUpPixelFormat( unsigned char colorBits, unsigned char depthBits ) const;

			////////////////////////////////////////////////////////////
			/// @brief Let's OpenGL know to target this window 
			///	       when referring to display calls.
			////////////////////////////////////////////////////////////
			void focusOpenGlOnWindow();

			HINSTANCE	_instanceHandle;
			HDC			_deviceContextHandle;
			HGLRC		_glResourceContext;
			#endif
		};
	}
}

#endif // PICKER_OPEN_GL_RENDER_WINDOW_HPP