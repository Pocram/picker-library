#ifndef PICKER_OPEN_GL_TRIANGLE_HPP
#define PICKER_OPEN_GL_TRIANGLE_HPP

#include <Picker/Config.hpp>
#include <Picker/OpenGL/Transformable.hpp>
#include <Picker/OpenGL/IDrawable.hpp>
#include <Picker/OpenGL/Vertex.hpp>
#include <Picker/Maths/Vector3.hpp>

#include <array>

namespace picker
{
	namespace ogl
	{
		////////////////////////////////////////////////////////////
		/// @class picker::ogl::Triangle
		/// @brief Represents a triangle that is made of three vertices.
		////////////////////////////////////////////////////////////
		class PICKER_EXPORT Triangle : 
			public picker::ogl::Transformable,
			public picker::ogl::IDrawable
		{
		public:
			////////////////////////////////////////////////////////////
			/// @brief Default constructor.
			////////////////////////////////////////////////////////////
			Triangle();

			////////////////////////////////////////////////////////////
			/// @brief Constructs a triangle.
			///
			/// @param positions The position of each vertex.
			////////////////////////////////////////////////////////////
			Triangle( const ::std::array<picker::Vector3F, 3>& positions );

			////////////////////////////////////////////////////////////
			/// @brief Constructs a triangle.
			///
			/// @param positionA The first vertex' position.
			/// @param positionB The second vertex' position.
			/// @param positionC The third vertex' position.
			////////////////////////////////////////////////////////////
			Triangle( const picker::Vector3F& positionA, const picker::Vector3F& positionB, const picker::Vector3F& positionC );

			////////////////////////////////////////////////////////////
			/// @brief Constructs a triangle.
			///
			/// @param vertices The three vertices.
			////////////////////////////////////////////////////////////
			Triangle( const ::std::array<picker::ogl::Vertex, 3>& vertices );

			////////////////////////////////////////////////////////////
			/// @brief Constructs a triangle.
			///
			/// @param vertexA The first vertex.
			/// @param vertexB The second vertex.
			/// @param vertexC The third vertex.
			////////////////////////////////////////////////////////////
			Triangle( const picker::ogl::Vertex& vertexA, const picker::ogl::Vertex& vertexB, const picker::ogl::Vertex& vertexC );


			////////////////////////////////////////////////////////////
			/// @brief Gets a vertex of the triangle.
			///
			/// @param index The vertex' index. Ranges from 0 to 2.
			////////////////////////////////////////////////////////////
			const picker::ogl::Vertex& getVertex( unsigned int index ) const;

			////////////////////////////////////////////////////////////
			/// @brief Gets a vertex of the triangle.
			///
			/// @param index The vertex' index. Ranges from 0 to 2.
			////////////////////////////////////////////////////////////
			picker::ogl::Vertex& getVertex( unsigned int index );

			////////////////////////////////////////////////////////////
			/// @brief Sets a vertex of the triangle.
			///
			/// @param index	The vertex' index. Ranges from 0 to 2.
			/// @param vertex	The vertex data.
			////////////////////////////////////////////////////////////
			void setVertex( unsigned int index, const picker::ogl::Vertex& vertex );


		protected:
			////////////////////////////////////////////////////////////
			/// @brief Draws the object to the render target.
			////////////////////////////////////////////////////////////
			void draw( IRenderTarget& target ) const override;


		private:
			::std::array<picker::ogl::Vertex, 3> _vertices;	///< All three vertices.
		};
	}
}

#endif // PICKER_OPEN_GL_TRIANGLE_HPP