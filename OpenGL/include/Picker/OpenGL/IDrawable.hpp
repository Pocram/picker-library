#ifndef PICKER_OPEN_GL_I_DRAWABLE_HPP
#define PICKER_OPEN_GL_I_DRAWABLE_HPP

#include <Picker/Config.hpp>

namespace picker
{
	namespace ogl
	{
		////////////////////////////////////////////////////////////
		/// @class picker::ogl::IRenderTarget
		/// @brief Interface for objects that can be drawn to a render target.
		////////////////////////////////////////////////////////////
		class PICKER_EXPORT IDrawable
		{
		public:
			friend class IRenderTarget;

			////////////////////////////////////////////////////////////
			/// @brief Virtual destructor.
			////////////////////////////////////////////////////////////
			virtual ~IDrawable() { }

		protected:
			////////////////////////////////////////////////////////////
			/// @brief Draws the object to the render target.
			////////////////////////////////////////////////////////////
			virtual void draw( IRenderTarget& target ) const = 0;
		};
	}
}

#endif // PICKER_OPEN_GL_I_DRAWABLE_HPP