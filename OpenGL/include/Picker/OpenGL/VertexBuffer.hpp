#ifndef PICKER_OPEN_GL_VERTEX_BUFFER_HPP
#define PICKER_OPEN_GL_VERTEX_BUFFER_HPP

#include <Picker/Config.hpp>
#include <Picker/OpenGL/Vertex.hpp>
#include <Picker/OpenGL/IDrawable.hpp>

#include <vector>

namespace picker
{
	namespace ogl
	{
		class Skybox;

		class PICKER_EXPORT VertexBuffer : public picker::ogl::IDrawable
		{
			friend class picker::ogl::Skybox;

		public:
			////////////////////////////////////////////////////////////
			/// @brief Default constructor.
			////////////////////////////////////////////////////////////
			VertexBuffer();

			////////////////////////////////////////////////////////////
			/// @brief Constructs a vertex buffer from a vector of vertices.
			////////////////////////////////////////////////////////////
			VertexBuffer( const ::std::vector<picker::ogl::Vertex>& vertices );

			////////////////////////////////////////////////////////////
			/// @brief Copy constructor.
			////////////////////////////////////////////////////////////
			VertexBuffer( const picker::ogl::VertexBuffer& other );

			////////////////////////////////////////////////////////////
			/// @brief Copy assignment operator.
			////////////////////////////////////////////////////////////
			VertexBuffer& operator=( const picker::ogl::VertexBuffer& other );

			////////////////////////////////////////////////////////////
			/// @brief Destructor.
			////////////////////////////////////////////////////////////
			~VertexBuffer();


		protected:
			void draw( IRenderTarget& target ) const override;


		private:
			::std::vector<picker::ogl::Vertex> _vertices;

			unsigned int _arrayId;
			unsigned int _positionBufferId;
			unsigned int _colorBufferId;
			unsigned int _normalBufferId;
			unsigned int _uvBufferId;

			void createBuffersFromVertexVector( const ::std::vector<picker::ogl::Vertex>& vertices );
		};
	}
}

#endif // PICKER_OPEN_GL_VERTEX_BUFFER_HPP