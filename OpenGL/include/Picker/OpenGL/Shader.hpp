#ifndef PICKER_OPEN_GL_SHADER_HPP
#define PICKER_OPEN_GL_SHADER_HPP

#include <Picker/Config.hpp>

#include <string>

namespace picker
{
	namespace ogl
	{
		////////////////////////////////////////////////////////////
		/// @class picker::ogl::Shader
		/// @brief Represents the base of either a vertex or fragment shader.
		////////////////////////////////////////////////////////////
		class PICKER_EXPORT Shader
		{
		public:
			~Shader();

			////////////////////////////////////////////////////////////
			/// @brief Returns the OpenGL shader ID.
			///
			/// Keep in mind that this is *not* the compiled shader program ID!
			////////////////////////////////////////////////////////////
			unsigned int getShaderId() const;

			////////////////////////////////////////////////////////////
			/// @brief Whether the shader has compiled successfully or not.
			/// @returns Whether the shader has compiled successfully or not.
			////////////////////////////////////////////////////////////
			bool hasCompiled() const;

			////////////////////////////////////////////////////////////
			/// @brief Gets the compilation error message.
			///
			/// If the compilation was successful, the message string is empty.
			///
			/// @returns The compilation error message.
			////////////////////////////////////////////////////////////
			const ::std::string& getCompilationErrorMessage() const;
			


		protected:
			////////////////////////////////////////////////////////////
			/// @brief Default constructor.
			////////////////////////////////////////////////////////////
			Shader();

			unsigned int	_shaderId;					///< The OpenGL shader ID. Note: Not the ID of the compiled program!
			bool			_hasCompiledSuccessfully;	///< Whether the shader has compiled successfully or not.
			::std::string	_compilationErrorMessage;	///< The compilation error message.
		};
	}
}

#endif // PICKER_OPEN_GL_SHADER_HPP