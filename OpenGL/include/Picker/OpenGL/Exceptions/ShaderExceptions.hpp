#ifndef PICKER_OPEN_GL_EXCEPTIONS_SHADER_EXCEPTIONS_HPP
#define PICKER_OPEN_GL_EXCEPTIONS_SHADER_EXCEPTIONS_HPP

#include <exception>
#include <string>

namespace picker
{
	namespace ogl
	{
		class ShaderFailedToCompile : public ::std::exception
		{
		public:
			ShaderFailedToCompile() :
				_message( "The shader was not able to compile!" )
			{
			}

			ShaderFailedToCompile( const ::std::string& message ) :
				_message( message )
			{
			}

			char const* what() const throw( ) override
			{
				return _message.c_str();
			}


		private:
			::std::string _message;
		};
	}
}

#endif // PICKER_OPEN_GL_EXCEPTIONS_SHADER_EXCEPTIONS_HPP