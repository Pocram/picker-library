#ifndef PICKER_OPEN_GL_EXCEPTIONS_MATERIAL_EXCEPTIONS_HPP
#define PICKER_OPEN_GL_EXCEPTIONS_MATERIAL_EXCEPTIONS_HPP

#include <exception>
#include <string>

namespace picker
{
	namespace ogl
	{
		class MaterialHasNoShader : public ::std::exception
		{
		public:
			char const* what() const throw( ) override
			{
				return "The given material has no associated shader!";
			}
		};

		class MaterialParameterDoesNotExist : public ::std::exception
		{
		public:
			MaterialParameterDoesNotExist() :
				_message( "A parameter in a material does not exist!" )
			{
			}

			MaterialParameterDoesNotExist( const ::std::string& message ) :
				_message( message )
			{
			}

			char const* what() const throw( ) override
			{
				return _message.c_str();
			}


		private:
			::std::string _message;
		};
	}
}

#endif // PICKER_OPEN_GL_EXCEPTIONS_MATERIAL_EXCEPTIONS_HPP