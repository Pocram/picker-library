#ifndef PICKER_OPEN_GL_TRANSFORMABLE_HPP
#define PICKER_OPEN_GL_TRANSFORMABLE_HPP

#include <Picker/Config.hpp>
#include <Picker/OpenGL/Transform.hpp>

namespace picker
{
	namespace ogl
	{
		////////////////////////////////////////////////////////////
		/// @class picker::ogl::Transformable
		/// @brief Represnts a transformable object. Usually used in inheritance.
		////////////////////////////////////////////////////////////
		class PICKER_EXPORT Transformable
		{
		public:
			////////////////////////////////////////////////////////////
			/// @brief Default constructor.
			////////////////////////////////////////////////////////////
			Transformable();


			////////////////////////////////////////////////////////////
			/// @brief Gets the transform.
			/// @returns The transform.
			////////////////////////////////////////////////////////////
			const picker::ogl::Transform& getTransform() const;
			
			////////////////////////////////////////////////////////////
			/// @brief Gets the transform.
			/// @returns The transform.
			////////////////////////////////////////////////////////////
			picker::ogl::Transform& getTransform();

			////////////////////////////////////////////////////////////
			/// @brief Sets the transform.
			/// @param transform The transform.
			////////////////////////////////////////////////////////////
			void setTransform( const picker::ogl::Transform& transform );


		private:
			picker::ogl::Transform _transform;	///< The actual transform.
		};
	}
}

#endif // PICKER_OPEN_GL_TRANSFORMABLE_HPP