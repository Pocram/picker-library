#ifndef PICKER_OPEN_GL_MATERIAL_HPP
#define PICKER_OPEN_GL_MATERIAL_HPP

#include <Picker/Config.hpp>
#include <Picker/Maths/Vector2.hpp>
#include <Picker/Maths/Vector3.hpp>
#include <Picker/Color.hpp>
#include <Picker/OpenGL/Texture.hpp>
#include <Picker/OpenGL/Shaders.hpp>
#include <Picker/OpenGL/Exceptions/MaterialExceptions.hpp>

#include <map>
#include <unordered_map>

namespace picker
{
	namespace ogl
	{
		class IRenderTarget;

		////////////////////////////////////////////////////////////
		/// @class picker::ogl::Material
		/// @brief Defines how a surface should be rendered.
		///
		/// A material contains a shader that describes rendering on 
		/// a per-pixel and per-vertex basis.
		/// The material acts as the interface between the program
		/// and the shader.
		////////////////////////////////////////////////////////////
		class PICKER_EXPORT Material
		{
			friend class picker::ogl::IRenderTarget;

			struct TextureRenderInfo
			{
				TextureRenderInfo() :
					texture( nullptr ),
					textureUnit( 0 )
				{
				}

				TextureRenderInfo( const picker::ogl::Texture* texture, unsigned textureUnit ) : 
					texture( texture ), 
					textureUnit( textureUnit )
				{
				}

				const picker::ogl::Texture*	texture;
				unsigned int				textureUnit;
			};

			template<typename T>
			using ParameterValuePair = ::std::unordered_map<int, T>;

		public:
			////////////////////////////////////////////////////////////
			/// @brief Default constructor.
			////////////////////////////////////////////////////////////
			Material();

			////////////////////////////////////////////////////////////
			/// @brief Constructs a material with a shader.
			///
			/// Since single shaders take up a lot of space in the computer's memory,
			/// this constructor should only be used when creating all the materials in one batch.
			/// For example, when loading a level in a game, the shader objects are created,
			/// the materials use these objects to instantiate themselves and the shader objects
			/// are then discarded.
			///
			/// @param vertexShader		The vertex shader.
			/// @param fragmentShader	The fragment shader.
			///
			/// @throws picker::ogl::ShaderFailedToCompile	Thrown if the two shaders could not be combined.
			////////////////////////////////////////////////////////////
			Material( const picker::ogl::VertexShader& vertexShader, const picker::ogl::FragmentShader& fragmentShader );

			////////////////////////////////////////////////////////////
			/// @brief Constructs a material with a shader.
			///
			/// This constructor should be used when creating a single material on the fly.
			/// If you need to create multiple shaders in one batch operation, use
			/// picker::ogl::Material::Material( const picker::ogl::VertexShader& vertexShader, const picker::ogl::FragmentShader& fragmentShader )
			/// instead.
			///
			/// @param pathToVertexShader	The path to the vertex shader file.
			/// @param pathToFragmentShader	The path to the fragment shader file.
			///
			/// @throws picker::ogl::ShaderFailedToCompile	Thrown if the two shaders could not be combined.
			////////////////////////////////////////////////////////////
			Material( const ::std::string& pathToVertexShader, const ::std::string& pathToFragmentShader );

			////////////////////////////////////////////////////////////
			/// @brief Destructor.
			////////////////////////////////////////////////////////////
			~Material();

			////////////////////////////////////////////////////////////
			/// @brief Loads two shaders and compiles them together.
			///
			/// Since single shaders take up a lot of space in the computer's memory,
			/// this method should only be used when creating all the materials in one batch.
			/// For example, when loading a level in a game, the shader objects are created,
			/// the materials use these objects to instantiate themselves and the shader objects
			/// are then discarded.
			///
			/// @returns Whether the shaders could link successfully.
			////////////////////////////////////////////////////////////
			bool loadShaders( const picker::ogl::VertexShader& vertexShader, const picker::ogl::FragmentShader& fragmentShader );

			////////////////////////////////////////////////////////////
			/// @brief Loads two shaders and compiles them together.
			///
			/// This method should be used when creating a single material on the fly.
			/// If you need to create multiple shaders in one batch operation, use
			/// picker::ogl::Material::Material( const picker::ogl::VertexShader& vertexShader, const picker::ogl::FragmentShader& fragmentShader )
			/// instead.
			///
			/// @param pathToVertexShader	The path to the vertex shader file.
			/// @param pathToFragmentShader	The path to the fragment shader file.
			///
			/// @throws picker::ogl::ShaderFailedToCompile	Thrown if the two shaders could not be combined.
			////////////////////////////////////////////////////////////
			bool loadShaders( const ::std::string& pathToVertexShader, const ::std::string& pathToFragmentShader );

			////////////////////////////////////////////////////////////
			/// @brief Gets the shader program ID used by OpenGL functions.
			////////////////////////////////////////////////////////////
			unsigned int getShaderProgramId() const;

			////////////////////////////////////////////////////////////
			/// @brief	Checks whether a shader parameter of the 
			///			given name exists.
			///
			/// Fetching a shader parameter can be an expensive action.
			/// Polling the parameter using this functions saves the name
			/// and its ID in an internal map.
			///
			/// @throws picker::ogl::MaterialHasNoShader	Thrown when the material has no assigned shader.
			///												Make sure to use either picker::ogl::Material::loadShaders() or
			///												picker::ogl::Material( const picker::ogl::Shader& shader ).
			////////////////////////////////////////////////////////////
			bool pollParameterByName( const ::std::string& parameterName );

			////////////////////////////////////////////////////////////
			/// @brief	Returns the OpenGL ID for a given parameter.
			////////////////////////////////////////////////////////////
			int getParameterId( const ::std::string& parameterName ) const;


			////////////////////////////////////////////////////////////
			/// @brief Sets a float value in the shader.
			///
			/// @param parameterName	The parameter's name.
			/// @param value			The parameter's value.
			///
			/// @throws picker::ogl::MaterialHasNoShader	Thrown when the material has no assigned shader.
			///												Make sure to use either picker::ogl::Material::loadShaders() or
			///												picker::ogl::Material( const picker::ogl::Shader& shader ).
			///
			/// @throws picker::ogl::MaterialParameterDoesNotExist	Thrown when the given parameter name does not correspond to any
			///														parameter in the shader.
			////////////////////////////////////////////////////////////
			void setFloat( const ::std::string& parameterName, float value );

			////////////////////////////////////////////////////////////
			/// @brief Sets an int value in the shader.
			///
			/// @param parameterName	The parameter's name.
			/// @param value			The parameter's value.
			///
			/// @throws picker::ogl::MaterialHasNoShader	Thrown when the material has no assigned shader.
			///												Make sure to use either picker::ogl::Material::loadShaders() or
			///												picker::ogl::Material( const picker::ogl::Shader& shader ).
			///
			/// @throws picker::ogl::MaterialParameterDoesNotExist	Thrown when the given parameter name does not correspond to any
			///														parameter in the shader.
			////////////////////////////////////////////////////////////
			void setInt( const ::std::string& parameterName, int value );

			////////////////////////////////////////////////////////////
			/// @brief Sets a Vector2F value in the shader.
			///
			/// @param parameterName	The parameter's name.
			/// @param value			The parameter's value.
			///
			/// @throws picker::ogl::MaterialHasNoShader	Thrown when the material has no assigned shader.
			///												Make sure to use either picker::ogl::Material::loadShaders() or
			///												picker::ogl::Material( const picker::ogl::Shader& shader ).
			///
			/// @throws picker::ogl::MaterialParameterDoesNotExist	Thrown when the given parameter name does not correspond to any
			///														parameter in the shader.
			////////////////////////////////////////////////////////////
			void setVector( const ::std::string& parameterName, const picker::Vector2F& value );

			////////////////////////////////////////////////////////////
			/// @brief Sets a Vector2I value in the shader.
			///
			/// @param parameterName	The parameter's name.
			/// @param value			The parameter's value.
			///
			/// @throws picker::ogl::MaterialHasNoShader	Thrown when the material has no assigned shader.
			///												Make sure to use either picker::ogl::Material::loadShaders() or
			///												picker::ogl::Material( const picker::ogl::Shader& shader ).
			///
			/// @throws picker::ogl::MaterialParameterDoesNotExist	Thrown when the given parameter name does not correspond to any
			///														parameter in the shader.
			////////////////////////////////////////////////////////////
			void setVector( const ::std::string& parameterName, const picker::Vector2I& value );

			////////////////////////////////////////////////////////////
			/// @brief Sets a Vector2U value in the shader.
			///
			/// @param parameterName	The parameter's name.
			/// @param value			The parameter's value.
			///
			/// @throws picker::ogl::MaterialHasNoShader	Thrown when the material has no assigned shader.
			///												Make sure to use either picker::ogl::Material::loadShaders() or
			///												picker::ogl::Material( const picker::ogl::Shader& shader ).
			///
			/// @throws picker::ogl::MaterialParameterDoesNotExist	Thrown when the given parameter name does not correspond to any
			///														parameter in the shader.
			////////////////////////////////////////////////////////////
			void setVector( const ::std::string& parameterName, const picker::Vector2U& value );

			////////////////////////////////////////////////////////////
			/// @brief Sets a Vector3F value in the shader.
			///
			/// @param parameterName	The parameter's name.
			/// @param value			The parameter's value.
			///
			/// @throws picker::ogl::MaterialHasNoShader	Thrown when the material has no assigned shader.
			///												Make sure to use either picker::ogl::Material::loadShaders() or
			///												picker::ogl::Material( const picker::ogl::Shader& shader ).
			///
			/// @throws picker::ogl::MaterialParameterDoesNotExist	Thrown when the given parameter name does not correspond to any
			///														parameter in the shader.
			////////////////////////////////////////////////////////////
			void setVector( const ::std::string& parameterName, const picker::Vector3F& value );

			////////////////////////////////////////////////////////////
			/// @brief Sets a Vector3I value in the shader.
			///
			/// @param parameterName	The parameter's name.
			/// @param value			The parameter's value.
			///
			/// @throws picker::ogl::MaterialHasNoShader	Thrown when the material has no assigned shader.
			///												Make sure to use either picker::ogl::Material::loadShaders() or
			///												picker::ogl::Material( const picker::ogl::Shader& shader ).
			///
			/// @throws picker::ogl::MaterialParameterDoesNotExist	Thrown when the given parameter name does not correspond to any
			///														parameter in the shader.
			////////////////////////////////////////////////////////////
			void setVector( const ::std::string& parameterName, const picker::Vector3I& value );

			////////////////////////////////////////////////////////////
			/// @brief Sets a Vector3U value in the shader.
			///
			/// @param parameterName	The parameter's name.
			/// @param value			The parameter's value.
			///
			/// @throws picker::ogl::MaterialHasNoShader	Thrown when the material has no assigned shader.
			///												Make sure to use either picker::ogl::Material::loadShaders() or
			///												picker::ogl::Material( const picker::ogl::Shader& shader ).
			///
			/// @throws picker::ogl::MaterialParameterDoesNotExist	Thrown when the given parameter name does not correspond to any
			///														parameter in the shader.
			////////////////////////////////////////////////////////////
			void setVector( const ::std::string& parameterName, const picker::Vector3U& value );

			////////////////////////////////////////////////////////////
			/// @brief Sets a Color value in the shader.
			///
			/// @param parameterName	The parameter's name.
			/// @param color			The color.
			///
			/// @throws picker::ogl::MaterialHasNoShader	Thrown when the material has no assigned shader.
			///												Make sure to use either picker::ogl::Material::loadShaders() or
			///												picker::ogl::Material( const picker::ogl::Shader& shader ).
			///
			/// @throws picker::ogl::MaterialParameterDoesNotExist	Thrown when the given parameter name does not correspond to any
			///														parameter in the shader.
			////////////////////////////////////////////////////////////
			void setColor( const ::std::string& parameterName, const picker::Color& color );

			////////////////////////////////////////////////////////////
			/// @brief Sets a Texture in the shader.
			///
			/// @param parameterName	The parameter's name.
			/// @param texture			The texture. The material will not take ownership of the texture. 
			///							Upon deletion of the material, the texture will still exist.
			///
			/// @throws picker::ogl::MaterialHasNoShader	Thrown when the material has no assigned shader.
			///												Make sure to use either picker::ogl::Material::loadShaders() or
			///												picker::ogl::Material( const picker::ogl::Shader& shader ).
			///
			/// @throws picker::ogl::MaterialParameterDoesNotExist	Thrown when the given parameter name does not correspond to any
			///														parameter in the shader.
			////////////////////////////////////////////////////////////
			void setTexture( const ::std::string& parameterName, const picker::ogl::Texture& texture, unsigned int textureUnit = 0 );



		private:
			void pollAllParameters();
			void handleParameterSetterStart( const ::std::string& parameterName );
			void checkForParameterValidity( const ::std::string& parameterName );
			void setUniforms() const;

			unsigned int					_shaderProgramId;	///< The shader's ID, used by OpenGL.
			::std::map<::std::string, int>	_shaderParameters;	///< A map of all previously fetched parameters.

			picker::ogl::Material::ParameterValuePair<float>	_floatParameters;
			picker::ogl::Material::ParameterValuePair<int>		_intParameters;
			picker::ogl::Material::ParameterValuePair<Vector2F>	_vector2fParameters;
			picker::ogl::Material::ParameterValuePair<Vector2I>	_vector2iParameters;
			picker::ogl::Material::ParameterValuePair<Vector2U>	_vector2uParameters;
			picker::ogl::Material::ParameterValuePair<Vector3F>	_vector3fParameters;
			picker::ogl::Material::ParameterValuePair<Vector3I>	_vector3iParameters;
			picker::ogl::Material::ParameterValuePair<Vector3U>	_vector3uParameters;
			picker::ogl::Material::ParameterValuePair<TextureRenderInfo> _textureParameters;
		};
	}
}

#endif // PICKER_OPEN_GL_MATERIAL_HPP