#ifndef PICKER_OPEN_GL_TRANSFORM_HPP
#define PICKER_OPEN_GL_TRANSFORM_HPP

#include <Picker/Config.hpp>
#include <Picker/Maths/Vector3.hpp>

#include <glm/glm.hpp>

namespace picker
{
	namespace ogl
	{
		////////////////////////////////////////////////////////////
		/// @class picker::ogl::Transform
		/// @brief Handles the position, rotation and scale of an object.
		////////////////////////////////////////////////////////////
		class PICKER_EXPORT Transform
		{
		public:
			////////////////////////////////////////////////////////////
			/// @brief Default constructor.
			////////////////////////////////////////////////////////////
			Transform();

			////////////////////////////////////////////////////////////
			/// @brief Constructs a transform.
			///
			/// @param position	The position of the object in world space.
			/// @param rotation	The rotation of the objet in Euler degree angles.
			/// @param scale	The scale of the object in world space.
			////////////////////////////////////////////////////////////
			Transform( const picker::Vector3F& position, const picker::Vector3F& rotation, const picker::Vector3F& scale );


			////////////////////////////////////////////////////////////
			/// @brief Gets the position in world space.
			///
			/// @returns The object's position in world space.
			////////////////////////////////////////////////////////////
			const picker::Vector3F& getPosition() const;

			////////////////////////////////////////////////////////////
			/// @brief Gets the position in world space.
			///
			/// @returns The object's position in world space.
			////////////////////////////////////////////////////////////
			picker::Vector3F& getPosition();

			////////////////////////////////////////////////////////////
			/// @brief Sets the object's position in world space.
			///
			/// @param position The position in world space.
			////////////////////////////////////////////////////////////
			void setPosition( const picker::Vector3F& position );

			////////////////////////////////////////////////////////////
			/// @brief Gets the rotation in world space.
			///
			/// @returns The object's rotation in world space.
			////////////////////////////////////////////////////////////
			const picker::Vector3F& getRotation() const;
			
			////////////////////////////////////////////////////////////
			/// @brief Gets the rotation in world space.
			///
			/// @returns The object's rotation in world space.
			////////////////////////////////////////////////////////////
			picker::Vector3F& getRotation();

			////////////////////////////////////////////////////////////
			/// @brief Sets the object's rotation in world space.
			///
			/// @param rotation The rotation in world space.
			////////////////////////////////////////////////////////////
			void setRotation( const picker::Vector3F& rotation );

			////////////////////////////////////////////////////////////
			/// @brief Gets the scale.
			///
			/// @returns The object's scale.
			////////////////////////////////////////////////////////////
			const picker::Vector3F& getScale() const;

			////////////////////////////////////////////////////////////
			/// @brief Gets the scale.
			///
			/// @returns The object's scale.
			////////////////////////////////////////////////////////////
			picker::Vector3F& getScale();

			////////////////////////////////////////////////////////////
			/// @brief Sets the object's scale.
			///
			/// @param scale The scale.
			////////////////////////////////////////////////////////////
			void setScale( const picker::Vector3F& scale );


			const glm::mat4& getModelMatrix() const;


		private:
			void updateModelMatrix();

			picker::Vector3F _position;	///< The position of the object in world space.
			picker::Vector3F _rotation;	///< The rotation of the objet in Euler degree angles.
			picker::Vector3F _scale;	///< The scale of the object in world space.

			glm::mat4		_modelMatrix;
		};
	}
}

#endif // PICKER_OPEN_GL_TRANSFORM_HPP
