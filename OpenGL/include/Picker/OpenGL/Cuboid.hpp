#ifndef PICKER_OPEN_GL_CUBOID_HPP
#define PICKER_OPEN_GL_CUBOID_HPP

#include <Picker/Config.hpp>
#include <Picker/OpenGL/IDrawable.hpp>
#include <Picker/OpenGL/Quad.hpp>

#include <array>

namespace picker
{
	namespace ogl
	{
		////////////////////////////////////////////////////////////
		/// @class picker::ogl::Cuboid
		/// @brief Represents a cuboid that is made of eight vertices.
		////////////////////////////////////////////////////////////
		class PICKER_EXPORT Cuboid : picker::ogl::IDrawable
		{
		public:
			////////////////////////////////////////////////////////////
			/// @brief Default constructor.
			////////////////////////////////////////////////////////////
			Cuboid();
			
			////////////////////////////////////////////////////////////
			/// @brief Constructs a cuboid.
			///
			/// @param corner			A corner position of the cuboid.
			/// @param oppositeCorner	The corner diagonally opposite of the first corner.
			////////////////////////////////////////////////////////////
			Cuboid( const picker::Vector3F& corner, const picker::Vector3F& oppositeCorner );



		protected:
			////////////////////////////////////////////////////////////
			/// @brief Draws the object to the render target.
			////////////////////////////////////////////////////////////
			void draw( IRenderTarget& target ) const override;


		private:
			::std::array<picker::ogl::Quad, 6> _faces; ///< A list of all the faces.
		};
	}
}

#endif // PICKER_OPEN_GL_CUBOID_HPP