#ifndef PICKER_OPEN_GL_TEXTURE_HPP
#define PICKER_OPEN_GL_TEXTURE_HPP

#include <Picker/Config.hpp>
#include <Picker/Image.hpp>
#include <string>

namespace picker
{
	namespace ogl
	{
		class Material;

		////////////////////////////////////////////////////////////
		/// @class picker::ogl::Texture
		/// @brief Wraps an image and sends it to the GPU.
		////////////////////////////////////////////////////////////
		class PICKER_EXPORT Texture
		{
		public:
			friend class picker::ogl::Material;

			////////////////////////////////////////////////////////////
			/// @brief Default constructor.
			////////////////////////////////////////////////////////////
			Texture();

			////////////////////////////////////////////////////////////
			/// @brief Copy constructor.
			/// @param copy Object instance to copy.
			////////////////////////////////////////////////////////////
			Texture( const Texture& copy );

			////////////////////////////////////////////////////////////
			/// @brief Destructor.
			////////////////////////////////////////////////////////////
			~Texture();


			////////////////////////////////////////////////////////////
			/// @brief Creates and initialises the texture.
			///
			/// @param width	The texture's width in pixels.
			/// @param height	The texture's height in pixels.
			///
			/// @returns True if the texture could be created.
			////////////////////////////////////////////////////////////
			bool create( unsigned int width, unsigned int height );

			////////////////////////////////////////////////////////////
			/// @brief Loads an image and sends it to the GPU.
			///
			/// @param image The image to load.
			///
			/// @returns True if the image could be loaded to the GPU.
			////////////////////////////////////////////////////////////
			bool loadFromImage( const picker::Image& image );

			////////////////////////////////////////////////////////////
			/// @brief Loads an image and sends it to the GPU.
			///
			/// @param pathToFile The path to the image file.
			///
			/// @returns True if the image could be found and loaded to the GPU.
			////////////////////////////////////////////////////////////
			bool loadFromFile( const ::std::string& pathToFile );


			////////////////////////////////////////////////////////////
			/// @brief Updates the texture with a given image.
			///
			/// @param image The new image to send to the GPU.
			////////////////////////////////////////////////////////////
			void update( const picker::Image& image );


		private:
			unsigned int	_openGlTextureId;
			unsigned int	_width;
			unsigned int	_height;
		};
	}
}

#endif // PICKER_OPEN_GL_TEXTURE_HPP