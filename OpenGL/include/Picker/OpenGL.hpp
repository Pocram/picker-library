#ifndef PICKER_OPEN_GL_HPP
#define PICKER_OPEN_GL_HPP

// The purpose of this header file is readability.
// The following definition checks and include statements have to be
// included into every file that OpenGL functions.
// You can simply include <Picker/OpenGL.hpp> instead.

#include <Picker/Config.hpp>
#if defined( PICKER_SYSTEM_WINDOWS )
	#include <windows.h>
	#include <GL/glew.h>
	#include <gl/GL.h>
	#include <gl/GLU.h>
#endif

namespace picker
{
	namespace ogl
	{
		#if defined( _DEBUG )
			// Use a loop to make the two expressions the equivalent of one call for use in single line if/else branches.
			#define glCheck( expression ) do { expression; picker::ogl::checkLastOpenGlError( __FILE__, __LINE__, #expression ); } while ( false )
		#else
			#define glCheck( expression ) ( expression )
		#endif

		////////////////////////////////////////////////////////////
		/// @brief Checks and outputs the last OpenGL error.
		///
		/// This function is used for debug purposes by the glCheck macro.
		///
		/// @param file			Source file that called the function.
		/// @param line			Line number that called the expression.
		/// @param expression	The expression as a string.
		////////////////////////////////////////////////////////////
		void checkLastOpenGlError( const char* file, unsigned int line, const char* expression );
	}
}

#endif // PICKER_OPEN_GL_HPP