#include <Picker/OpenGL/Vertex.hpp>

picker::ogl::Vertex::Vertex() :
	_position( 0.0f, 0.0f, 0.0f ),
	_color( 1.0f, 1.0f, 1.0f ),
	_normal( 0.0f, 0.0f, 0.0f ),
	_uvCoordinate( 0.0f, 0.0f )
{
}

picker::ogl::Vertex::Vertex( const picker::Vector3F& position )	:
	_position( position ),
	_color( 1.0f, 1.0f, 1.0f ),
	_normal( 0.0f, 0.0f, 0.0f ),
	_uvCoordinate( 0.0f, 0.0f )
{
}

picker::ogl::Vertex::Vertex( const picker::Vector3F& position, const picker::Vector3F normal ) :
	_position( position ),
	_color( 1.0f, 1.0f, 1.0f ),
	_normal( normal ),
	_uvCoordinate( 0.0f, 0.0f )
{
}

picker::ogl::Vertex::Vertex( const picker::Vector3F& position, const picker::Color& color ) :
	_position( position ),
	_color( color ),
	_normal( 0.0f, 0.0f, 0.0f ),
	_uvCoordinate( 0.0f, 0.0f )
{
}

picker::ogl::Vertex::Vertex( const picker::Vector3F& position, const picker::Color& color, const picker::Vector3F normal ) :
	_position( position ),
	_color( color ),
	_normal( normal ),
	_uvCoordinate( 0.0f, 0.0f )
{
}

picker::ogl::Vertex::Vertex( const picker::Vector3F& position, const picker::Color& color, const picker::Vector2F uvCoordinate ) :
	_position( position ),
	_color( color ),
	_normal( 0.0f, 0.0f, 0.0f ),
	_uvCoordinate( uvCoordinate )
{
}

picker::ogl::Vertex::Vertex( const picker::Vector3F& position, const picker::Color& color, const picker::Vector3F normal, const picker::Vector2F uvCoordinate ) :
	_position( position ),
	_color( color ),
	_normal( normal ),
	_uvCoordinate( uvCoordinate )
{
}

picker::ogl::Vertex::Vertex( const picker::Vector3F& position, const picker::Vector3F normal, const picker::Vector2F uvCoordinate ) :
	_position( position ),
	_color( 1.0f, 1.0f, 1.0f ),
	_normal( normal ),
	_uvCoordinate( uvCoordinate )
{
}

picker::Vector3F picker::ogl::Vertex::getPostion() const
{
	return _position;
}

void picker::ogl::Vertex::setPosition( const picker::Vector3F& position )
{
	_position = position;
}

picker::Color picker::ogl::Vertex::getColor() const
{
	return _color;
}

void picker::ogl::Vertex::setColor( const picker::Color& color )
{
	_color = color;
}

picker::Vector3F picker::ogl::Vertex::getNormal() const
{
	return _normal;
}

void picker::ogl::Vertex::setNormal( const picker::Vector3F& normal )
{
	_normal = normal;
}

picker::Vector2F picker::ogl::Vertex::getUvCoordinate() const
{
	return _uvCoordinate;
}

void picker::ogl::Vertex::setUvCoordinate( const picker::Vector2F& uvCoordinate )
{
	_uvCoordinate = uvCoordinate;
}

picker::ogl::SortedVertex picker::ogl::Vertex::toSorted() const
{
	return picker::ogl::SortedVertex( getPostion(), getColor(), getNormal(), getUvCoordinate() );
}

picker::ogl::SortedVertex::SortedVertex() :
	xPosition( 0.0f ),
	yPosition( 0.0f ),
	zPosition( 0.0f ),
	uTextureCoordinate( 0.0f ),
	vTextureCoordinate( 0.0f ),
	normalX( 0.0f ),
	normalY( 0.0f ),
	normalZ( 0.0f ),
	colorR( 1.0f ),
	colorG( 1.0f ),
	colorB( 1.0f )
{
}

picker::ogl::SortedVertex::SortedVertex( const picker::Vector3F& position ) :
	xPosition( position.x ),
	yPosition( position.y ),
	zPosition( position.z ),
	uTextureCoordinate( 0.0f ),
	vTextureCoordinate( 0.0f ),
	normalX( 0.0f ),
	normalY( 0.0f ),
	normalZ( 0.0f ),
	colorR( 1.0f ),
	colorG( 1.0f ),
	colorB( 1.0f )
{
}

picker::ogl::SortedVertex::SortedVertex( const picker::Vector3F& position, const picker::Color& color ) :
	xPosition( position.x ),
	yPosition( position.y ),
	zPosition( position.z ),
	uTextureCoordinate( 0.0f ),
	vTextureCoordinate( 0.0f ),
	normalX( 0.0f ),
	normalY( 0.0f ),
	normalZ( 0.0f ),
	colorR( color.r ),
	colorG( color.g ),
	colorB( color.b )
{
}

picker::ogl::SortedVertex::SortedVertex( const picker::Vector3F& position, const picker::Color& color, const picker::Vector3F normal ) :
	xPosition( position.x ),
	yPosition( position.y ),
	zPosition( position.z ),
	uTextureCoordinate( 0.0f ),
	vTextureCoordinate( 0.0f ),
	normalX( normal.x ),
	normalY( normal.y ),
	normalZ( normal.z ),
	colorR( color.r ),
	colorG( color.g ),
	colorB( color.b )
{
}

picker::ogl::SortedVertex::SortedVertex( const picker::Vector3F& position, const picker::Color& color, const picker::Vector2F uvCoordinate ) :
	xPosition( position.x ),
	yPosition( position.y ),
	zPosition( position.z ),
	uTextureCoordinate( uvCoordinate.x ),
	vTextureCoordinate( uvCoordinate.y ),
	normalX( 0.0f ),
	normalY( 0.0f ),
	normalZ( 0.0f ),
	colorR( color.r ),
	colorG( color.g ),
	colorB( color.b )
{
}

picker::ogl::SortedVertex::SortedVertex( const picker::Vector3F& position, const picker::Color& color, const picker::Vector3F normal, const picker::Vector2F uvCoordinate ) :
	xPosition( position.x ),
	yPosition( position.y ),
	zPosition( position.z ),
	uTextureCoordinate( uvCoordinate.x ),
	vTextureCoordinate( uvCoordinate.y ),
	normalX( normal.x ),
	normalY( normal.y ),
	normalZ( normal.z ),
	colorR( color.r ),
	colorG( color.g ),
	colorB( color.b )
{
}
