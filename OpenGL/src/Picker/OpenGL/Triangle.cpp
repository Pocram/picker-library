#include <Picker/OpenGL/Triangle.hpp>

#include <Picker/OpenGL.hpp>

picker::ogl::Triangle::Triangle()
{
	_vertices[0] = picker::ogl::Vertex( picker::Vector3F( 0.0f, 0.0f, 0.0f ), picker::Color( 1.0f, 1.0f, 1.0f ), picker::Vector3F( 0.0f, 0.0f, -1.0f ), picker::Vector2F( 0.0f, 0.0f ) );
	_vertices[1] = picker::ogl::Vertex( picker::Vector3F( 0.0f, 1.0f, 0.0f ), picker::Color( 1.0f, 1.0f, 1.0f ), picker::Vector3F( 0.0f, 0.0f, -1.0f ), picker::Vector2F( 0.0f, 0.0f ) );
	_vertices[2] = picker::ogl::Vertex( picker::Vector3F( 1.0f, 0.0f, 0.0f ), picker::Color( 1.0f, 1.0f, 1.0f ), picker::Vector3F( 0.0f, 0.0f, -1.0f ), picker::Vector2F( 0.0f, 0.0f ) );
}

picker::ogl::Triangle::Triangle( const ::std::array<picker::Vector3F, 3>& positions )
{
	_vertices[0] = picker::ogl::Vertex( positions[0], picker::Color( 1.0f, 1.0f, 1.0f ), picker::Vector3F( 0.0f, 0.0f, -1.0f ), picker::Vector2F( 0.0f, 0.0f ) );
	_vertices[1] = picker::ogl::Vertex( positions[1], picker::Color( 1.0f, 1.0f, 1.0f ), picker::Vector3F( 0.0f, 0.0f, -1.0f ), picker::Vector2F( 0.0f, 0.0f ) );
	_vertices[2] = picker::ogl::Vertex( positions[2], picker::Color( 1.0f, 1.0f, 1.0f ), picker::Vector3F( 0.0f, 0.0f, -1.0f ), picker::Vector2F( 0.0f, 0.0f ) );
}

picker::ogl::Triangle::Triangle( const picker::Vector3F& positionA, const picker::Vector3F& positionB, const picker::Vector3F& positionC )
{
	_vertices[0] = picker::ogl::Vertex( positionA, picker::Color( 1.0f, 1.0f, 1.0f ), picker::Vector3F( 0.0f, 0.0f, -1.0f ), picker::Vector2F( 0.0f, 0.0f ) );
	_vertices[1] = picker::ogl::Vertex( positionB, picker::Color( 1.0f, 1.0f, 1.0f ), picker::Vector3F( 0.0f, 0.0f, -1.0f ), picker::Vector2F( 0.0f, 0.0f ) );
	_vertices[2] = picker::ogl::Vertex( positionC, picker::Color( 1.0f, 1.0f, 1.0f ), picker::Vector3F( 0.0f, 0.0f, -1.0f ), picker::Vector2F( 0.0f, 0.0f ) );
}

picker::ogl::Triangle::Triangle( const ::std::array<picker::ogl::Vertex, 3>& vertices )
{
	_vertices = vertices;
}

picker::ogl::Triangle::Triangle( const picker::ogl::Vertex& vertexA, const picker::ogl::Vertex& vertexB, const picker::ogl::Vertex& vertexC )
{
	_vertices[0] = vertexA;
	_vertices[1] = vertexB;
	_vertices[2] = vertexC;
}

const picker::ogl::Vertex& picker::ogl::Triangle::getVertex( unsigned index ) const
{
	return _vertices[index];
}

picker::ogl::Vertex& picker::ogl::Triangle::getVertex( unsigned index )
{
	return _vertices[index];
}

void picker::ogl::Triangle::setVertex( unsigned index, const picker::ogl::Vertex& vertex )
{
	_vertices[index] = vertex;
}

void picker::ogl::Triangle::draw( IRenderTarget& target ) const
{
	#if defined( PICKER_SYSTEM_WINDOWS )
	glBegin( GL_TRIANGLES );

	for ( unsigned int i = 0; i < 3; i++ )
	{
		const picker::ogl::Vertex& v = getVertex( i );

		glTexCoord2f(	v.getUvCoordinate().x,	v.getUvCoordinate().y );
		glColor3f(		v.getColor().r,			v.getColor().g,			v.getColor().b );
		glNormal3f(		v.getNormal().x,		v.getNormal().y,		v.getNormal().z );
		glVertex3f(		v.getPostion().x,		v.getPostion().y,		v.getPostion().z );
	}

	glEnd();
	#endif
}
