#include <Picker/OpenGL/Transformable.hpp>

picker::ogl::Transformable::Transformable() :
	_transform( picker::ogl::Transform() )
{
}

const picker::ogl::Transform& picker::ogl::Transformable::getTransform() const
{
	return _transform;
}

picker::ogl::Transform& picker::ogl::Transformable::getTransform()
{
	return _transform;
}

void picker::ogl::Transformable::setTransform( const picker::ogl::Transform& transform )
{
	_transform = transform;
}
