#include <Picker/OpenGL/Shader.hpp>

picker::ogl::Shader::~Shader()
{
}

unsigned int picker::ogl::Shader::getShaderId() const
{
	return _shaderId;
}

bool picker::ogl::Shader::hasCompiled() const
{
	return _hasCompiledSuccessfully;
}

const ::std::string& picker::ogl::Shader::getCompilationErrorMessage() const
{
	return _compilationErrorMessage;
}

picker::ogl::Shader::Shader() : 
	_shaderId( -1 ),
    _hasCompiledSuccessfully( false )
{
}
