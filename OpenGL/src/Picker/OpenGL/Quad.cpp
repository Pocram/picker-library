#include <Picker/OpenGL/Quad.hpp>

#include <Picker/OpenGL.hpp>

picker::ogl::Quad::Quad()
{
	_vertices[0] = picker::ogl::Vertex( picker::Vector3F( 0.0f, 0.0f, 0.0f ), picker::Color( 1.0f, 1.0f, 1.0f ), picker::Vector3F( 0.0f, 0.0f, -1.0f ), picker::Vector2F( 0.0f, 0.0f ) );
	_vertices[1] = picker::ogl::Vertex( picker::Vector3F( 0.0f, 1.0f, 0.0f ), picker::Color( 1.0f, 1.0f, 1.0f ), picker::Vector3F( 0.0f, 0.0f, -1.0f ), picker::Vector2F( 0.0f, 0.0f ) );
	_vertices[2] = picker::ogl::Vertex( picker::Vector3F( 1.0f, 1.0f, 0.0f ), picker::Color( 1.0f, 1.0f, 1.0f ), picker::Vector3F( 0.0f, 0.0f, -1.0f ), picker::Vector2F( 0.0f, 0.0f ) );
	_vertices[3] = picker::ogl::Vertex( picker::Vector3F( 1.0f, 0.0f, 0.0f ), picker::Color( 1.0f, 1.0f, 1.0f ), picker::Vector3F( 0.0f, 0.0f, -1.0f ), picker::Vector2F( 0.0f, 0.0f ) );
}

picker::ogl::Quad::Quad( const picker::Vector3F& pointA, const picker::Vector3F& pointB, const picker::Vector3F& pointC, const picker::Vector3F& pointD )
{
	_vertices[0] = picker::ogl::Vertex( pointA, picker::Color( 1.0f, 1.0f, 1.0f ), picker::Vector3F( 0.0f, 0.0f, -1.0f ), picker::Vector2F( 0.0f, 0.0f ) );
	_vertices[1] = picker::ogl::Vertex( pointB, picker::Color( 1.0f, 1.0f, 1.0f ), picker::Vector3F( 0.0f, 0.0f, -1.0f ), picker::Vector2F( 0.0f, 0.0f ) );
	_vertices[2] = picker::ogl::Vertex( pointC, picker::Color( 1.0f, 1.0f, 1.0f ), picker::Vector3F( 0.0f, 0.0f, -1.0f ), picker::Vector2F( 0.0f, 0.0f ) );
	_vertices[3] = picker::ogl::Vertex( pointD, picker::Color( 1.0f, 1.0f, 1.0f ), picker::Vector3F( 0.0f, 0.0f, -1.0f ), picker::Vector2F( 0.0f, 0.0f ) );
}

picker::ogl::Quad::Quad( const ::std::array<picker::Vector3F, 4>& vertices )
{
	_vertices[0] = picker::ogl::Vertex( vertices[0], picker::Color( 1.0f, 1.0f, 1.0f ), picker::Vector3F( 0.0f, 0.0f, -1.0f ), picker::Vector2F( 0.0f, 0.0f ) );
	_vertices[1] = picker::ogl::Vertex( vertices[1], picker::Color( 1.0f, 1.0f, 1.0f ), picker::Vector3F( 0.0f, 0.0f, -1.0f ), picker::Vector2F( 0.0f, 0.0f ) );
	_vertices[2] = picker::ogl::Vertex( vertices[2], picker::Color( 1.0f, 1.0f, 1.0f ), picker::Vector3F( 0.0f, 0.0f, -1.0f ), picker::Vector2F( 0.0f, 0.0f ) );
	_vertices[3] = picker::ogl::Vertex( vertices[3], picker::Color( 1.0f, 1.0f, 1.0f ), picker::Vector3F( 0.0f, 0.0f, -1.0f ), picker::Vector2F( 0.0f, 0.0f ) );
}

picker::ogl::Quad::Quad( const picker::ogl::Vertex& vertexA, const picker::ogl::Vertex& vertexB, const picker::ogl::Vertex& vertexC, const picker::ogl::Vertex& vertexD )
{
	_vertices[0] = vertexA;
	_vertices[1] = vertexB;
	_vertices[2] = vertexC;
	_vertices[3] = vertexD;
}

picker::ogl::Quad::Quad( const ::std::array<picker::ogl::Vertex, 4>& vertices )
{
	_vertices = vertices;
}

const picker::ogl::Vertex& picker::ogl::Quad::getVertex( unsigned index ) const
{
	return _vertices[index];
}

picker::ogl::Vertex& picker::ogl::Quad::getVertex( unsigned index )
{
	return _vertices[index];
}

void picker::ogl::Quad::setVertex( unsigned index, const picker::ogl::Vertex& vertex )
{
	_vertices[index] = vertex;
}

void picker::ogl::Quad::draw( IRenderTarget& target ) const
{
	#if defined( PICKER_SYSTEM_WINDOWS )
	glBegin( GL_TRIANGLES );

	// Draw two triangles: 0-1-2 and 2-3-0.
	const ::std::array<unsigned int, 6> indexOrder = { 0, 1, 2, 2, 3, 0 };
	for( unsigned int i = 0; i < indexOrder.size(); i++ )
	{
		const unsigned int j = indexOrder[i];

		glTexCoord2f(	_vertices[j].getUvCoordinate().x,	_vertices[j].getUvCoordinate().y );
		glNormal3f(		_vertices[j].getNormal().x,			_vertices[j].getNormal().y,			_vertices[j].getNormal().z );
		glColor3f(		_vertices[j].getColor().r,			_vertices[j].getColor().g,			_vertices[j].getColor().b );
		glVertex3f(		_vertices[j].getPostion().x,		_vertices[j].getPostion().y,		_vertices[j].getPostion().z );
	}

	glEnd();
	#endif
}
