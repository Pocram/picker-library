#include <Picker/OpenGL/Material.hpp>
#include <Picker/OpenGL/ShaderUtility.hpp>
#include <Picker/OpenGL.hpp>
#include <iostream>

picker::ogl::Material::Material() :
	_shaderProgramId( 0 )
{
}

picker::ogl::Material::Material( const picker::ogl::VertexShader& vertexShader, const picker::ogl::FragmentShader& fragmentShader )
{
	if ( loadShaders( vertexShader, fragmentShader ) == false )
	{
		// TODO: Different exception?
		throw picker::ogl::ShaderFailedToCompile();
	}
}

picker::ogl::Material::Material( const ::std::string& pathToVertexShader, const ::std::string& pathToFragmentShader )
{
	if ( loadShaders( pathToVertexShader, pathToFragmentShader ) == false )
	{
		// TODO: Different exception?
		throw picker::ogl::ShaderFailedToCompile();
	}
}

picker::ogl::Material::~Material()
{
	glCheck( glDeleteProgram( _shaderProgramId ) );
}

bool picker::ogl::Material::loadShaders( const picker::ogl::VertexShader& vertexShader, const picker::ogl::FragmentShader& fragmentShader )
{
	bool haveShadersBeenLinked = picker::ogl::linkShaders( vertexShader.getShaderId(), fragmentShader.getShaderId(), _shaderProgramId );
	if ( haveShadersBeenLinked )
	{
		pollAllParameters();
	}
	return haveShadersBeenLinked;
}

bool picker::ogl::Material::loadShaders( const ::std::string& pathToVertexShader, const ::std::string& pathToFragmentShader )
{
	unsigned int vertexShaderId;
	unsigned int fragmentShaderId;

	::std::string vertexErrorMessage;
	if( picker::ogl::compileShader( pathToVertexShader, picker::ogl::ShaderType::Vertex, vertexShaderId, vertexErrorMessage ) == false )
	{
		::std::cout << "Error: Vertex shader at \"" << pathToVertexShader << "\" failed to compile:\n";
		::std::cout << "\t" << vertexErrorMessage << "\n";
		return false;
	}

	::std::string fragmentErrorMessage;
	if ( picker::ogl::compileShader( pathToFragmentShader, picker::ogl::ShaderType::Fragment, fragmentShaderId, fragmentErrorMessage ) == false )
	{
		::std::cout << "Error: Fragment shader at \"" << pathToFragmentShader << "\" failed to compile:\n";
		::std::cout << "\t" << vertexErrorMessage << "\n";
		return false;
	}

	bool haveShadersBeenLinked = picker::ogl::linkShaders( vertexShaderId, fragmentShaderId, _shaderProgramId );
	if( haveShadersBeenLinked )
	{
		pollAllParameters();
	}
	
	glCheck( glDeleteShader( vertexShaderId ) );
	glCheck( glDeleteShader( fragmentShaderId ) );

	return haveShadersBeenLinked;
}

unsigned int picker::ogl::Material::getShaderProgramId() const
{
	return _shaderProgramId;
}

bool picker::ogl::Material::pollParameterByName( const ::std::string& parameterName )
{
	int parameterId;
	glCheck( parameterId = glGetUniformLocation( _shaderProgramId, parameterName.c_str() ) );
	if ( parameterId >= 0 )
	{
		_shaderParameters[parameterName] = parameterId;
		return true;
	}
	return false;
}

int picker::ogl::Material::getParameterId( const ::std::string& parameterName ) const
{
	return _shaderParameters.at( parameterName );
}

void picker::ogl::Material::setFloat( const ::std::string& parameterName, float value )
{
	handleParameterSetterStart( parameterName );

	int parameterId = _shaderParameters[parameterName];
	_floatParameters[parameterId] = value;
}

void picker::ogl::Material::setInt( const ::std::string& parameterName, int value )
{
	handleParameterSetterStart( parameterName );

	int parameterId = _shaderParameters[parameterName];
	_intParameters[parameterId] = value;
}

void picker::ogl::Material::setVector( const ::std::string& parameterName, const picker::Vector2F& value )
{
	handleParameterSetterStart( parameterName );

	int parameterId = _shaderParameters[parameterName];
	_vector2fParameters[parameterId] = value;
}

void picker::ogl::Material::setVector( const ::std::string& parameterName, const picker::Vector2I& value )
{
	handleParameterSetterStart( parameterName );

	int parameterId = _shaderParameters[parameterName];
	_vector2iParameters[parameterId] = value;
}

void picker::ogl::Material::setVector( const ::std::string& parameterName, const picker::Vector2U& value )
{
	handleParameterSetterStart( parameterName );

	int parameterId = _shaderParameters[parameterName];
	_vector2uParameters[parameterId] = value;
}

void picker::ogl::Material::setVector( const ::std::string& parameterName, const picker::Vector3F& value )
{
	handleParameterSetterStart( parameterName );

	int parameterId = _shaderParameters[parameterName];
	_vector3fParameters[parameterId] = value;
}

void picker::ogl::Material::setVector( const ::std::string& parameterName, const picker::Vector3I& value )
{
	handleParameterSetterStart( parameterName );

	int parameterId = _shaderParameters[parameterName];
	_vector3iParameters[parameterId] = value;
}

void picker::ogl::Material::setVector( const ::std::string& parameterName, const picker::Vector3U& value )
{
	handleParameterSetterStart( parameterName );

	int parameterId = _shaderParameters[parameterName];
	_vector3uParameters[parameterId] = value;
}

void picker::ogl::Material::setColor( const ::std::string& parameterName, const picker::Color& color )
{
	setVector( parameterName, picker::Vector3F( color.r, color.g, color.b ) );
}

void picker::ogl::Material::setTexture( const ::std::string& parameterName, const picker::ogl::Texture& texture, unsigned int textureUnit )
{
	handleParameterSetterStart( parameterName );
	
	int parameterId = _shaderParameters[parameterName];
	_textureParameters[parameterId] = picker::ogl::Material::TextureRenderInfo( &texture, textureUnit );
}

void picker::ogl::Material::handleParameterSetterStart( const ::std::string& parameterName )
{
	checkForParameterValidity( parameterName );
}

void picker::ogl::Material::checkForParameterValidity( const ::std::string& parameterName )
{
	// Check if parameter has been polled before and poll if necessary.
	if ( _shaderParameters.find( parameterName ) == _shaderParameters.end() )
	{
		// If the poll returns false, the parameter does not exist.
		if ( pollParameterByName( parameterName ) == false )
		{
			//throw picker::ogl::MaterialParameterDoesNotExist( "The material parameter \"" + parameterName + "\" does not exist!" );
			::std::cout << "The material parameter \"" << parameterName << "\" does not exist!" << "\n";
		}
	}
}

void picker::ogl::Material::setUniforms() const
{
	glCheck( glUseProgram( _shaderProgramId ) );

	for ( auto pair : _intParameters )
	{
		glCheck( glUniform1i( pair.first, pair.second ) );
	}

	for ( auto pair : _floatParameters )
	{
		glCheck( glUniform1f( pair.first, pair.second ) );
	}


	for ( auto pair : _vector2fParameters )
	{
		glCheck( glUniform2f( pair.first, pair.second.x, pair.second.y ) );
	}

	for ( auto pair : _vector2iParameters )
	{
		glCheck( glUniform2i( pair.first, pair.second.x, pair.second.y ) );
	}

	for ( auto pair : _vector2uParameters )
	{
		glCheck( glUniform2ui( pair.first, pair.second.x, pair.second.y ) );
	}


	for ( auto pair : _vector3fParameters )
	{
		glCheck( glUniform3f( pair.first, pair.second.x, pair.second.y, pair.second.z ) );
	}

	for ( auto pair : _vector3iParameters )
	{
		glCheck( glUniform3i( pair.first, pair.second.x, pair.second.y, pair.second.z ) );
	}

	for ( auto pair : _vector3uParameters )
	{
		glCheck( glUniform3ui( pair.first, pair.second.x, pair.second.y, pair.second.z ) );
	}


	for ( auto pair : _textureParameters )
	{
		glCheck( glUniform1i( pair.first, pair.second.textureUnit ) );
		glCheck( glActiveTexture( GL_TEXTURE0 + pair.second.textureUnit ) );
		glCheck( glBindTexture( GL_TEXTURE_2D, pair.second.texture->_openGlTextureId ) );
	}
}

void picker::ogl::Material::pollAllParameters()
{
	int parameterCount;
	glCheck( glGetProgramiv( _shaderProgramId, GL_ACTIVE_UNIFORMS, &parameterCount ) );
	for ( int i = 0; i < parameterCount; i++ )
	{
		GLint			size;			// Size of the variable
		GLenum			type;			// Type of the variable (float, vec3 or mat4, etc)
		const GLsizei	bufSize = 64;	// Maximum name length
		GLchar			name[bufSize];	// Variable name in GLSL
		GLsizei			length;			// Name length
		glCheck( glGetActiveUniform( _shaderProgramId, static_cast<GLuint>( i ), bufSize, &length, &size, &type, name ) );
		pollParameterByName( name );
	}
}
