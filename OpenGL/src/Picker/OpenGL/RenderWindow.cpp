#include <Picker/OpenGL/RenderWindow.hpp>
#include <Picker/Color.hpp>
#include <Picker/OpenGL.hpp>
#include <iostream>

picker::ogl::RenderWindow::RenderWindow() : picker::Window()
	// Initialise platform dependant members.
	#if defined( PICKER_SYSTEM_WINDOWS )
		, _instanceHandle( nullptr ),
		_deviceContextHandle( nullptr ),
		_glResourceContext( nullptr )
	#endif
{
	glewExperimental = true;
	if( glewInit() != GLEW_OK )
	{
		::std::cout << "Failed to initialise GLEW!\n";
		// TODO: Throw exception
	}
}

picker::ogl::RenderWindow::RenderWindow( unsigned width, unsigned height, const ::std::string& title, unsigned char colorBits, unsigned char depthBits ) :
	picker::Window( width, height, title )
	// Initialise platform dependant members.
	#if defined( PICKER_SYSTEM_WINDOWS )
		, _instanceHandle( nullptr ),
		_deviceContextHandle( nullptr ),
		_glResourceContext( nullptr )
	#endif
{
	#if defined( PICKER_SYSTEM_WINDOWS )
	RECT rect;

	_deviceContextHandle = GetDC( getWindowHandle() );
	if ( _deviceContextHandle == nullptr )
	{
		// TODO: Throw exception?
	}

	setUpPixelFormat( colorBits, depthBits );

	_glResourceContext = wglCreateContext( _deviceContextHandle );
	focusOpenGlOnWindow();
	GetClientRect( getWindowHandle(), &rect );
	
	glClearDepth( 1.0f );
	glEnable( GL_DEPTH_TEST );

	glewExperimental = true;
	if ( glewInit() != GLEW_OK )
	{
		::std::cout << "Failed to initialise GLEW!\n";
		// TODO: Throw exception
	}

	#endif
}

picker::ogl::RenderWindow::~RenderWindow()
{
	#if defined( PICKER_SYSTEM_WINDOWS )
	if ( _deviceContextHandle == nullptr )
	{
		return;
	}

	wglMakeCurrent( _deviceContextHandle, nullptr );
	wglDeleteContext( _glResourceContext );
	#endif
}

void picker::ogl::RenderWindow::clearWindow( const picker::Color& clearColor )
{
	#if defined( PICKER_SYSTEM_WINDOWS )
	focusOpenGlOnWindow();
	glClearColor( clearColor.r, clearColor.g, clearColor.b, clearColor.a );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	#endif
}

void picker::ogl::RenderWindow::display()
{
	#if defined( PICKER_SYSTEM_WINDOWS )
	glFlush();
	SwapBuffers( _deviceContextHandle );
	#endif
}

#if defined( PICKER_SYSTEM_WINDOWS )
bool picker::ogl::RenderWindow::setUpPixelFormat( unsigned char colorBits, unsigned char depthBits ) const
{
	PIXELFORMATDESCRIPTOR pixelFormatDescriptor;
	pixelFormatDescriptor.nSize = sizeof( pixelFormatDescriptor );
	pixelFormatDescriptor.nVersion = 1;
	pixelFormatDescriptor.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pixelFormatDescriptor.dwLayerMask = PFD_MAIN_PLANE;
	pixelFormatDescriptor.iPixelType = PFD_TYPE_COLORINDEX;
	pixelFormatDescriptor.cColorBits = colorBits;
	pixelFormatDescriptor.cDepthBits = depthBits;
	pixelFormatDescriptor.cAccumBits = 0;
	pixelFormatDescriptor.cStencilBits = 0;

	int pixelFormat = ChoosePixelFormat( _deviceContextHandle, &pixelFormatDescriptor );
	if ( pixelFormat == 0 )
	{
		return false;
	}

	return SetPixelFormat( _deviceContextHandle, pixelFormat, &pixelFormatDescriptor ) == TRUE;
}

void picker::ogl::RenderWindow::focusOpenGlOnWindow()
{
	wglMakeCurrent( _deviceContextHandle, _glResourceContext );
}
#endif