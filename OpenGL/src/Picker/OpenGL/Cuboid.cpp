#include <Picker/OpenGL/Cuboid.hpp>
#include <Picker/OpenGL/IRenderTarget.hpp>

picker::ogl::Cuboid::Cuboid()
{
	_faces[0] = picker::ogl::Quad();
}

picker::ogl::Cuboid::Cuboid( const picker::Vector3F& corner, const picker::Vector3F& oppositeCorner )
{
}

void picker::ogl::Cuboid::draw( IRenderTarget& target ) const
{
	for( unsigned char i = 0; i < _faces.size(); i++ )
	{
		target.draw( _faces[i] );
	}
}
