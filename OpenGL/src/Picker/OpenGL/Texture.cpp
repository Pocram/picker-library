#include <Picker/OpenGL/Texture.hpp>
#include <Picker/OpenGL.hpp>

picker::ogl::Texture::Texture() :
	_openGlTextureId( 0 ),
	_width( 0 ),
	_height( 0 )
{
}

picker::ogl::Texture::Texture( const Texture& copy ) :
	_openGlTextureId( 0 ),
	_width( 0 ),
	_height( 0 )
{
	// No texture ID? Then we don't need to copy it.
	if( copy._openGlTextureId == 0 )
	{
		return;
	}

	// TODO: Further implementation.
}

picker::ogl::Texture::~Texture()
{
	// No texture ID? Then we don't need to destroy it.
	if ( _openGlTextureId == 0 )
	{
		return;
	}

	glDeleteTextures( 1, &_openGlTextureId );
}

bool picker::ogl::Texture::create( unsigned width, unsigned height )
{
	if( width == 0 || height == 0 )
	{
		// Invalid size.
		return false;
	}

	_width	= width;
	_height	= height;

	// Only create a new ID if there currently is none.
	if( _openGlTextureId == 0 )
	{
		GLuint textureId;
		glGenTextures( 1, &textureId );
		_openGlTextureId = static_cast<unsigned int>( textureId );
		
		glCheck( glBindTexture( GL_TEXTURE_2D, _openGlTextureId ) );
		glCheck( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT ) );
		glCheck( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT ) );
		glCheck( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST ) );
		glCheck( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST ) );
	}

	return true;
}

bool picker::ogl::Texture::loadFromImage( const picker::Image& image )
{
	if ( create( image.getDimensions().x, image.getDimensions().y ) == false )
	{
		return false;
	}

	update( image );
	return true;
}

bool picker::ogl::Texture::loadFromFile( const ::std::string& pathToFile )
{
	picker::Image image;
	return image.loadFromFile( pathToFile ) && loadFromImage( image );
}

void picker::ogl::Texture::update( const picker::Image& image )
{
	glCheck( glBindTexture( GL_TEXTURE_2D, _openGlTextureId ) );
	glCheck( glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, image.getDimensions().x, image.getDimensions().y, 0, GL_RGBA, GL_UNSIGNED_BYTE, &image.getPixels()[0] ) );
	glCheck( glFlush() );
}
