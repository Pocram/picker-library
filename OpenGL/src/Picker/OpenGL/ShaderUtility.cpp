#include <Picker/OpenGL/ShaderUtility.hpp>
#include <Picker/OpenGL.hpp>

#include <iostream>
#include <fstream>

bool picker::ogl::compileShader( const ::std::string& pathToShaderFile, ShaderType shaderType, unsigned int& outShaderId )
{
	::std::string outMessage;
	return picker::ogl::compileShader( pathToShaderFile, shaderType, outShaderId, outMessage );
}

bool picker::ogl::compileShader( const ::std::string& pathToShaderFile, ShaderType shaderType, unsigned int& outShaderId, ::std::string& outErrorMessage )
{
	::std::ifstream	file( pathToShaderFile, ::std::ios::in );
	::std::string	fileContent;

	if ( file.is_open() == false )
	{
		return false;
	}
	else
	{
		::std::string line;
		while ( ::std::getline( file, line ) )
		{
			fileContent += "\n" + line;
		}
		file.close();
	}

	int glShaderType;
	switch( shaderType )
	{
		case Vertex:
			glShaderType = GL_VERTEX_SHADER;
			break;
		case Fragment: 
			glShaderType = GL_FRAGMENT_SHADER;
			break;
		default:
			return false;
	}
	int shaderId = glCreateShader( glShaderType );

	// Compile
	const char* code = fileContent.c_str();
	glShaderSource( shaderId, 1, &code, nullptr );
	glCompileShader( shaderId );

	int shaderLoadResult = GL_FALSE;
	glGetShaderiv( shaderId, GL_COMPILE_STATUS, &shaderLoadResult );

	if ( shaderLoadResult == GL_TRUE )
	{
		outShaderId = shaderId;
	}
	else
	{
		char message[1024];
		memset( message, 0, 1024 );
		glGetShaderInfoLog( shaderId, 1023, nullptr, message );
		::std::cout << "ERROR: Could not load shader file at \"" << pathToShaderFile << "\": " << message << "\n";
		outErrorMessage = message;
		return false;
	}
	return shaderLoadResult == GL_TRUE;
}

bool picker::ogl::linkShaders( int vertexShaderId, int fragmentShaderId, unsigned int& outShaderProgramId )
{
	::std::string outMessage;
	return picker::ogl::linkShaders( vertexShaderId, fragmentShaderId, outShaderProgramId, outMessage );
}

bool picker::ogl::linkShaders( int vertexShaderId, int fragmentShaderId, unsigned int& outShaderProgramId, ::std::string& outErrorMessage )
{
	int shaderProgramId = glCreateProgram();

	glAttachShader( shaderProgramId, vertexShaderId );
	glAttachShader( shaderProgramId, fragmentShaderId );

	glLinkProgram( shaderProgramId );

	// Check for linker errors and get error message.
	int linkResult;
	glGetProgramiv( shaderProgramId, GL_LINK_STATUS, &linkResult );
	if ( linkResult == GL_FALSE )
	{
		char message[1024];
		memset( message, 0, 1024 );
		glGetShaderInfoLog( shaderProgramId, 1023, nullptr, message );
		::std::cout << "ERROR: Could not link shaders: " << message << "\n";
		outErrorMessage = message;
		return false;
	}

	glDetachShader( shaderProgramId, vertexShaderId );
	glDetachShader( shaderProgramId, fragmentShaderId );

	outShaderProgramId = shaderProgramId;
	return true;
}
