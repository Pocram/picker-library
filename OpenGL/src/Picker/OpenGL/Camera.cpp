#include <Picker/OpenGL/Camera.hpp>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>

picker::ogl::Camera::Camera() : 
	_aspectRatio( 16.0f / 9.0f ),
	_fieldOfView( 90.0f ),
	_viewDirection( 0.0f, 0.0f, -1.0f )
{
	updateProjectionMatrix();
	_viewMatrix = glm::lookAt( glm::vec3( 0.0f, 0.0f, 0.0f ), glm::vec3( 0.0f, 0.0f, 0.0f ), glm::vec3( 0.0f, 1.0f, 0.0f ) );
}

picker::ogl::Camera::Camera( const picker::Vector3F& position, float aspectRatio, float fieldOfView, float nearClippingDistance, float farClippingDistance ) :
	_aspectRatio( aspectRatio ),
	_fieldOfView( fieldOfView ),
	_position( position ),
	_viewDirection( 0.0f, 0.0f, -1.0f )
{
	updateProjectionMatrix();
	_viewMatrix	= glm::lookAt( glm::vec3( _position.x, _position.y, _position.z ), glm::vec3( _position.x, _position.y, _position.z ) + glm::vec3( _viewDirection.x, _viewDirection.y, _viewDirection.z ), glm::vec3( 0.0f, 1.0f, 0.0f ) );
}

void picker::ogl::Camera::setPosition( const picker::Vector3F& position )
{
	_position = position;
	_viewMatrix = glm::lookAt( glm::vec3( _position.x, _position.y, _position.z ), glm::vec3( _position.x, _position.y, _position.z ) + glm::vec3( _viewDirection.x, _viewDirection.y, _viewDirection.z ), glm::vec3( 0.0f, 1.0f, 0.0f ) );
}

picker::Vector3F picker::ogl::Camera::getPosition() const
{
	return _position;
}

const glm::mat4& picker::ogl::Camera::getProjectionMatrix() const
{
	return _projectionMatrix;
}

const glm::mat4& picker::ogl::Camera::getViewMatrix() const
{
	return _viewMatrix;
}

void picker::ogl::Camera::lookAt( const picker::Vector3F& target, const picker::Vector3F up )
{
	_viewDirection = target - _position;
	_viewMatrix	= glm::lookAt( glm::vec3( _position.x, _position.y, _position.z ), glm::vec3( _position.x, _position.y, _position.z ) + glm::vec3( _viewDirection.x, _viewDirection.y, _viewDirection.z ), glm::vec3( 0.0f, 1.0f, 0.0f ) );
}

void picker::ogl::Camera::lookInDirection( const picker::Vector3F& direction, const picker::Vector3F up )
{
	_viewDirection = direction;
	_viewMatrix = glm::lookAt( glm::vec3( _position.x, _position.y, _position.z ), glm::vec3( _position.x, _position.y, _position.z ) + glm::vec3( _viewDirection.x, _viewDirection.y, _viewDirection.z ), glm::vec3( 0.0f, 1.0f, 0.0f ) );
}

void picker::ogl::Camera::setAspectRatio( float aspectRatio )
{
	_aspectRatio = aspectRatio;
	updateProjectionMatrix();
}

void picker::ogl::Camera::setFieldOfView( float fieldOfView )
{
	_fieldOfView = fieldOfView;
	updateProjectionMatrix();
}

void picker::ogl::Camera::updateProjectionMatrix()
{
	_projectionMatrix = glm::perspective( _fieldOfView, _aspectRatio, 0.01f, 100.0f );
}
