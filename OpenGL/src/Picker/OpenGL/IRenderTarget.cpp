#include <Picker/OpenGL/IRenderTarget.hpp>
#include <Picker/OpenGL/IDrawable.hpp>
#include <Picker/OpenGL/Material.hpp>
#include <Picker/OpenGL/Transform.hpp>
#include <Picker/OpenGL/Camera.hpp>
#include <Picker/OpenGL.hpp>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

picker::ogl::IRenderTarget::~IRenderTarget()
{
}

void picker::ogl::IRenderTarget::draw( const IDrawable& drawableObject )
{
	glCheck( glUseProgram( 0 ) );
	drawableObject.draw( *this );
}

void picker::ogl::IRenderTarget::draw( const IDrawable& drawableObject, const picker::ogl::Material& material, const picker::ogl::Camera& camera, const picker::ogl::Transform& transform )
{
	glCheck( glUseProgram( material.getShaderProgramId() ) );
	
	// Calculate MVP matrix and send it to the shader.
	glm::mat4 mvpMatrix = camera.getProjectionMatrix() * camera.getViewMatrix() * transform.getModelMatrix();
	glCheck( glUniformMatrix4fv( material.getParameterId( "MVP" ), 1, GL_FALSE, &mvpMatrix[0][0] ) );

	material.setUniforms();

	drawableObject.draw( *this );
}
