#include <Picker/OpenGL/VertexShader.hpp>
#include <Picker/OpenGL/ShaderUtility.hpp>

picker::ogl::VertexShader::VertexShader()
{
}

picker::ogl::VertexShader::VertexShader( const ::std::string& pathToShaderFile )
{
	if( loadFromFile( pathToShaderFile ) == false )
	{
		throw picker::ogl::ShaderFailedToCompile( "The vertex shader at \"" + pathToShaderFile + "\" could not be compiled!" );
	}
}

bool picker::ogl::VertexShader::loadFromFile( const ::std::string& pathToShaderFile )
{
	return picker::ogl::compileShader( pathToShaderFile, picker::ogl::ShaderType::Vertex, _shaderId, _compilationErrorMessage );
}
