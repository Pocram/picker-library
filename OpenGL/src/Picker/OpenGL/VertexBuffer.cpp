#include <Picker/OpenGL/VertexBuffer.hpp>
#include <Picker/OpenGL/IRenderTarget.hpp>
#include <Picker/OpenGL.hpp>

picker::ogl::VertexBuffer::VertexBuffer() : 
	_arrayId( 0 ), 
	_positionBufferId( 0 ), 
	_colorBufferId( 0 ), 
	_normalBufferId( 0 ), 
	_uvBufferId( 0 )
{
}

picker::ogl::VertexBuffer::VertexBuffer( const ::std::vector<picker::ogl::Vertex>& vertices ) :
	_vertices( vertices ),
	_positionBufferId( 0 )
{
	createBuffersFromVertexVector( vertices );
}

picker::ogl::VertexBuffer::VertexBuffer( const picker::ogl::VertexBuffer& other )
{
	_vertices = other._vertices;
	createBuffersFromVertexVector( _vertices );
}

picker::ogl::VertexBuffer& picker::ogl::VertexBuffer::operator=( const picker::ogl::VertexBuffer& other )
{
	_vertices = other._vertices;
	createBuffersFromVertexVector( _vertices );
	return *this;
}

picker::ogl::VertexBuffer::~VertexBuffer()
{
	glDeleteBuffers( 1, &_positionBufferId );
	glDeleteBuffers( 1, &_colorBufferId );
	glDeleteBuffers( 1, &_normalBufferId );
	glDeleteBuffers( 1, &_uvBufferId );
}

void picker::ogl::VertexBuffer::draw( IRenderTarget& target ) const
{
	if( _vertices.size() == 0 )
	{
		return;
	}

	// 0: Positions
	glCheck( glEnableVertexAttribArray( 0 ) );
	glCheck( glBindBuffer( GL_ARRAY_BUFFER, _positionBufferId ) );
	glCheck( glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, nullptr ) );
	
	// 1: Colors
	glCheck( glEnableVertexAttribArray( 1 ) );
	glCheck( glBindBuffer( GL_ARRAY_BUFFER, _colorBufferId ) );
	glCheck( glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, 0, nullptr ) );

	// 2: Normals
	glCheck( glEnableVertexAttribArray( 2 ) );
	glCheck( glBindBuffer( GL_ARRAY_BUFFER, _normalBufferId ) );
	glCheck( glVertexAttribPointer( 2, 3, GL_FLOAT, GL_FALSE, 0, nullptr ) );

	// 3: UVs
	glCheck( glEnableVertexAttribArray( 3 ) );
	glCheck( glBindBuffer( GL_ARRAY_BUFFER, _uvBufferId ) );
	glCheck( glVertexAttribPointer( 3, 2, GL_FLOAT, GL_FALSE, 0, nullptr ) );

	//glCheck( glBindVertexArray( _arrayId ) );
	glCheck( glBindBuffer( GL_ARRAY_BUFFER, _positionBufferId ) );
	glCheck( glDrawArrays( GL_TRIANGLES, 0, _vertices.size() ) );
	
	glCheck( glDisableVertexAttribArray( 0 ) );
	glCheck( glDisableVertexAttribArray( 1 ) );
	glCheck( glDisableVertexAttribArray( 2 ) );
	glCheck( glDisableVertexAttribArray( 3 ) );
	glCheck( glBindBuffer( GL_ARRAY_BUFFER, _uvBufferId ) );
}

void picker::ogl::VertexBuffer::createBuffersFromVertexVector( const ::std::vector<picker::ogl::Vertex>& vertices )
{
	if( vertices.size() == 0 )
	{
		return;
	}

	::std::vector<float> positions( 3 * vertices.size() );
	::std::vector<float> colors( 3 * vertices.size() );
	::std::vector<float> normals( 3 * vertices.size() );
	::std::vector<float> uvs( 2 * vertices.size() );

	for ( unsigned int i = 0; i < vertices.size(); i++ )
	{
		positions[3 * i + 0] = vertices[i].getPostion().x;
		positions[3 * i + 1] = vertices[i].getPostion().y;
		positions[3 * i + 2] = vertices[i].getPostion().z;

		colors[3 * i + 0] = vertices[i].getColor().r;
		colors[3 * i + 1] = vertices[i].getColor().g;
		colors[3 * i + 2] = vertices[i].getColor().b;

		normals[3 * i + 0] = vertices[i].getNormal().x;
		normals[3 * i + 1] = vertices[i].getNormal().y;
		normals[3 * i + 2] = vertices[i].getNormal().z;

		uvs[2 * i + 0] = vertices[i].getUvCoordinate().x;
		uvs[2 * i + 1] = vertices[i].getUvCoordinate().y;
	}

	glCheck( glGenVertexArrays( 1, &_arrayId ) );
	glCheck( glBindVertexArray( _arrayId ) );

	glCheck( glGenBuffers( 1, &_positionBufferId ) );
	glCheck( glBindBuffer( GL_ARRAY_BUFFER, _positionBufferId ) );
	glCheck( glBufferData( GL_ARRAY_BUFFER, sizeof( float ) * positions.size(), &positions[0], GL_STATIC_DRAW ) );

	glCheck( glGenBuffers( 1, &_colorBufferId ) );
	glCheck( glBindBuffer( GL_ARRAY_BUFFER, _colorBufferId ) );
	glCheck( glBufferData( GL_ARRAY_BUFFER, sizeof( float ) * colors.size(), &colors[0], GL_STATIC_DRAW ) );

	glCheck( glGenBuffers( 1, &_normalBufferId ) );
	glCheck( glBindBuffer( GL_ARRAY_BUFFER, _normalBufferId ) );
	glCheck( glBufferData( GL_ARRAY_BUFFER, sizeof( float ) * normals.size(), &normals[0], GL_STATIC_DRAW ) );

	glCheck( glGenBuffers( 1, &_uvBufferId ) );
	glCheck( glBindBuffer( GL_ARRAY_BUFFER, _uvBufferId ) );
	glCheck( glBufferData( GL_ARRAY_BUFFER, sizeof( float ) * uvs.size(), &uvs[0], GL_STATIC_DRAW ) );
}
