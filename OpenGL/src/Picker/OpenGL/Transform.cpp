#include <Picker/OpenGL/Transform.hpp>

#define _USE_MATH_DEFINES 
#include <math.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>

picker::ogl::Transform::Transform() :
	_position( 0.0f, 0.0f, 0.0f ),
	_rotation( 0.0f, 0.0f, 0.0f ),
	_scale( 1.0f, 1.0f, 1.0f ),
	_modelMatrix( 1.0f )
{
	updateModelMatrix();
}

const glm::mat4& picker::ogl::Transform::getModelMatrix() const
{
	return _modelMatrix;
}

picker::ogl::Transform::Transform( const picker::Vector3F& position, const picker::Vector3F& rotation, const picker::Vector3F& scale ) :
	_position( position ),
	_rotation( rotation ),
	_scale( scale ),
	_modelMatrix( 1.0f )
{
	updateModelMatrix();
}

const picker::Vector3F& picker::ogl::Transform::getPosition() const
{
	return _position;
}

picker::Vector3F& picker::ogl::Transform::getPosition()
{
	return _position;
}

void picker::ogl::Transform::setPosition( const picker::Vector3F& position )
{
	_position = position;
	updateModelMatrix();
}

void picker::ogl::Transform::updateModelMatrix()
{
	picker::Vector3F radianRotation = _rotation * static_cast<float>( M_PI ) / 180.0f;

	glm::mat4 rotationMatrix	= glm::eulerAngleXYZ( radianRotation.x, radianRotation.y, radianRotation.z ) ;
	glm::mat4 scaleMatrix		= glm::scale( glm::mat4( 1.0f ), glm::vec3( _scale.x, _scale.y, _scale.z ) );
	glm::mat4 translationMatrix = glm::translate( glm::mat4( 1.0f ), glm::vec3( _position.x, _position.y, _position.z ) );
	_modelMatrix				= scaleMatrix * translationMatrix * rotationMatrix;
}

const picker::Vector3F& picker::ogl::Transform::getRotation() const
{
	return _rotation;
}

picker::Vector3F& picker::ogl::Transform::getRotation()
{
	return _rotation;
}

void picker::ogl::Transform::setRotation( const picker::Vector3F& rotation )
{
	_rotation = rotation;
	updateModelMatrix();
}

const picker::Vector3F& picker::ogl::Transform::getScale() const
{
	return _scale;
}

picker::Vector3F& picker::ogl::Transform::getScale()
{
	return _scale;
}

void picker::ogl::Transform::setScale( const picker::Vector3F& scale )
{
	_scale = scale;
	updateModelMatrix();
}
