#include <Picker/OpenGL/FragmentShader.hpp>
#include <Picker/OpenGL/ShaderUtility.hpp>

picker::ogl::FragmentShader::FragmentShader()
{
}

picker::ogl::FragmentShader::FragmentShader( const ::std::string& pathToShaderFile )
{
	if ( loadFromFile( pathToShaderFile ) == false )
	{
		throw picker::ogl::ShaderFailedToCompile( "The fragment shader at \"" + pathToShaderFile + "\" could not be compiled!" );
	}
}

bool picker::ogl::FragmentShader::loadFromFile( const ::std::string& pathToShaderFile )
{
	return picker::ogl::compileShader( pathToShaderFile, picker::ogl::ShaderType::Fragment, _shaderId, _compilationErrorMessage );
}
