#include <Picker/OpenGL/Skybox.hpp>
#include <Picker/OpenGL/IRenderTarget.hpp>
#include <Picker/OpenGL.hpp>

picker::ogl::Skybox::Skybox()
{
	generateVertexBuffer();
}

void picker::ogl::Skybox::draw( IRenderTarget& target ) const
{
	// Render the skybox in the background.
	glCheck( glDepthMask( GL_FALSE ) );

	_vertexBuffer.draw( target );

	glCheck( glDepthMask( GL_TRUE ) );
}

void picker::ogl::Skybox::generateVertexBuffer()
{
	::std::vector<picker::ogl::Vertex> vertexBuffer;

	const picker::Vector2F uvRatios( 1.0f / 4.0f, 1.0f / 3.0f );

	// Bottom
	const picker::ogl::Vertex b00( picker::Vector3F( -1.0f, -1.0f, -1.0f ),	picker::Vector3F( 0.0f, -1.0f, 0.0f ),	picker::Vector2F( 1.0f * uvRatios.x, 2.0f * uvRatios.y ) );
	const picker::ogl::Vertex b01( picker::Vector3F( -1.0f, -1.0f, 1.0f ),	picker::Vector3F( 0.0f, -1.0f, 0.0f ),	picker::Vector2F( 1.0f * uvRatios.x, 3.0f * uvRatios.y ) );
	const picker::ogl::Vertex b11( picker::Vector3F( 1.0f, -1.0f, 1.0f ),	picker::Vector3F( 0.0f, -1.0f, 0.0f ),	picker::Vector2F( 2.0f * uvRatios.x, 3.0f * uvRatios.y ) );
	const picker::ogl::Vertex b10( picker::Vector3F( 1.0f, -1.0f, -1.0f ),	picker::Vector3F( 0.0f, -1.0f, 0.0f ),	picker::Vector2F( 2.0f * uvRatios.x, 2.0f * uvRatios.y ) );

	vertexBuffer.push_back( b00 );
	vertexBuffer.push_back( b01 );
	vertexBuffer.push_back( b11 );
	vertexBuffer.push_back( b00 );
	vertexBuffer.push_back( b11 );
	vertexBuffer.push_back( b10 );

	// Top
	const const picker::ogl::Vertex t00( picker::Vector3F( -1.0f, 1.0f, -1.0f ),	picker::Vector3F( 0.0f, 1.0f, 0.0f ),	picker::Vector2F( 1.0f * uvRatios.x, 1.0f * uvRatios.y ) );
	const const picker::ogl::Vertex t01( picker::Vector3F( -1.0f, 1.0f, 1.0f ),		picker::Vector3F( 0.0f, 1.0f, 0.0f ),	picker::Vector2F( 1.0f * uvRatios.x, 0.0f * uvRatios.y ) );
	const const picker::ogl::Vertex t11( picker::Vector3F( 1.0f, 1.0f, 1.0f ),		picker::Vector3F( 0.0f, 1.0f, 0.0f ),	picker::Vector2F( 2.0f * uvRatios.x, 0.0f * uvRatios.y ) );
	const const picker::ogl::Vertex t10( picker::Vector3F( 1.0f, 1.0f, -1.0f ),		picker::Vector3F( 0.0f, 1.0f, 0.0f ),	picker::Vector2F( 2.0f * uvRatios.x, 1.0f * uvRatios.y ) );

	vertexBuffer.push_back( t00 );
	vertexBuffer.push_back( t01 );
	vertexBuffer.push_back( t11 );
	vertexBuffer.push_back( t00 );
	vertexBuffer.push_back( t11 );
	vertexBuffer.push_back( t10 );

	// North
	const picker::ogl::Vertex n00( picker::Vector3F( -1.0f, -1.0f, -1.0f ),	picker::Vector3F( 0.0f, 0.0f, -1.0f ),	picker::Vector2F( 1.0f * uvRatios.x, 2.0f * uvRatios.y ) );
	const picker::ogl::Vertex n01( picker::Vector3F( -1.0f, 1.0f, -1.0f ),	picker::Vector3F( 0.0f, 0.0f, -1.0f ),	picker::Vector2F( 1.0f * uvRatios.x, 1.0f * uvRatios.y ) );
	const picker::ogl::Vertex n11( picker::Vector3F( 1.0f, 1.0f, -1.0f ),	picker::Vector3F( 0.0f, 0.0f, -1.0f ),	picker::Vector2F( 2.0f * uvRatios.x, 1.0f * uvRatios.y ) );
	const picker::ogl::Vertex n10( picker::Vector3F( 1.0f, -1.0f, -1.0f ),	picker::Vector3F( 0.0f, 0.0f, -1.0f ),	picker::Vector2F( 2.0f * uvRatios.x, 2.0f * uvRatios.y ) );

	vertexBuffer.push_back( n00 );
	vertexBuffer.push_back( n01 );
	vertexBuffer.push_back( n11 );
	vertexBuffer.push_back( n00 );
	vertexBuffer.push_back( n11 );
	vertexBuffer.push_back( n10 );

	// South
	const picker::ogl::Vertex s00( picker::Vector3F( -1.0f, -1.0f, 1.0f ),	picker::Vector3F( 0.0f, 0.0f, 1.0f ),	picker::Vector2F( 4.0f * uvRatios.x, 2.0f * uvRatios.y ) );
	const picker::ogl::Vertex s01( picker::Vector3F( -1.0f, 1.0f, 1.0f ),	picker::Vector3F( 0.0f, 0.0f, 1.0f ),	picker::Vector2F( 4.0f * uvRatios.x, 1.0f * uvRatios.y ) );
	const picker::ogl::Vertex s11( picker::Vector3F( 1.0f, 1.0f, 1.0f ),	picker::Vector3F( 0.0f, 0.0f, 1.0f ),	picker::Vector2F( 3.0f * uvRatios.x, 1.0f * uvRatios.y ) );
	const picker::ogl::Vertex s10( picker::Vector3F( 1.0f, -1.0f, 1.0f ),	picker::Vector3F( 0.0f, 0.0f, 1.0f ),	picker::Vector2F( 3.0f * uvRatios.x, 2.0f * uvRatios.y ) );

	vertexBuffer.push_back( s00 );
	vertexBuffer.push_back( s01 );
	vertexBuffer.push_back( s11 );
	vertexBuffer.push_back( s00 );
	vertexBuffer.push_back( s11 );
	vertexBuffer.push_back( s10 );

	// East
	const picker::ogl::Vertex e00( picker::Vector3F( 1.0f, -1.0f, -1.0f ),	picker::Vector3F( 1.0f, 0.0f, 0.0f ),	picker::Vector2F( 2.0f * uvRatios.x, 2.0f * uvRatios.y ) );
	const picker::ogl::Vertex e01( picker::Vector3F( 1.0f, -1.0f, 1.0f ),	picker::Vector3F( 1.0f, 0.0f, 0.0f ),	picker::Vector2F( 3.0f * uvRatios.x, 2.0f * uvRatios.y ) );
	const picker::ogl::Vertex e11( picker::Vector3F( 1.0f, 1.0f, 1.0f ),	picker::Vector3F( 1.0f, 0.0f, 0.0f ),	picker::Vector2F( 3.0f * uvRatios.x, 1.0f * uvRatios.y ) );
	const picker::ogl::Vertex e10( picker::Vector3F( 1.0f, 1.0f, -1.0f ),	picker::Vector3F( 1.0f, 0.0f, 0.0f ),	picker::Vector2F( 2.0f * uvRatios.x, 1.0f * uvRatios.y ) );

	vertexBuffer.push_back( e00 );
	vertexBuffer.push_back( e01 );
	vertexBuffer.push_back( e11 );
	vertexBuffer.push_back( e00 );
	vertexBuffer.push_back( e11 );
	vertexBuffer.push_back( e10 );

	// West
	const picker::ogl::Vertex w00( picker::Vector3F( -1.0f,	-1.0f, -1.0f ),	picker::Vector3F( -1.0f, 0.0f, 0.0f ),	picker::Vector2F( 1.0f * uvRatios.x, 2.0f * uvRatios.y ) );
	const picker::ogl::Vertex w01( picker::Vector3F( -1.0f, -1.0f, 1.0f ),	picker::Vector3F( -1.0f, 0.0f, 0.0f ),	picker::Vector2F( 0.0f * uvRatios.x, 2.0f * uvRatios.y ) );
	const picker::ogl::Vertex w11( picker::Vector3F( -1.0f,	1.0f, 1.0f ),	picker::Vector3F( -1.0f, 0.0f, 0.0f ),	picker::Vector2F( 0.0f * uvRatios.x, 1.0f * uvRatios.y ) );
	const picker::ogl::Vertex w10( picker::Vector3F( -1.0f,	1.0f, -1.0f ),	picker::Vector3F( -1.0f, 0.0f, 0.0f ),	picker::Vector2F( 1.0f * uvRatios.x, 1.0f * uvRatios.y ) );

	vertexBuffer.push_back( w00 );
	vertexBuffer.push_back( w01 );
	vertexBuffer.push_back( w11 );
	vertexBuffer.push_back( w00 );
	vertexBuffer.push_back( w11 );
	vertexBuffer.push_back( w10 );

	_vertexBuffer = picker::ogl::VertexBuffer( vertexBuffer );
}
