#include <Picker/OpenGL/Mesh.hpp>

#include <Picker/OpenGL.hpp>
#include <Picker/String.hpp>
#include <Picker/OpenGL/IRenderTarget.hpp>
#include <Picker/OpenGL/Triangle.hpp>
#include <Picker/OpenGL.hpp>

#include <fstream>

picker::ogl::Mesh::Mesh()// : _vertexBuffer()
{
}

bool picker::ogl::Mesh::loadFromFile( const ::std::string& path )
{
	::std::ifstream file( path );
	if( file.is_open() == false )
	{
		return false;
	}

	// We temporarily create a new mesh instance.
	// Any loading actions are applied to this mesh.
	// If loading fails, the original object is not altered.
	// If it succeeds, the contents are applied to the current object.
	picker::ogl::Mesh tempMesh;

	::std::string line;
	while ( ::std::getline( file, line ) )
	{
		// Vertex position handling.
		if ( line.find( "v " ) != line.npos )
		{
			// We know that the line starts with "v  ", which is 3 characters long.
			::std::string positionsString = line.substr( 3 );
			::std::vector<::std::string> positionStrings = picker::splitString( positionsString, " " );
			picker::Vector3F position;
			try
			{
				position.x = ::std::stof( positionStrings[0] );
				position.y = ::std::stof( positionStrings[1] );
				position.z = ::std::stof( positionStrings[2] );
				tempMesh._vertexPositions.push_back( position );
			}
			catch ( ::std::invalid_argument )
			{
				return false;
			}
			catch ( ::std::out_of_range )
			{
				return false;
			}
		}
		// Vertex UV handling.
		else if( line.find("vt ") != line.npos )
		{
			// We know that the line starts with "vt ", which is 3 characters long.
			::std::string uvString = line.substr( 3 );
			::std::vector<::std::string> uvStrings = picker::splitString( uvString, " " );
			picker::Vector2F uvCoordinate;
			try
			{
				uvCoordinate.x = ::std::stof( uvStrings[0] );
				uvCoordinate.y = ::std::stof( uvStrings[1] );
				tempMesh._uvCoordinates.push_back( uvCoordinate );
			}
			catch ( ::std::invalid_argument )
			{
				return false;
			}
			catch ( ::std::out_of_range )
			{
				return false;
			}
		}
		// Vertex normal handling.
		else if ( line.find( "vn " ) != line.npos )
		{
			// We know that the line starts with "vn ", which is 3 characters long.
			::std::string normalString = line.substr( 3 );
			::std::vector<::std::string> normalStrings = picker::splitString( normalString, " " );
			picker::Vector3F normal;
			try
			{
				normal.x = ::std::stof( normalStrings[0] );
				normal.y = ::std::stof( normalStrings[1] );
				normal.z = ::std::stof( normalStrings[2] );
				tempMesh._normals.push_back( normal );
			}
			catch ( ::std::invalid_argument )
			{
				return false;
			}
			catch ( ::std::out_of_range )
			{
				return false;
			}
		}
		// Face handling.
		else if ( line.find( "f " ) != line.npos )
		{
			// We know that the line starts with "f ", which is 2 characters long.
			::std::string faceString = line.substr( 2 );
			::std::vector<::std::string> faceStrings = picker::splitString( faceString, " " );
			
			// Only triangles and quads are supported.
			if( faceStrings.size() != 3 && faceStrings.size() != 4 )
			{
				// Return here instead of continuing because 
				// one broken face leads to a broken model.
				return false;
			}

			::std::vector<picker::ogl::Mesh::VertexIndeces> vertices;
			// Iterate over all vertices of a face and collect them in the vector above.
			for ( unsigned int i = 0; i < faceStrings.size(); i++ )
			{
				::std::string vertexString = faceStrings[i];
				::std::vector<::std::string> vertexStrings = picker::splitString( vertexString, "/", "0" );
				if( vertexStrings.size() == 0 )
				{
					return false;
				}

				picker::ogl::Mesh::VertexIndeces vertex = {};
				try
				{
					// Wavefront .obj files are one-indexed.
					if( vertexStrings.size() >= 1 ) vertex.positionIndex		= ::std::stoi( vertexStrings[0] ) - 1;
					if( vertexStrings.size() >= 2 ) vertex.uvCoordinateIndex	= ::std::stoi( vertexStrings[1] ) - 1;
					if( vertexStrings.size() >= 3 ) vertex.normalIndex			= ::std::stoi( vertexStrings[2] ) - 1;
					vertices.push_back( vertex );
				}
				catch ( ::std::invalid_argument )
				{
					return false;
				}
				catch ( ::std::out_of_range )
				{
					return false;
				}
			}

			if( vertices.size() == 3 )
			{
				picker::ogl::Mesh::VertexIndexTriple triangle;
				triangle.a = vertices[0];
				triangle.b = vertices[1];
				triangle.c = vertices[2];
				tempMesh._triangles.push_back( triangle );
			}
			else if( vertices.size() == 4 )
			{
				picker::ogl::Mesh::VertexIndexTriple triangleA;
				triangleA.a = vertices[0];
				triangleA.b = vertices[1];
				triangleA.c = vertices[2];

				picker::ogl::Mesh::VertexIndexTriple triangleB;
				triangleB.a = vertices[2];
				triangleB.b = vertices[3];
				triangleB.c = vertices[0];

				tempMesh._triangles.push_back( triangleA );
				tempMesh._triangles.push_back( triangleB );
			}
			else
			{
				// Only triangles and quads are supported as of now.
				return false;
			}
		}
	}

	_vertexPositions	= tempMesh._vertexPositions;
	_uvCoordinates		= tempMesh._uvCoordinates;
	_normals			= tempMesh._normals;
	_triangles			= tempMesh._triangles;
	return true;
}

void picker::ogl::Mesh::draw( IRenderTarget& target ) const
{
	#if defined( PICKER_SYSTEM_WINDOWS )
	const picker::ogl::Transform& t = getTransform();
	glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
	glPushMatrix();
	glTranslatef( t.getPosition().x, t.getPosition().y, t.getPosition().z );
	glRotatef( t.getRotation().x, 1.0f, 0.0f, 0.0f );
	glRotatef( t.getRotation().y, 0.0f, 1.0f, 0.0f );
	glRotatef( t.getRotation().z, 0.0f, 0.0f, 1.0f );
	glScalef( t.getScale().x, t.getScale().y, t.getScale().z );
	#endif

	for ( unsigned int i = 0; i < _triangles.size(); i++ )
	{
		const picker::ogl::Mesh::VertexIndexTriple& tI = _triangles[i];

		const picker::ogl::Vertex vA( _vertexPositions[tI.a.positionIndex], picker::Color( 1.0f, 1.0f, 1.0f ), _normals[tI.a.normalIndex], _uvCoordinates[tI.a.uvCoordinateIndex] );
		const picker::ogl::Vertex vB( _vertexPositions[tI.b.positionIndex], picker::Color( 1.0f, 1.0f, 1.0f ), _normals[tI.b.normalIndex], _uvCoordinates[tI.b.uvCoordinateIndex] );
		const picker::ogl::Vertex vC( _vertexPositions[tI.c.positionIndex], picker::Color( 1.0f, 1.0f, 1.0f ), _normals[tI.c.normalIndex], _uvCoordinates[tI.c.uvCoordinateIndex] );

		picker::ogl::Triangle triangle( vA, vB, vC );
		target.draw( triangle );
	}

	//target.draw( _vertexBuffer );

	#if defined( PICKER_SYSTEM_WINDOWS )
	glPopMatrix();
	#endif
}
