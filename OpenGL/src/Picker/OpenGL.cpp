#include <Picker/OpenGL.hpp>
#include <iostream>

void picker::ogl::checkLastOpenGlError( const char* file, unsigned line, const char* expression )
{
	GLenum lastErrorCode = glGetError();
	if( lastErrorCode == GL_NO_ERROR )
	{
		return;
	}

	::std::cout << "An internal OpenGL call failed in \"" << file << "\" (" << line << ").\n";
	::std::cout << "Expression:\n\t" << expression << "\n";
	::std::cout << "Error description:\n\t" << lastErrorCode << ", " << gluErrorString( lastErrorCode ) << "\n";
}
